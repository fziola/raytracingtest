function getCanvasPos(e) {
    // screen to canvas pos -> offset + scroll
    // and client canvas pos to draw canvas pos -> width is inner size and clientWidth is size of element in html
    x = canvas2d.width * ((e.clientX - canvas2d.offsetLeft + window.scrollX) / canvas2d.clientWidth)
    y = canvas2d.height * ((e.clientY - canvas2d.offsetTop + window.scrollY) / canvas2d.clientHeight)
    return [x,y]
}

function correctCanvasInnerSize(movecam=true) {
    // the problem is, when a canvas resizes its clientWidth the inner size stays the same, and therefore will be displayed as very stretched
    resizeCanvas2d(movecam)
    resizeCanvas3d()
}

function offsetCanvas(dimension, value) {
    // move objects, lights, cameraPos
    objects.forEach(object => {
        object.forEach(point => {
            point[dimension] += value
        })
    })
    lights.forEach(light => {
        light[dimension] += value
    })
    cameraPos[dimension] += value
    resizeCanvas2d(movecam=false)
}

function zoomCanvas(value) {
    if(value == 0) {
        canvasZoom = baseZoom
    } else {
        canvasZoom += value
        if (canvasZoom < 0.1) {
            canvasZoom = 0.1
        }        
    }

    resizeCanvas2d(movecam=false)
}

function resizeCanvas2d(movecam=true) {
    canvas2d.width = Math.round(canvas2d.clientWidth * canvasZoom)
    canvas2d.height = Math.round(canvas2d.clientHeight * canvasZoom)
    objects[0] = [[0.0,0.0],[canvas2d.width,0.0,],[canvas2d.width,canvas2d.height],[0.0,canvas2d.height]]

    if(movecam) {
        if(cameraPos[0] > canvas2d.width) {
            cameraPos[0] = Math.round(canvas2d.width / 2)
        }
        if(cameraPos[1] > canvas2d.height) {
            cameraPos[1] = Math.round(canvas2d.height / 2)
        }
    }
}

function resizeCanvas3d() {
    canvas3d.width = Math.round(canvas3d.clientWidth * baseZoom)
    canvas3d.height = Math.round(canvas3d.clientHeight * baseZoom)
}