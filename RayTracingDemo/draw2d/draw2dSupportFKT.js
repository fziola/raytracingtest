function combineColors(reflColor, reflectiveness, refrColor, refractiveness, localColor, objColor) {
  let objColorPart = divVectorNumber(objColor, 255)
  let refrC = [0,0,0]
  // i mean the reflectiveness added if total reflection should NOT be added to the local color ONLY the refl ray color
  let addToReflectiveness = 0
  if (refrColor) {
    // (refr*incColor + (1-refr)*inccolor*(objColor/255)) * refr
    refrC = multVectorNumber(addVector(multVectorNumber(refrColor, refractiveness), multVectorNumber(multVectorsEach(objColorPart, refrColor), 1-refractiveness)), refractiveness)
  } else {
    // handle total reflection
    addToReflectiveness = refractiveness
  }
  let reflC = multVectorsEach(multVectorNumber(reflColor, reflectiveness + addToReflectiveness), objColorPart)
  return addVector(addVector(reflC, refrC), multVectorNumber(localColor, reflectiveness))
}

function getRefractionAngle_and_vec(wallVec, incDirVec, objNrFrom, objNrTo) {
  let wallN = multVectorNumber(normalize2d(rotatepoint2d(wallVec, (2*Math.PI) - 1.57079632679)), -1)   // rotate 90 deg to right
  let angle = angle2d(normalize2d(incDirVec), wallN)
  let outAngle = -1*Math.asin((Math.sin(angle)*objMaterials[objNrFrom][2])/objMaterials[objNrTo][2])
  // yes im rotating the vector into position
  let outVec = rotatepoint2d(wallN, outAngle)
  return [outAngle, outVec]
}

function getCriticalAngle(from, to) {
  let criticalAngle = 12345   // nur wenn vom Lot weggebrochen wird relevant
  if(objMaterials[to][2] > objMaterials[from][2]) {
    criticalAngle = Math.asin(objMaterials[to][2] / objMaterials[from][2])
  }
  return criticalAngle
}

function getAlphaValue(recCount, currentRayInfluence) {
  switch(rayAlphaOption) {
    case 1:
      return 1-((recCount-1)/maxRec)
    case 2:
      return currentRayInfluence
    default:
      return 1
  }
}

function getDrawRayColor(standard, lightColor) {
  switch(rayColorOption) {
    case 1:
      return lightColor
    default:
      return standard
  }
}