function line_intersect2d(p1, p2, p3, p4) {
    // get intersection Point of 2 linesegments p1-p2 and p3-p4
    // inspired by Paul Bourke http://paulbourke.net/geometry/pointlineplane/
    // Return FALSE if the lines don't intersect

    // Check if none of the lines are of length 0
	if ((p1[0] === p2[0] && p1[1] === p2[1]) || (p3[0] === p4[0] && p3[1] === p4[1])) {
		return false
	}
    var denom = (p4[1] - p3[1])*(p2[0] - p1[0]) - (p4[0] - p3[0])*(p2[1] - p1[1]);
    if (denom == 0) {
        return null;
    }
    ua = ((p4[0] - p3[0])*(p1[1] - p3[1]) - (p4[1] - p3[1])*(p1[0] - p3[0]))/denom;
    ub = ((p2[0] - p1[0])*(p1[1] - p3[1]) - (p2[1] - p1[1])*(p1[0] - p3[0]))/denom;

    // is the intersection along the segments
	if (ua < 0 || ua > 1 || ub < 0 || ub > 1) {
		return false
	}

    return [
        p1[0] + ua * (p2[0] - p1[0]), p1[1] + ua * (p2[1] - p1[1])
    ]
}

function distToObjWall_nr(startPoint, vec, objNr) {
    return distToObjWall(startPoint, vec, objects[objNr])
}

function distToObjWall(startPoint, vec, obj) {
    // get dist to outline of obj.
    let dist = -1
    let pointFarAway = multVectorNumber(vec, 20000000)
    for (let i = 0; i < obj.length; i++) {
        let hitPoint = line_intersect2d(startPoint, pointFarAway, obj[i], obj[(i+1) % obj.length])
        if (hitPoint) {
            let temp = vecLength(subVector(hitPoint, startPoint))
            if (temp > dist) {
                dist = temp
            }
        }
    }
    return dist
}

function getIntersectionPoint2d(pA, pB) {
    let firstHit
    let minDist = canvas2d.width+canvas2d.height
    let objC = 0
    let currentColor
    let objNr = 0
    objects.forEach(o => {
        let i
        for (i = 0; i < o.length; i++) {
            ret = line_intersect2d(pA, pB, o[i], o[(i+1)%o.length])
            if(Array.isArray(ret)) {
                let di = vecLength(subVector(ret, pA))
                
                if(di < minDist) {
                    firstHit = ret
                    minDist = di
                    currentColor = objColors[objC]
                    objNr = objC
                }
            }
        }
        objC += 1
    })
    return firstHit
}

function getIntersectionInfo2d_vector(p, vec) {
    // returns [firstHit, objNr, wallVector]
    return getIntersectionInfo2d(p, addVector(p, multVectorNumber(vec, 2000000)))
}

function getIntersectionInfo2d(pA, pB) {
    let firstHit
    let minDist = canvas2d.width+canvas2d.height
    let objC = 0
    let currentColor
    let objNr = 0
    let wallVector
    let ctxx = canvas2d.getContext('2d')
    objects.forEach(o => {
        let i
        for (i = 0; i < o.length; i++) {
            ret = line_intersect2d(pA, pB, o[i], o[(i+1)%o.length])
            if(Array.isArray(ret)) {
                let wallVTemp = subVector(o[i], o[(i+1)%o.length])
                let wallNTemp = multVectorNumber(normalize2d(rotatepoint2d(wallVTemp, 1.57079632679)), -0.001)  // rotate 90 deg to right
                let di = vecLength(subVector(ret, pA))

                //drawCircle(ctxx, ret[0],ret[1],[0,255,0], 10)
                // pointsOfInterest.push([ret[0],ret[1], 2, true, 0, [255,255,255], 0, 0])
                
                if(di < minDist) {
                    firstHit = ret
                    minDist = di
                    currentColor = objColors[objC]
                    objNr = objC
                    wallVector = subVector(o[i], o[(i+1)%o.length])
                    wallN = wallNTemp
                }
            }
        }
        objC += 1
    })

    if(Array.isArray(firstHit)) {
        if (showNormals && objNr > 0) {
            // draw normals
            let wallNormalDrawPoint = [firstHit[0]+wallN[0]*50000, firstHit[1]+wallN[1]*50000]
            drawBasicPath(ctxx, [firstHit,wallNormalDrawPoint], invertArray(objColors[0], 255))
        }
        // add very small wall normal to the point,
        // so the intersection doesnt find it as intersection with dist 0
        // a dist != 0 check is not good bcuz it can go INTO elements so dir is important
        firstHit[0] += wallN[0]
        firstHit[1] += wallN[1]
        return [firstHit, objNr, wallVector]
    }
    return false
}

function shadowRayIntersectionTest2d(x, y) {
    let hits = []
    lights.forEach(l => {
        // cast a ray to each light source and just see if its blocked
        hits.push(getIntersectionPoint2d([x, y], [l[0], l[1]]))
    })
    return hits
}