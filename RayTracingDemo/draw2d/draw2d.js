function drawObjects(ctx) {
    var c = 1
    let color

    objects.slice(1).forEach(obj => {
        color = [255,255,255]

        if (c == highlighted) {
            color = [0,255,0]
            drawBasicPath(ctx, [centerOfHighlighted, rotationGrabPoint], [125, 125, 125])
            drawCircle(ctx, rotationGrabPoint[0], rotationGrabPoint[1], [255, 255, 255], radiusOfGrabPoint, 5)
            fillCircle(ctx, rotationGrabPoint[0], rotationGrabPoint[1], [0, 0, 0], radiusOfGrabPoint)
            // draw center -> only visible if outside obj
            fillCircle(ctx, centerOfHighlighted[0], centerOfHighlighted[1], objColors[c], radiusOfGrabPoint)
        }
        fillBasicShape(ctx, obj, objColors[c], alpha=1-objMaterials[c][1])
        if (c != 0) {
            // dont want to draw the border
            drawBasicShape(ctx, obj, color, 2)
        }
        c += 1
    })
}

function drawTemp(ctx) {
    // draws temporary lines of the "[draw] button function"
    if(drawArray.length > 1) {
        let back = rgbStringToArray(window.getComputedStyle(canvas2d).backgroundColor)
        let r = 255 - back[0]
        let g = 255 - back[1]
        let b = 255 - back[2]

        drawBasicShape(ctx, drawArray, [r,g,b], 5)
    }
}

function drawLight(context, centerX, centerY, colorArray, radius, higlighted) {
    let invColor = invertArray(colorArray, 255)
    let rayArray = [0,0]
    if(higlighted) {
        drawCircle(context, centerX, centerY, invColor, radius + 5)
    }
    switch(lampStyle) {
        case 0:
            fillCircle(context, centerX, centerY, colorArray, radiusLight)
            drawCircle(context, centerX, centerY, invColor, radius)
            break
        case 1:
            rayArray = [radius*lightRayLength, 0]
            for(let i = 0; i < 8; i++) {
                rayArray = rotatepoint2d(rayArray, 0.785398)    // 360/8 = 45 deg
                drawBasicPath(context, [[centerX, centerY],addVector([centerX, centerY],rayArray)], colorArray, 3)
            }
            fillCircle(context, centerX, centerY, colorArray, radiusLight)
            drawCircle(context, centerX, centerY, invColor, radius)
            break
        case 2:
            fillCircle(context, centerX, centerY, colorArray, radiusLight)
            drawCircle(context, centerX, centerY, invColor, radius)
            rayArray = rotatepoint2d([radius, 0], 0.785398)
            for(let i = 0; i < 4; i++) {
                rayArray = rotatepoint2d(rayArray, 1.5708)    // 360/4 = 90 deg
                drawBasicPath(context, [[centerX, centerY],addVector([centerX, centerY],rayArray)], invColor, 3)
            }
            break
        default:
            console.log("not a lampStyle")
    }
}

function drawCam(ctx, pos, direction, distToScreen) {
    let size = 25
    /*
    Bogen
      |
      v
     ____
    /    \  links: B, rechts: C
    \    /
     \  /
      V     Unten: A
    
    */
    
    // draw camera Cone
    let A = pos
    let temp = multVectorNumber(direction, size)
    let center = addVector(A, temp)
    let B = addVector(A, rotatepoint2d(temp, -0.8))
    let C = addVector(A, rotatepoint2d(temp, 0.8))

    let angle = angle2d([0.0,1.0], direction)
    let circleStart = angle

    let camColor = invertArray(objColors[0], 255)

    if (capture_SVG) {
        svg_state.push(["paths", [[C, A, B], camColor, 2, 1]])
        // arc und path A funktionieren anders, das ist der beste Nachbau den ich bekomme
        svg_state.push(["arc", [[B, C], center, [size*0.8, size*0.8], camColor, 2, 1, 1, 1]])
    }
    
    ctx.beginPath()

    ctx.moveTo(C[0], C[1])
    ctx.lineTo(A[0], A[1])
    ctx.lineTo(B[0], B[1])
    ctx.arc(center[0], center[1], size, circleStart, circleStart + Math.PI, false)

    ctx.closePath()
    ctx.lineWidth = 2
    ctx.strokeStyle = rgbArrayToString(camColor)
    ctx.stroke()

    if (highlightedCam) {
        let bb = [cameraPos[1] + 100, cameraPos[0] + 100, cameraPos[1] - 100, cameraPos[0] - 100]
        let camPoints = [[bb[3],bb[2]], [bb[1],bb[2]], [bb[1],bb[0]], [bb[3],bb[0]]]
        drawBasicShape(ctx, camPoints, [100,100,100], 1)
    }
}

function drawRay(ctx, camPos, numberOfRays, ray, left_right) {
    recCount = 0
    let point = getScreen_Ray_Start(left_right,numberOfRays,ray)

    // follow Ray
    // continue the vector out of bounds and look for intersections
    // the canvas border is a BB

    let minDist = canvas2d.width+canvas2d.height
    let dir = normalize2d(subVector(point, camPos))

    // be careful with big out of bounds or the rounding errors of js will be too noticable
    let continuedLine = [point[0]+dir[0]*minDist, point[1]+dir[1]*minDist]

    let firstHit_objNr_wallVec = getIntersectionInfo2d(point, continuedLine)
    if(firstHit_objNr_wallVec) {
        let firstHit = firstHit_objNr_wallVec[0]
        let objNr = firstHit_objNr_wallVec[1]
        let wallVec = firstHit_objNr_wallVec[2]

        //let testPath = [camPos, continuedLine]
        //drawBasicPath(ctx, testPath, "red", 2)

        left = left_right[0]
        right = left_right[1]
        //let scrPath = [left, right]
        //drawBasicPath(ctx, scrPath, "white", 2)

        let screenPart = getScreenPart(left_right,numberOfRays,ray)

        // simple shadow ray
        let bonusoffset = 20
        if(displayExplainBoxes && ray === 0) {
            let tempPopup = document.getElementById("miniPopup")
            tempPopup.style.display = "block"
            tempPopup.style.left = `${(firstHit[0] / canvas2d.width)*canvas2d.clientWidth + canvas2d.offsetLeft + bonusoffset}px`
            tempPopup.style.top = `${(firstHit[1] / canvas2d.height)*canvas2d.clientHeight + canvas2d.offsetTop + bonusoffset}px`
            let temppopupText = document.getElementById("miniPopupText")
            temppopupText.innerHTML = "cast a shadow Ray to all light sources and see if there is light"
        }

        let wallN = normalize2d(rotatepoint2d(wallVec, (2*Math.PI) - 1.57079632679))   // rotate 90 deg to left
        let toCam = normalize2d(subVector(camPos, firstHit))
        let myColor_array = phongLighting2d(ctx, objNr, firstHit, wallN, toCam, 1)

        let totalColor_array = getNewRay(myColor_array, objNr, wallVec, firstHit, dir, ctx, recCount, 1)

        let path = [camPos, point, firstHit]
        drawBasicPath(ctx, path, getDrawRayColor(rayColors.rayColor, totalColor_array), 2, showRay)
        // 1d screen
        drawBasicPath(ctx, screenPart, totalColor_array, 7)
    }
}

function getNewRay(localColor, objNr, wallVector, firstHit, dir, ctx, recCount, currentRayInfluence, incObjNr=0) {
    if(currentRayInfluence > minInfluence) {
        // show obj color on hit (without the reflection and refr)
        if(showObjHits && (objNr != 0 || showObjHitOnCanvas)) {
            fillCircle(ctx, firstHit[0], firstHit[1], localColor, 10)
            drawCircle(ctx, firstHit[0], firstHit[1], invertArray(objColors[0], 255), 10)
        }
        pointsOfInterest.push([firstHit[0], firstHit[1], 2, false, objNr, localColor, phi, theta])
    }
    // make new rays at the point a ray hits an obj
    // recursion if reflective / refractive surface
    let reflectiveness = objMaterials[objNr][0]
    let refractiveness = objMaterials[objNr][1]
    let refrColor = null
    let reflColor = [0,0,0]
    if (refractiveness > 0) {
        refrColor = refractRay(ctx, objNr, wallVector, dir, firstHit, incObjNr, recCount, currentRayInfluence*refractiveness)
    }
    if(reflectiveness > 0) {
        reflColor = reflectRay(ctx, wallVector, dir, firstHit, recCount, currentRayInfluence*reflectiveness)
    }
    return combineColors(reflColor, reflectiveness, refrColor, refractiveness, localColor, objColors[objNr])
}

function refractRay(ctx, objNr, wallVecOld, incDir, point, incObjNr, recCount, currentRayInfluence) {
    let angle_and_vec = getRefractionAngle_and_vec(wallVecOld, incDir, incObjNr, objNr)
    let outAngle = angle_and_vec[0]
    let outVec = angle_and_vec[1]

    if (!outAngle || outAngle > getCriticalAngle(incObjNr, objNr)) {
        // total reflection should be handled in getNewRay
        return undefined
    }
    return castInternalRay(ctx, objNr, outVec, point, recCount, currentRayInfluence)
}

function castInternalRay(ctx, objNr, incDir, point, recCount, currentRayInfluence) {
    let outsideObjNr = 0
    let reflectiveness = objMaterials[objNr][0]
    let refractiveness = objMaterials[objNr][1]
    recCount += 1
    if(recCount > maxRec) {
        return objColors[objNr]
    }
    let firstHit_info = getIntersectionInfo2d_vector(addVector(point, incDir), incDir)
    let closestObj = firstHit_info[1]
    if(closestObj == 0) {
        // in case it overlaps with the edge
        return [0,0,0]
    }
    let refrColor = [0,0,0]

    let angle_vec_leave_ob = getRefractionAngle_and_vec(multVectorNumber(firstHit_info[2], -1), incDir, objNr, outsideObjNr)
    let vec_leave_ob = angle_vec_leave_ob[1]
    if (!angle_vec_leave_ob[0] || angle_vec_leave_ob[0] > getCriticalAngle(objNr, outsideObjNr)) {
        // total reflection should be handled in getNewRay
        refrColor = undefined
    } else {
        // cast new ray outside of the obj.
        let newRayStart = addVector(firstHit_info[0], incDir)
        let outsideHit_info = getIntersectionInfo2d_vector(newRayStart, vec_leave_ob)

        if (outsideHit_info[0]) {
            let path = [firstHit_info[0], outsideHit_info[0]]
            let wallN_new = normalize2d(rotatepoint2d(outsideHit_info[2], (2*Math.PI) - 1.57079632679))   // rotate 90 deg to right
            let hitToOrigin = multVectorNumber(vec_leave_ob, -1)
            let myColor_array = phongLighting2d(ctx, outsideHit_info[1], outsideHit_info[0], wallN_new, hitToOrigin, currentRayInfluence)
            refrColor = getNewRay(myColor_array, outsideHit_info[1], outsideHit_info[2], outsideHit_info[0], vec_leave_ob, ctx, recCount, currentRayInfluence*refractiveness, incObjNr=0)
            drawBasicPath(ctx, path, getDrawRayColor(rayColors.rayColor, refrColor), 2, showRay, alpha=getAlphaValue(recCount, currentRayInfluence*refractiveness))
        }
    }
    // internal reflection
    let wallN = normalize2d(rotatepoint2d(firstHit_info[2], (2*Math.PI) - 1.57079632679))   // rotate 90 deg to right
    let reflDir = subVector(incDir, multVectorNumber(wallN,(2*vector2dDotProduct(wallN, incDir))))
    let objColorPart = divVectorNumber(objColors[objNr], 255)
    // refl*refleciveness*objColor + refr*refrectiveness
    let refrC = [0,0,0]
    if (refrColor) {
        refrC = multVectorNumber(refrColor, refractiveness)
    } else {
        // handle total reflection
        reflectiveness += refractiveness
    }
    let reflColor = castInternalRay(ctx, objNr, reflDir, addVector(firstHit_info[0], reflDir), recCount, currentRayInfluence*reflectiveness)
    let c = addVector(multVectorsEach(multVectorNumber(reflColor, reflectiveness), objColorPart), refrC)
    drawBasicPath(ctx, [point, firstHit_info[0]], getDrawRayColor(invertArray(objColors[objNr], 255), c), 2, showRay, getAlphaValue(recCount, currentRayInfluence))
    return c
}

function reflectRay(ctx, wallVecOld, incDir, point, recCount, currentRayInfluence) {
    recCount += 1
    if(recCount > maxRec) {
        return rgbStringToArray(window.getComputedStyle(canvas2d).backgroundColor)
    }
    let wallN = normalize2d(rotatepoint2d(wallVecOld, (2*Math.PI) - 1.57079632679))   // rotate 90 deg to right
    let reflectionDirection = subVector(incDir, multVectorNumber(wallN,(2*vector2dDotProduct(wallN, incDir))))

    let minDist = canvas2d.width+canvas2d.height
    let dir = normalize2d(reflectionDirection)

    let continuedLine = [point[0]+dir[0]*minDist, point[1]+dir[1]*minDist]

    let firstHit_objNr_wallVec = getIntersectionInfo2d(point, continuedLine)
    if(firstHit_objNr_wallVec) {
        let firstHit = firstHit_objNr_wallVec[0]
        let objNr = firstHit_objNr_wallVec[1]
        let wallVec = firstHit_objNr_wallVec[2]

        let path = [point, firstHit]
        let toPoint = normalize2d(subVector(point, firstHit))
        let wallN_new = normalize2d(rotatepoint2d(wallVec, (2*Math.PI) - 1.57079632679))   // rotate 90 deg to right
        let myColor_array = phongLighting2d(ctx, objNr, firstHit, wallN_new, toPoint, currentRayInfluence)

        let c = getNewRay(myColor_array, objNr, wallVec, firstHit, dir, ctx, recCount, currentRayInfluence)
        drawBasicPath(ctx, path, getDrawRayColor(rayColors.rayColor, c), 2, showRay, getAlphaValue(recCount, currentRayInfluence))
        return c
    }
    return [0,0,0]
}
