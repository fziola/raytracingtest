function clearCanvas(context) {
    context.clearRect(0, 0, canvas2d.width, canvas2d.height)
}

function fillCircle(context, centerX, centerY, colorArray, radius, alpha=1) {
    context.globalAlpha = alpha
    if (capture_SVG) {
        svg_state.push(["circles_fill", [[centerX, centerY], colorArray, radius, alpha]])
    }
    color = rgbArrayToString(colorArray)
    context.beginPath()
    context.arc(centerX, centerY, radius, 0, 2 * Math.PI, false)
    context.fillStyle = color
    context.fill()
    context.lineWidth = 1
    context.strokeStyle = color
    context.stroke()
}

function drawCircle(context, centerX, centerY, colorArray, radius, lineWidth=2, alpha=1) {
    context.globalAlpha = alpha
    if (capture_SVG) {
        svg_state.push(["circles_outline", [[centerX, centerY], colorArray, radius, lineWidth, alpha]])
    }
    color = rgbArrayToString(colorArray)
    context.beginPath()
    context.arc(centerX, centerY, radius, 0, 2 * Math.PI, false)
    context.lineWidth = lineWidth
    context.strokeStyle = color
    context.stroke()
}

function drawBasicShape(context, points, colorArray, lineWidth, alpha=1) {
    context.globalAlpha = alpha
    if (capture_SVG) {
        points.push(points[0])  // returns the length lmao
        svg_state.push(["paths", [points, colorArray, lineWidth, alpha]])
    }
    color = rgbArrayToString(colorArray)
    context.beginPath()
    context.moveTo(points[0][0], points[0][1])
    // slice 1 cuz that is the starting point
    points.slice(1).forEach(p => {
        context.lineTo(p[0], p[1])
    })
    context.closePath()
    context.strokeStyle = color
    context.lineWidth = lineWidth
    context.stroke()
}

function fillBasicShape(context, points, colorArray, alpha=1) {
    context.globalAlpha = alpha
    if (capture_SVG) {
        points.push(points[0])  // returns the length lmao
        svg_state.push(["paths_fill", [points, colorArray, alpha]])
    }
    color = rgbArrayToString(colorArray)
    context.beginPath()
    context.moveTo(points[0][0], points[0][1])
    // slice 1 cuz that is the starting point
    points.slice(1).forEach(p => {
        context.lineTo(p[0], p[1])
    })
    context.closePath()
    context.fillStyle = color
    context.fill()
}

function drawBasicPath(context, points, colorArray, lineWidth=2, showBool=true, alpha=1) {
    if (showBool) {
        if (capture_SVG) {
            svg_state.push(["paths", [points, colorArray, lineWidth, alpha]])
        }
        context.beginPath()
        context.moveTo(points[0][0], points[0][1])
        // slice 1 cuz that is the starting point
        points.slice(1).forEach(p => {
            context.lineTo(p[0], p[1])
        })
        context.strokeStyle = rgbArrayToString(colorArray)
        context.globalAlpha = alpha
        context.lineWidth = lineWidth
        context.stroke()
    }
}
