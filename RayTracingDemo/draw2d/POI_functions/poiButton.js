function toggleFormula() {
  formulaDisplay = !formulaDisplay
  document.getElementById("POI_toggle_formula").innerHTML = " Display current values in Equation: " + formulaDisplay
  updatePoiPopup()
}

function updatePoiPopup() {
  if(chosenPOI >= 0 && chosenPOI < lastPointsOfInterest.length) {
    let POIexplanationId = 'POIexplanation'
    let tempPopupText = document.getElementById(POIexplanationId)
    tempPopupText.innerHTML = get_POI_popup_text(chosenPOI)
    MathJax.Hub.Queue(["Typeset", MathJax.Hub, POIexplanationId]) // reload item with MathJax
  }
}