function init() {
  correctCanvasInnerSize()
  loadFullStateFromLocalStorage()

  setupForSmallScreens()
  setUpButtonTexts()

  makeMenuHeadersDraggable()

  // set inner canvas size

  // start of with camActive
  // button2d("editCamBut")
  canvas2d.addEventListener("mousedown", function (e) {
    mouseDown2d(e)
  }, false)
  canvas2d.addEventListener("mouseup", function (e) {
    mouseUp2d(e)
  }, false)
  canvas2d.addEventListener('mousemove', function (e) {
    mouseMove2d(e)
  }, false)
  document.addEventListener('keydown', (e) => {
    keyDown(e)
  })
  document.addEventListener('keyup', (e) => {
    keyUp(e)
  })
  window.addEventListener('contextmenu', function (e) { 
    e.preventDefault()
  }, false)
  window.addEventListener("resize", correctCanvasInnerSize)
  window.addEventListener("wheel", onMouseWheel, { passive: false })
  window.addEventListener("unload", saveFullStateToLocalStorage)

  setUpGPUFunctions()
  window.requestAnimationFrame(draw2d)
  if(continuous3D) {
    window.requestAnimationFrame(draw3d)
  }
}

/*
=============================================================================================================
=============================================================================================================
canvas 2d functions
=============================================================================================================
=============================================================================================================
*/

// draw functions ===========================================================================================

function draw3d() {
  console.log('start 3d')
  const ctx3d = canvas3d.getContext('2d') // ironic, isnt it?

  let w = Math.round(size3d * canvas3d.clientWidth)
  let h = Math.round(size3d * canvas3d.clientHeight)
  canvas3d.width = w
  canvas3d.height = h

  clearCanvas(ctx3d)

  if(useCPU) {
    ctx3d.putImageData(get3DscreenCPU(w, h), 0 ,0)
  } else {
    ctx3d.putImageData(get3DscreenGPU_B(w, h), 0 ,0)
  }

  console.log('end 3d')
  // ctx3d.putImageData(get3DscreenGPU(w, h), 0, 0)
  if(continuous3D) {
    window.requestAnimationFrame(draw3d)
  }
}

function draw2d() {
  if (canvas2d.getContext) {
    if(measureTime_2d) {
      // remove this, it is only to test and should not be accessible to the user
      var startTime = performance.now()
    }

    if (capture_SVG_start) {
      capture_SVG_start = false
      capture_SVG = true
    }

    const ctx = canvas2d.getContext('2d')
    clearCanvas(ctx)
    // draw background because canvas background is not shown in svg/png
    fillBasicShape(ctx, objects[0], objColors[0], alpha=1)

    let tempC = 0
    lights.forEach(l => {
      let lCol = lightColors[tempC]
      drawLight(ctx, l[0], l[1], lCol, radiusLight, (tempC == highlightedLight))
      tempC += 1
    })

    drawObjects(ctx)

    drawTemp(ctx)

    camVec = normalize2d([cameraDir[0], cameraDir[1]])
    let cPos = [cameraPos[0],cameraPos[1]]
    let left_right = getScreenLineCorners(cPos, camVec, distToScreen)
    // draw rays
    for(let ray = 0; ray < numberOfRays; ray++) {
      drawRay(ctx, cPos, numberOfRays, ray, left_right)
    }

    // draw camera
    drawCam(ctx, cPos, camVec, distToScreen)

    if(showPOI) {
      for(let poi = 0; poi < pointsOfInterest.length; poi++) {
        if(pointsOfInterest[poi][3]) {
          if(poi != chosenPOI) {
            fillCircle(ctx, pointsOfInterest[poi][0], pointsOfInterest[poi][1], [255,0,0], 10)
            drawCircle(ctx, pointsOfInterest[poi][0], pointsOfInterest[poi][1], [255,255,255], 10)
          } else {
            fillCircle(ctx, pointsOfInterest[poi][0], pointsOfInterest[poi][1], [0,255,0], 10)
            drawCircle(ctx, pointsOfInterest[poi][0], pointsOfInterest[poi][1], [255,255,255], 10)
          }
        }
      }
    }

    lastLightsForPOI = lightsForPOI.slice()
    lightsForPOI = []
    lastPointsOfInterest = pointsOfInterest.slice()
    pointsOfInterest = []

    if (capture_SVG) {
      capture_SVG = false
      export_captured_SVG_state()
      svg_state = []
    }
  }

  if(measureTime_2d) {
    // remove this, it is only to test and should not be accessible to the user
    var endTime = performance.now()
    totalMeasuredTime += endTime - startTime
    timesMeasured += 1
    let fps = 1/((totalMeasuredTime / timesMeasured)/1000)
    console.log(`Draw2d FPS: ${fps}`)
  }
  window.requestAnimationFrame(draw2d)
}

// drag functions ===========================================================================================
// https://www.w3schools.com/howto/howto_js_draggable.asp

function makeMenuHeadersDraggable() {
  var x = document.getElementsByClassName("dragHeader")
  for (let i = 0; i < x.length; i++) {
    x[i].parentElement.style.left = String(canvas2d.clientWidth+canvas2d.offsetLeft) + "px"
    x[i].parentElement.style.top = String(canvas2d.clientHeight*0.5) + "px"
    dragElement(x[i])
  }
  let headerSubs = document.getElementsByClassName("dragHeaderSub")
  for (let i = 0; i < headerSubs.length; i++) {
    dragElement(headerSubs[i], true)
  }
}

function dragElement(elmnt, subHeader=false) {
  var pos1 = 0, pos2 = 0, pos3 = 0, pos4 = 0;
  elmnt.onmousedown = dragMouseDown;

  function dragMouseDown(e) {
    e = e || window.event;
    e.preventDefault();
    // get the mouse cursor position at startup:
    pos3 = e.clientX;
    pos4 = e.clientY;
    document.onmouseup = closeDragElement;
    // call a function whenever the cursor moves:
    document.onmousemove = elementDrag;
  }

  function elementDrag(e) {
    e = e || window.event;
    e.preventDefault();
    // calculate the new cursor position:
    pos1 = pos3 - e.clientX;
    pos2 = pos4 - e.clientY;
    pos3 = e.clientX;
    pos4 = e.clientY;
    // set the element's new position:
    if(subHeader) {
      // if the dragheader is in another div
      elmnt.parentElement.parentElement.style.top = (elmnt.parentElement.parentElement.offsetTop - pos2) + "px";
      elmnt.parentElement.parentElement.style.left = (elmnt.parentElement.parentElement.offsetLeft - pos1) + "px";
    } else {
      elmnt.parentElement.style.top = (elmnt.parentElement.offsetTop - pos2) + "px";
      elmnt.parentElement.style.left = (elmnt.parentElement.offsetLeft - pos1) + "px";
    }
  }

  function closeDragElement() {
    // stop moving when mouse button is released:
    document.onmouseup = null;
    document.onmousemove = null;
  }
}


// setup functions ==========================================================================================
function updateAllPopups() {
  updateCurrentEditMenu()
  // updatePoiPopup() // poi popup is never open when u click something else :I
}

function setUpButtonTexts() {
  // set variable to the option buttons so it is displayed
  document.getElementById("showFormulasBtn").innerHTML = "  show formulas:  " + displayExplainBoxes
  document.getElementById("showRayBtn").innerHTML = " show rays: " + showRay
  document.getElementById("showShadowRayBtn").innerHTML = " show shadow rays: " + showShadowRay
  document.getElementById("showNormalsBtn").innerHTML = " show object normals: " + showNormals
  document.getElementById("showObjHitsBtn").innerHTML = " show object hit color: " + showObjHits
  document.getElementById("showPreserveEnergy").innerHTML = "preserve energy: " + showEnergyPreservationTerm
  document.getElementById("showPOI").innerHTML = " show points of interest: " + showPOI
  document.getElementById("POI_toggle_formula").innerHTML = " Display current values in Equation: " + formulaDisplay
  document.getElementById("toggleGPUbutton").innerHTML = 'using GPU: '+ !useCPU
  document.getElementById("toggleContinuosButton").innerHTML = 'continuous3d: '+ continuous3D
  document.getElementById("lampStyleButton").innerHTML = 'Style: ' + lampStyleNames[lampStyle]
  document.getElementById("showBtnHoverTextsButton").innerHTML = 'Show hover-texts: ' + showBtnHoverTexts
  document.getElementById("showObjHitOnCanvasEdge").innerHTML = 'Show obj. hit: ' + showObjHitOnCanvas

  let textA = 'always 1'
  switch(rayAlphaOption) {
    case 1:
      textA = 'lower with recursion'
      break
    case 2:
      textA = 'according to influence'
      break
  }
  document.getElementById('controlRayAlphaButton').innerHTML = 'Currently: ' + textA
  let textB = 'default'
  switch(rayColorOption) {
    case 1:
      textB = 'color of light'
      break
  }
  document.getElementById('controlRayColorButton').innerHTML = 'Currently: ' + textB

  document.getElementById('inputMaxRec').value = maxRec
  document.getElementById('inputRefrIndexBackground').value = objMaterials[0][2]

  // 3d
  document.getElementById('range_size_3d').value = Math.round(size3d*100)
  document.getElementById('range_fov_3d').value = Math.round(fovStep*100)
}

function setUpGPUFunctions() {
  // gpu.addFunction(multVectorNumber)
  // gpu.addFunction(addVectorNumber_GPU_3d)
  // gpu.addFunction(addVector_GPU_3d)
  // gpu.addFunction(normalize3d)
  // gpu.addFunction(crossProduct3d)
  // gpu.addFunction(subVector)
  // gpu.addFunction(vecLength)
  // gpu.addFunction(dotProduct)

  // gpu.addFunction(rotate3D_z_axis)

  // gpu.addFunction(getPlaneNormal)
  // gpu.addFunction(getPointRelativeToPlane)
  // gpu.addFunction(pointOnFace)

  // // gpu.addFunction(intersect3D_ray_all_GPU)
  // gpu.addFunction(testGPU)
}

function setupForSmallScreens() {
  const width  = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth
  const height = window.innerHeight|| document.documentElement.clientHeight|| document.body.clientHeight

  if(width < 500 ) {
    let els = document.getElementsByClassName('mltext')
    for (let i = 0; i < els.length; i++) {
      els[i].style.display = 'none'
    }
  }

  if(width < 850 ) {
    let els = document.getElementsByClassName('canvasTitle')
    for (let i = 0; i < els.length; i++) {
      els[i].style.fontSize = '3vw'
    }
  }

  // if(width < 1051 ) {
  //   console.log('small screen detected, CSS changed')
  //   let els = document.getElementsByClassName('side')
  //   for (let i = 0; i < els.length; i++) {
  //     els[i].style.float = 'none'
  //     els[i].style.width = '40%'
  //   }
  // }
}