function test2dMath() {
  test_point_in_polygon()
}

function test_point_in_polygon() {
  console.log('testing pointInPolygon(point, corners)')

  const testCases = [
    { point: [1,1], corners: [[0,0], [2,0], [1,2]], expected: true },
    { point: [0,1], corners: [[0,0], [2,2], [0,2]], expected: true },
    { point: [1,1], corners: [[0,0], [0,2], [2,0], [2,2]], expected: true },
    { point: [1,2], corners: [[0,0], [2,0], [0,2]], expected: false },
    { point: [3,3], corners: [[0,0], [2,2], [0,2]], expected: false }
  ]
  for(const testCase of testCases) {
    console.log(testCase.point, 'should ' + (testCase.expected ? '' : 'not ') + 'be in', testCase.corners)
    if(pointInPolygon(testCase.point, testCase.corners) == testCase.expected) {
      console.log('%c Success!', 'color: #11ff11')
    } else {
      allTestsSuccessfull = false
      console.log('%c Failure!', 'color: #ff1111')
    }
  }
}
