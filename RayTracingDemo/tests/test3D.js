function test3dMath() {
  // test cross product
  console.log([1,2,3], 'x', [-7,8,9], '=',  [-6, -30, 22])
  if(arraysEqual(crossProduct3d([1,2,3], [-7,8,9]), [-6, -30, 22])) {
    console.log('%c Success!', 'color: #11ff11')
  } else {
    allTestsSuccessfull = false
    console.log('%c Failure! (actual outcome:'+crossProduct3d([1,2,3], [-7,8,9])+')', 'color: #ff1111')
  }

  test2dMath()
  test_3d_math()
  test_point_on_face()
  test_ray_face_intersect()
  dump_bs()
}

function test_3d_math() {
  const reflTestCases = [
    { vec: [1,1,-1], n: [0,0,1], target: [1,1,1] },
    { vec: [1,0,0], n: [-1,0,0], target: [-1,0,0] },
    { vec: [0,1,0], n: [0,-1,-1], target: [0,0,-1] }
  ]
  for(const testCase of reflTestCases) {
    console.log(testCase.vec, 'hitting face with n = ', testCase.n, 'should be reflected in direction', testCase.target)
    if(arraysEqualNumber(reflect3d(testCase.vec, testCase.n), testCase.target, 0.00000001, true)) {
      console.log('%c Success!', 'color: #11ff11')
    } else {
      allTestsSuccessfull = false
      console.log('%c Failure! (actual outcome:'+reflect3d(testCase.vec, testCase.n)+')', 'color: #ff1111')
    }
  }

  const getNormal_testCases = [
    { surface: [[0,0,0], [0,1,0], [1,0,0]], expected: [0,0,1] },
    { surface: [[0,0,0], [0,1,1], [1,1,0]], expected: [1,-1,1] }
  ]
  for(const testCase of getNormal_testCases) {
    console.log(testCase.surface, 'should have normal', testCase.expected)
    if(arraysEqualNumber(getPlaneNormal(testCase.surface), testCase.expected, 0.00000001, true)) {
      console.log('%c Success!', 'color: #11ff11')
    } else {
      allTestsSuccessfull = false
      console.log('%c Failure! (actual outcome:'+getPlaneNormal(testCase.surface)+')', 'color: #ff1111')
    }
  }
}

function test_point_on_face() {
  // test if 3d point is on a 3d face
  console.log([50,50,50], 'should be on face', [[100,100,0], [0,0,0], [100,100,100], [0,0,100]])
  if(pointOnFace([50,50,50], [[100,100,0], [0,0,0], [100,100,100], [0,0,100]])) {
    console.log('%c Success!', 'color: #11ff11')
  } else {
    allTestsSuccessfull = false
    console.log('%c Failure! (actual outcome:'+pointOnFace([50,50,50], [[100,100,0], [0,0,0], [100,100,100], [0,0,100]])+')', 'color: #ff1111')
  }

  console.log([5,0,5], 'should be on face', [[0,0,10], [0,0,0],[10,0,10], [10,0,0]])
  if(pointOnFace([5,0,5], [[0,0,10], [0,0,0], [10,0,10], [10,0,0]])) {
    console.log('%c Success!', 'color: #11ff11')
  } else {
    allTestsSuccessfull = false
    console.log('%c Failure! (actual outcome:'+pointOnFace([5,0,5], [[0,0,10], [0,0,0], [10,0,10], [10,0,0]])+')', 'color: #ff1111')
  }

  let inputP = [5,5,5]
  let inputF = [[0,0,10], [0,0,0], [10,0,10], [10,0,0]]
  console.log(inputP, 'should not be on face', inputF)
  if(!pointOnFace(inputP, inputF)) {
    console.log('%c Success!', 'color: #11ff11')
  } else {
    allTestsSuccessfull = false
    console.log('%c Failure! (actual outcome:'+pointOnFace(inputP, inputF)+')', 'color: #ff1111')
  }

  inputP = [11,0,5]
  inputF = [[0,0,10], [0,0,0], [10,0,10], [10,0,0]]
  console.log(inputP, 'should not be on face', inputF)
  if(!pointOnFace(inputP, inputF)) {
    console.log('%c Success!', 'color: #11ff11')
  } else {
    allTestsSuccessfull = false
    console.log('%c Failure! (actual outcome:'+pointOnFace(inputP, inputF)+')', 'color: #ff1111')
  }

  inputP = [0, 335.2935287785161, 337.012122730119]
  inputF = [[0, 719, 1000], [0, 719, 0], [0, 0, 0], [0, 0, 1000]]
  console.log(inputP, 'should be on face', inputF)
  if(pointOnFace(inputP, inputF)) {
    console.log('%c Success!', 'color: #11ff11')
  } else {
    allTestsSuccessfull = false
    console.log('%c Failure! (actual outcome:'+pointOnFace(inputP, inputF)+')', 'color: #ff1111')
  }
}

function test_ray_face_intersect() {
  // test if ray hits plane in 3d
  console.log('line', [50,-50,50], '+t*', [0,1,0] , 'should hit plane xz')
  if(arraysEqual(lineIntersection_ray_face([[0,0,0],[100,0,0],[0,0,100],[100,0,100]], [50,-50,50], [0,1,0]), [50,0,50])) {
    console.log('%c Success!', 'color: #11ff11')
  } else {
    allTestsSuccessfull = false
    console.log('%c Failure! (actual outcome:'+lineIntersection_ray_face([[0,0,0],[100,0,0],[0,0,100],[100,0,100]], [50,-50,50], [0,1,0])+')', 'color: #ff1111')
  }

  console.log('line', [50,-50,50], '+t*', [0,-1,0] , 'should not hit plane xz')
  if(!arraysEqual(lineIntersection_ray_face([[0,0,0],[100,0,0],[0,0,100],[100,0,100]], [50,-50,50], [0,-1,0]), [50,0,50])) {
    console.log('%c Success!', 'color: #11ff11')
  } else {
    allTestsSuccessfull = false
    console.log('%c Failure! (actual outcome:'+lineIntersection_ray_face([[0,0,0],[100,0,0],[0,0,100],[100,0,100]], [50,-50,50], [0,-1,0])+')', 'color: #ff1111')
  }

  console.log('line', [-50,-50,50], '+t*', [0,1,0] , 'should not hit plane [0,0,0] to [100,0,100]')
  if(arraysEqual(lineIntersection_ray_face([[0,0,0],[100,0,0],[0,0,100],[100,0,100]], [-50,-50,50], [0,1,0]), [50,0,50]) == false) {
    console.log('%c Success!', 'color: #11ff11')
  } else {
    allTestsSuccessfull = false
    console.log('%c Failure! (actual outcome:'+lineIntersection_ray_face([[0,0,0],[100,0,0],[0,0,100],[100,0,100]], [-50,-50,50], [0,1,0])+')', 'color: #ff1111')
  }

  console.log('weird edge case?')
  if(lineIntersection_ray_face([[941, 719, 1000],[941, 719, 0],[0, 719, 1000],[0, 719, 0]], [578,248,100], [-128,-52,0]) == false) {
    console.log('%c Success!', 'color: #11ff11')
  } else {
    allTestsSuccessfull = false
    console.log('%c Failure! (actual outcome:'+lineIntersection_ray_face([[941, 719, 1000],[941, 719, 0],[0, 719, 1000],[0, 719, 0]], [578,248,100], [-128,-52,0])+')',
      'color: #ff1111')
  }
}

function dump_bs() {
  console.log('random test dump:')

  console.log('#1')
  if(lineIntersection_ray_face([[704.3817694080474, 124.3235378700827, 1000],[704.3817694080474, 124.3235378700827, 0],
    [764.1861691774648, 659.8694445328892, 1000],[764.1861691774648, 659.8694445328892, 0]],
    [528.726779022565, 391.33846624501643, 499.5],
    [28.72677902256499, -8.661533754983589, -0.5]
    )) {
      console.log('%c Success!', 'color: #11ff11')
    } else {
      allTestsSuccessfull = false
      console.log('%c Failure!', 'color: #ff1111')
    }
}