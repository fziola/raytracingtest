const standardText = 
`Find Controls and Information here!\n
press:\n
"e" to edit\n
"r" to add a rectangle\n
"space" to move the camera\n
"d" to draw a new object!
`
const editText = 
`In edit mode you can move The objects on the 2d Canvas by dragging them, you can delete with "DEL" or "BACKSPACE" and you can rotate them with the arrow keys!
Deselect with Spacebar.
`
const addRectText = 
`Just drag to make a new rectangle!
`
const addCircleText = 
`Not implemented!
`
const drawText = 
`Click to set the corners of a custom shaped object and press "c" to finish it!
`
const editCamText = 
`Left/rightclick to change number of rays, arrow keys to move the camera, move the mouse to change the dir of the camera and press "space" to lock the camera!
`