const editObjBtnHoverText = 
`
Select and edit Objects on the 2D-Canvas!
<br>
[more detailed controls below]
`

const rectBtnHoverText = 
`
Drag to add a rectangle!
<br>
[more detailed controls below]
`

const circleBtnHoverText = 
`
NOT IMPLEMENTED!
`

const drawBtnHoverText = 
`
Click to add a corner, press 'c' to finish!
<br>
[more detailed controls below]
`

const addLightBtnHoverText =
`
Click to add a light!
<br>
[more detailed controls below]
`

const editCamBtnHoverText = 
`
More advanced controls for the camera!
<br>
[more detailed controls below]
`