const cursor = document.getElementById("cursor")

const canvas2d = document.getElementById('canvas2d')
const baseZoom = 1.5
let canvasZoom = baseZoom
const instructionText = document.getElementById('instructionText')

// recursion of the ray (number of reflections / refr)
let maxRec = 10

// min influence to add poi and objHit
const minInfluence = 0.05

const objects = [
  // LEAVE THIS IN!!! this is the canvas border, it has to be an object or you trace rays into nowhere
  [[0.0,0.0],[canvas2d.width,0.0,],[canvas2d.width,canvas2d.height],[0.0,canvas2d.height]],
  // GIVE ALL POINTS IN THE OBJECT AS ARRAYS (all paths are being drawn closed)
  // this is draw canvas pos
  [[100.0,50.0],[100.0,150.0],[200.0,150.0],[200.0,50.0]]
]

const objZLimits = [
  // LEAVE THIS IN!!! this is the canvas border, it has to be an object or you trace rays into nowhere
  [-1000, 1000],
  [250,750]
]

const objRotations = [
  // LEAVE THIS IN!!! this is the canvas border, it has to be an object or you trace rays into nowhere
  0,
  // store the current rotation of the element, the stored points are alreadyrotated, this is mostly for
  // drawing the rotation-line (to drag-rotate) and for the reset-rotation option
  0
]

// this is the points you can grab on a highlighted OBJ to rotatate it  ->  ----o
let rotationGrabPoint = [0, 0]
let centerOfHighlighted = [0, 0]
const radiusOfGrabPoint = 10
let draggingRotationGrabPoint = false

const objMaterials = [
  // LEAVE THIS IN!!! this is the canvas border, it has to be non-refl/refr or you trace rays into nowhere
  [0,0,0.7],
  // materials are made with the following parameters:
  // [reflectiveness, refractiveness, refractive index]
  // example:
  // reflect = 0.5 -> a new ray will be traced and the color it gets is half of the pixel color
  // reflect = 1   -> perfect mirror
  [1,0,1.5]
]

const objFactors = [
  // LEAVE THIS IN!!! this is the canvas border, it has to be non-refl/refr or you trace rays into nowhere
  // i mean it isnt called anyways but it would mess up my counting ^^
  [0,0,0,0,false],
  // [Kd (factor diffuse), Ks (factor specular), n (specular exponent), Ka (factor ambient), highlightObjColor]
  // highlightObjColor is a boolean that is true if the highlight is to be multiplied by the object color
  // https://en.wikipedia.org/w/index.php?title=Specular_highlight&oldid=1073412464
  standardObjFactors()
]

const objColors = [
  // store as array [r,g,b]
  // LEAVE THIS IN!!! this is the canvas border, it has to be an object or you trace rays into nowhere
  rgbStringToArray(window.getComputedStyle(canvas2d).backgroundColor),
  [255,0,0]
]

const radiusLight = 30  // radius of the lamp itself

const ambientLight = [255,255,255]

const lights = [
  // GIVE [X,Y,Z] (Z has no effect in 2d)
  [600,100,500]
]
const lightColors = [
  [255,255,255]
]

const cameraPos = [
  // GIVE X,Y,Z (Z has no effect in 2d)
  500,400,500
]
const cameraDir = [
  // GIVE X,Y,Z (Z has no effect in 2d)
  -1,-1,0
]
const distToScreen = 100
var numberOfRays = 1
const maxRays2d = 30

// variables used for drawing
let startPos = [0.0,0.0]  // set at mouseDown
let hoverPos = [0.0,0.0]
let drawArray = []        // it is used to store temp points of draw / the hover of addRect
let objInClipboard = {
  points: [],
  color: [],
  materials: [],
  factors: [],
  rotation: 0
}                         // it is used to hold an item to be copied
let highlighted = -1      // what object is highlighted by edit
let highlightedLight = -1 // what LightSource is highlighted by editLight
let lastMousePos = [0.0,0.0]  // used for move

var holdKeySpeedup = 1    // used to make movement faster the longer you hold a key

var lampStyle = 0         // 0 -> plain; 1 -> sunRays; 2 -> x shape through lamp
const lampStyleNames = ["plain", "sun", "cross"]
const numberOfLampStyles = 3
const lightRayLength = 1.3  // multiplay this by radius of light to get ray length 