// add a POI like:
// [x, y, POItype, display]
// display is true or false -> some POI are rendered in the scene and i dont want to draw over them
// POItype is a NR to show which HTML to load in to the popup
// 0 -> free shadowRay              [..., objNR, lightNR]
// 1 -> blocked shadowRay           [..., objNR, lightNR]
// 2 -> hitpoint of ray on object   [..., objNR, color_at_pos, phi, theta]
// 3 -> shadow ray hits transparent object [refractiveness, lightfromDir]
let pointsOfInterest = []
let lastPointsOfInterest = []   // made so i can select while POI are empty (each frame they get reset)
let chosenPOI = -1
let showPOI = false
// array of arrays
// [[lightNr1, lnr2, ...]]
// KEEP THIS MATCHING WITH THE "pointsOfInterest" or you will run into issues
// add one per poi
let lightsForPOI = []
let lastLightsForPOI = []
// ==========================================================================================
let formulaDisplay = false
// ==========================================================================================
// save angles for POI texts
let phi = 0
let theta = 0
