function get_POI_popup_text(POInr) {
    const currentPOi = lastPointsOfInterest[POInr]
    switch(currentPOi[2]) {
        case 0:
            return shadowRayPOI_innerHTML(false, currentPOi[4], currentPOi[5])
        case 1:
            return shadowRayPOI_innerHTML(true, currentPOi[4], currentPOi[5])
        case 2:
            return phongOnHit_innerHTML(currentPOi[4], POInr, currentPOi[5])
        case 3:
            return shadowRay_transparentObj_POI_innerHTML(currentPOi[5], POInr)
        default:
            console.log("Unkown POInr:", POInr)
            return "TODO"
    }
}

function shadowRayPOI_innerHTML(blocked, objNR, lightNR) {
    let notA =`not`
    let notB = ''
    let actualValues = ''

    if (formulaDisplay) {
        actualValues += 'with light $' + getMathJaxVector(lightColors[lightNR]) + '$'
    }

    if (blocked) {
        notA = ``
        notB = `not`
    }
    return `Shadow rays are used to determine if a point on an object is lit up by a lightsource.
        In Raytracing these rays go from each point where the camera-ray hits an object to each lightsource.
        The shadowray from $O_` + objNR + `$ to $L_` + lightNR +  `$ has ` + notA +  ` been blocked by an object, therfore the lightsource ` + actualValues + ` does ` + notB + ` contribute to the color of this part of the object`
}

function phongOnHit_innerHTML(objNR, POInr, color_at_pos) {
    let k_d = objFactors[objNR][0]
    let k_s = objFactors[objNR][1]
    let n = objFactors[objNR][2]
    let k_a = objFactors[objNR][3]
    let r = objColors[objNR][0]
    let g = objColors[objNR][1]
    let b = objColors[objNR][2]

    let anglePhi = Math.acos(lastPointsOfInterest[POInr][6])
    let angleTheta = Math.acos(lastPointsOfInterest[POInr][7])

    if (formulaDisplay) {
        let sumText = ''
        for (let light of lastLightsForPOI[POInr]) {
            sumText +=  '+ ['
            sumText +=  `( ` + getMathJaxVector(light) + ` * ` + k_d + ` * cos(` + round2(radiansToDegrees(anglePhi)) + `^\\circ) *
                ` + getMathJaxVector([r,g,b]) + ` ) + ( ` + getMathJaxVector(light) + ` * ` + k_s +
                ` * cos^{` + n + `}(` + round2(radiansToDegrees(angleTheta)) + `^\\circ) )`
            sumText += `]` + '\\\\'
        }

        return 'Equation with symbols:' +
            `$$ I_{out} = I_a * K_a * C_o + \\sum_{m \\in L}[( I_{in} * K_d * cos(\\phi)  * C_o ) + ( I_{in} * K_s * cos^\\alpha(\\theta) )] $$` +
            'Equation with current values:' +
            `$$ ` + getMathJaxVector(color_at_pos) + ` = ` + getMathJaxVector(ambientLight) + ` * ` + k_a + ` * ` + getMathJaxVector([r,g,b]) + ` ` +
            sumText + `$$`
    } else {
        return `At this point a camera ray has hit object $O_` + objNR + `$. To determine which color this point of the object should have, 
            you have to look at both the objects properties and the light coming in from all lightsources.
            This can be done using the formula of Phong-lighting: ` + 
            `$$ I_{out} = I_a * K_a * C_o + \\sum_{m \\in L}[( I_{in} * K_d * cos(\\phi)  * C_o ) + ( I_{in} * K_s * cos^\\alpha(\\theta) )] $$` +
            `<img src="./images/angles2.jpg" style="width: 30rem;height:17.5rem;margin-left:auto;margin-right:auto;display:block;"></img>`
    }
}

function shadowRay_transparentObj_POI_innerHTML(incLight, POInr) {
    if (!formulaDisplay) {
        return `At this point the shadow ray hits a transparent object, the light is not completely blocked, but will be reduced based on color and
        refractiveness of the object.`
    } else {
        return `At this point the shadow ray hits a transparent object, the light is not completely blocked, but will be reduced based on color and
        refractiveness of the object. The original color of the light ` + getMathJaxVector(lightColors[POInr]) + ` is now reduced
        to ` + getMathJaxVector(incLight)
    }
}

function containsFloat(vec) {
    for(v of  vec) {
        if(isFloat(v)) {
            return true
        }
    }
    return false
}

function getMathJaxVector(vec) {
    if (containsFloat(vec)) {
        for (let i = 0; i < vec.length; i++) {
            vec[i] = Math.round(vec[i])
        } 
    }

    let variableText = vec[0]
    for (v of vec.slice(1)) {
        variableText += ` \\\\` + v
    }
    return `\\begin{bmatrix}` + variableText +`\\end{bmatrix}`
}