function applyTemplate(id) {
    let template
    switch (id) {
        case 'templateBTN2':
            template = template2
            break;
        default:
            template = template1
    }
    if(template) {
        setFullState(template)
        setUpButtonTexts()
    } else {
        console.log('no template found!')
    }
}

// how to create template:
// 1. click "export"
// 2. "as JSON"
// open .txt file
// copy paste: "const <templateName> <file contents here>"
// create template button (with new id)
// add the template to applyTemplate
// see loadFullState.js to see every value needed for the state
const template1 = {
    "bools":[true,false,false,true,true,false],
    "canvasColors":{"rayColor":[255,128,0],"shadowRayColor":[100,100,100],"shadowRayBlockedColor":[100,0,0]},
    "objects":[[[0,0],[1026,0],[1026,719],[0,719]],[[100.5,46.53235908141977],[100.5,681.4739039665973],[190.5,681.4739039665973],[190.5,46.53235908141977]]],
    "objZLimits":[[-1000, 1000], [250,750]],
    "objRotations":[0,0,0,0,0,0,0,0,0,0,0,0,0],"objMaterials":[[0,0,0.7],[0.25,0,1.5]],
    "objFactors":[[0,0,0,0,false],[0.6,0.7,25,0.3,false]],
    "objColors":[[255,255,255],[0,0,255]],
    "lights":[[661.5,64.54488517745301,500]],"lightColors":[[255,255,255]],"cameraPos":[500,400,500],
    "cameraDir":[-182,-0.7223382045929156,0],"objectOptions":[20,2],"showPOI":false,"canvasBackground":"#ffffff",
    "canvasZoom":1.5,"continuous3D":false,"useCPU":true,"size3d":0.5,"fovStep":0.7,"rayAlphaOption":0,"rayColorOption":0,
    "maxRec":10,"showBtnHoverTexts":true
}

const template2 = {
    "bools":[true,true,false,true,false,false],
    "canvasColors":{"rayColor":[255,128,0],"shadowRayColor":[100,100,100],"shadowRayBlockedColor":[100,0,0]},
    "objects":[[[0,0],[1026,0],[1026,674],[0,674]],[[100,100],[100,200],[200,200],[200,100]],[[277.5,24.01781737193764],[277.5,49.53674832962138],
    [450,49.53674832962138],[450,24.01781737193764]],[[867,438.32516703786183],[867,537.3986636971047],[910.5,537.3986636971047],[910.5,438.32516703786183]],
    [[686.095814003288,616.3432991042937],[687.9284414889632,682.3668674969052],[750.904185996712,680.6188389803389],[749.0715585110368,614.5952705877274]]],
    "objZLimits":[[-1000, 1000], [250,750], [250,750], [250,750], [250,750]],
    "objRotations":[0,0,0,0,6.255435261370071],"objMaterials":[[0,0,0.7],[0.2,0,1.5],[0.25,0,1.5],[0.25,0,1.5],[0.25,0,1.5]],
    "objFactors":[[0,0,0,0,false],[0.6,0.7,25,0.9,false],[0.6,0.7,25,0.9,false],[0.6,0.7,25,0.9,false],[0.6,0.7,25,0.9,false]],
    "objColors":[[0,0,0],[0,0,0],[0,8,255],[0,8,255],[0,8,255]],
    "lights":[[600,100,500]],"lightColors":[[255,255,255]],"cameraPos":[500,400,500],"cameraDir":[-275,-224.36971046770603,0],
    "objectOptions":[1,0],"showPOI":false,"canvasBackground":null,
    "canvasZoom":1.5,"continuous3D":false,"useCPU":true,"size3d":0.5,"fovStep":0.7,"rayAlphaOption":0,"rayColorOption":0,
    "maxRec":10,"showBtnHoverTexts":true
}
