/*
IMPORTANT:
ALL CHANGES HERE AFFECT all setStateFunctions files ESPECIALLY the importTemplates need to be changed!!!
bools.js
    save all bools from the buttons/options (and editmode?)
    DO NOT save the other actives or menuOpen
canvas2dColors.js
    save rayColors
objects.js
    save everything except:
    DO NOT save any const that isnt an arrray   (they are hardcoded)
    DO NOT save anything that is document.getElement...
    DO NOT save GPU
    DO NOT save variables used for drawing
    DO NOT save: holdKeySpeedup, recCount, rotationGrabPoint, centerOfHighlighted, draggingRotationGrabPoint
pointsOfInterest.js
    save [showPOI, chosenPOI???]
*/

// TODO: https://stackoverflow.com/questions/13292744/why-isnt-localstorage-persisting-in-chrome <- test?
// TODO: chosenPOI????

function setFullState(state_JSON) {
  const bools = state_JSON.bools
  showRay = bools[0]
  showShadowRay = bools[1]
  showNormals = bools[2]
  showObjHits = bools[3]
  showEnergyPreservationTerm = bools[4]
  displayExplainBoxes = bools[5]
  copyToConstObj(rayColors, state_JSON.canvasColors)
  copyToConstArray(objects, state_JSON.objects)
  copyToConstArray(objZLimits, state_JSON.objZLimits)
  copyToConstArray(objRotations, state_JSON.objRotations)
  copyToConstArray(objMaterials, state_JSON.objMaterials)
  copyToConstArray(objFactors, state_JSON.objFactors)
  copyToConstArray(objColors, state_JSON.objColors)
  copyToConstArray(lights, state_JSON.lights)
  copyToConstArray(lightColors, state_JSON.lightColors)
  copyToConstArray(cameraPos, state_JSON.cameraPos)
  copyToConstArray(cameraDir, state_JSON.cameraDir)
  const objectOptions = state_JSON.objectOptions
  numberOfRays = objectOptions[0]
  lampStyle = objectOptions[1]

  showPOI = state_JSON.showPOI
  canvasZoom = state_JSON.canvasZoom

  maxRec = state_JSON.maxRec
  rayAlphaOption = state_JSON.rayAlphaOption
  rayColorOption = state_JSON.rayColorOption

  const background2d = state_JSON.canvasBackground
  var root = document.querySelector(':root')
  root.style.setProperty('--canvas2dBackround', background2d)
  objColors[0] = rgbStringToArray(window.getComputedStyle(canvas2d).backgroundColor)
  localStorage.setItem('canvasBackground', JSON.stringify(background2d))

  continuous3D = state_JSON.continuous3D
  useCPU = state_JSON.useCPU
  size3d = state_JSON.size3d
  fovStep = state_JSON.fovStep
  showBtnHoverTexts = state_JSON.showBtnHoverTexts

  correctCanvasInnerSize(movecam=false)
  changes.length = 0
}

function getFullState() {
  return {
    bools: [showRay, showShadowRay, showNormals, showObjHits, showEnergyPreservationTerm, displayExplainBoxes],
    canvasColors: rayColors,
    objects: objects,
    objZLimits: objZLimits,
    objRotations: objRotations,
    objMaterials: objMaterials,
    objFactors: objFactors,
    objColors: objColors,
    lights: lights,
    lightColors: lightColors,
    cameraPos: cameraPos,
    cameraDir: cameraDir,
    objectOptions: [numberOfRays, lampStyle],
    showPOI: showPOI,
    canvasBackground: JSON.parse(localStorage.getItem('canvasBackground')),
    canvasZoom: canvasZoom,
    maxRec: maxRec,
    rayAlphaOption: rayAlphaOption,
    rayColorOption: rayColorOption,
    continuous3D: continuous3D,
    useCPU: useCPU,
    size3d: size3d,
    fovStep: fovStep,
    showBtnHoverTexts: showBtnHoverTexts
  }
}
