const changes = []

let changesCurrentPos = 0
let redoBtn = document.getElementById('redoBtn')
let undoBtn = document.getElementById('undoBtn')

// Syntax:
// ['attrName', previousValue, newValue]

function setStateIteration(undoredo) {
    // undoredo:
    // 1 -> redo
    // -1 -> undo
    if ((undoredo == -1 && changesCurrentPos > 0) || (undoredo == 1 && changesCurrentPos < changes.length)) {
        let val = ['default']
        if (undoredo == -1) {
            changesCurrentPos -= 1
            val = getDeepCopy(changes[changesCurrentPos][1])
            redoBtn.disabled = false
        } else {
            // doing this above can be an error if currPos == changes.length (which is the standard)
            val = getDeepCopy(changes[changesCurrentPos][2])
        }

        switch (changes[changesCurrentPos][0]) {
            case 'fullObjects':
                copyToConstArray(objects, val.objects)
                copyToConstArray(objZLimits, val.objZLimits)
                copyToConstArray(objRotations, val.objRotations)
                setGrabPoint()
                copyToConstArray(objMaterials, val.objMaterials)
                copyToConstArray(objFactors, val.objFactors)
                copyToConstArray(objColors, val.objColors)
                if (highlighted >= objects.length) {
                    unsetHighlightObj()
                }
                break
            case 'fullLights':
                copyToConstArray(lights, val.lights)
                copyToConstArray(lightColors, val.lightColors)
                if (highlightedLight >= lights.length) {
                    unsetHighlightLight()
                }
                break
            case 'fullCamera':
                copyToConstArray(cameraPos, val.cameraPos)
                copyToConstArray(cameraDir, val.cameraDir)
                break
            case 'showRay':
                toggleShowRay()
                break
            case 'showShadowRay':
                toggleShowShadowRay()
                break
            case 'showNormals':
                toggleShowNormals()
                break
            case 'showObjHits':
                toggleShowObjHits()
                break
            case 'showEnergyPreservationTerm':
                toggleShowPreserveEnergy_helper()
                break
            case 'displayExplainBoxes':
                toggleShowFormulas()
                break
            case 'canvasColors':
                copyToConstObj(rayColors, val)
                break
            case 'objects':
                copyToConstArray(objects, val)
                setGrabPoint()
                break
            case 'objZLimits':
                copyToConstArray(objZLimits, val)
                break
            case 'objRotations':
                copyToConstArray(objRotations, val)
                setGrabPoint()
                break
            case 'objMaterials':
                copyToConstArray(objMaterials, val)
                if(highlighted > 0) {
                    document.getElementById('inputObj_refl').value = objMaterials[highlighted][0] * 100
                    document.getElementById('inputObj_refr').value = objMaterials[highlighted][1] * 100
                }
                break
            case 'objFactors':
                copyToConstArray(objFactors, val)
                break
            case 'objColors':
                copyToConstArray(objColors, val)
                break
            case 'lights':
                copyToConstArray(lights, val)
                break
            case 'lightColors':
                copyToConstArray(lightColors, val)
                break
            case 'cameraPos':
                copyToConstArray(cameraPos, val)
                break
            case 'cameraDir':
                copyToConstArray(cameraDir, val)
                break
            case 'numberOfRays':
                numberOfRays = val
                break
            case 'objectOptions':
                objectOptions = val
                numberOfRays = objectOptions[0]
                lampStyle = objectOptions[1]
                break
            case 'showPOI':
                toggleShowPOI()
                break
            case 'canvasZoom':
                // do nothing
                break
            case 'background2d':
                const background2d = val
                var root = document.querySelector(':root')
                root.style.setProperty('--canvas2dBackround', background2d)
                objColors[0] = rgbStringToArray(window.getComputedStyle(canvas2d).backgroundColor)
                localStorage.setItem('canvasBackground', JSON.stringify(background2d))
                break
            default:
                console.log('[missing changes case] pos:', changesCurrentPos, changes[changesCurrentPos], ' undoredo:', undoredo)
        }
        if (undoredo == 1) {
            changesCurrentPos += 1
        }
        correctCanvasInnerSize(movecam=false)
    }
    if (changesCurrentPos == 0) {
        undoBtn.disabled = true
    } else {
        undoBtn.disabled = false
        if(changesCurrentPos == changes.length) {
            redoBtn.disabled = true
        } 
    }
    updateCurrentEditMenu()
}

function storeChange(attrName, oldValue, newValue) {
    undoBtn.disabled = false
    let pushIt = true
    if (changes.length > 0) {
        let lastChange = changes[changes.length-1]
        if (isRepeatedAction(lastChange[0], attrName)) {
            pushIt = false
            lastChange[2] = getDeepCopy(newValue)      
        }
    }

    if(pushIt) {
        // this line prevents all redos by cutting the changes Array when a new change is made
        changes.length = changesCurrentPos
        redoBtn.disabled = true
        changes.push(getDeepCopy([attrName, oldValue, newValue]))
        changesCurrentPos += 1
    }
}

function isRepeatedAction(oldName, newName) {
    // sometimes there are repeated actions like dragging an object
    // in that case dont push every change just edit the last one
    return (oldName === newName) && ['objects', 'lights', 'objColors', 'cameraPos', 'cameraDir', 'fullCamera', 'background2d', 'objMaterials'].includes(newName)
}

function getObjectsFull() {
    return getDeepCopy({
        objects: objects,
        objZLimits: objZLimits,
        objColors: objColors,
        objMaterials: objMaterials,
        objFactors: objFactors,
        objRotations: objRotations
    })
}

function getLightsFull() {
    return getDeepCopy({
        lights: lights,
        lightColors: lightColors
    })
}

function getCameraFull() {
    return getDeepCopy({
        cameraPos: cameraPos,
        cameraDir: cameraDir
    })
}