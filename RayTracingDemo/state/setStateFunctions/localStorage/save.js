function saveFullStateToLocalStorage() {
    let state = getFullState()
    for (const [key, value] of Object.entries(state)) {
        localStorage.setItem(key, JSON.stringify(value))
    }

    if(dontSaveLocalStorage) {
        localStorage.clear()
    }
}

function saveCanvasBackgroundToLocalStorage(input) {
    // input is rgb string: rgb(208, 11, 11)
    localStorage.setItem('canvasBackground', JSON.stringify(rgbStringToHex(input)))
}

function saveWithoutLocalStorage() {
    dontSaveLocalStorage = true
    location.reload()
}
