function loadFullStateFromLocalStorage() {
    if (localStorage.length > 0) {
        let state_JSON = {}
        for (var i = 0; i < localStorage.length; i++) {
            state_JSON[localStorage.key(i)] = JSON.parse(localStorage.getItem(localStorage.key(i)))
        }
        setFullState(state_JSON)
    }

    if(useCPU) {
        continuous3D = false
    }
}
