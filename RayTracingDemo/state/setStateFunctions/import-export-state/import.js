function loadFullStateFromJSON(event) {
    const file = event.target.files[0]
    if (file) {
        var reader = new FileReader()
        reader.readAsText(file, "UTF-8")
        reader.onload = function (evt) {
            const json = JSON.parse(evt.target.result)
            setFullState(json)
        }
    }
}
