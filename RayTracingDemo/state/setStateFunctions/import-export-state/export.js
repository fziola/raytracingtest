function saveFullStateToJSON() {
    const JSON_OBJECT = getFullState()
    saveJSON(JSON.stringify(JSON_OBJECT), 'state_JSON.txt')
}

function saveJSON(content, fileName, contentType='text/plain') {
    var a = document.createElement("a")
    var file = new Blob([content], {type: contentType})
    a.href = URL.createObjectURL(file)
    a.download = fileName
    a.click()
}
