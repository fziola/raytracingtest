var moveActive = false                  // this is so that i can keep objects selected without them following mouse

// button bools
var editActive = false                  // this is active if any action (add / draw / delete) is being performed on canvas2d
var selectionModeActive = false         // this is for editing the objects and light and camera
var addRectActive = false
var addCircleActive = false
var drawActive = false
var addLightActive = false
var highlightedCam = false
var dragCamActive = false               // if selectionmode is active and you clicked on cam, you can drag it
var editCamActive = false               // rotate camera and change number of rays

// top menu
var showBtnHoverTexts = true
var rayAlphaOption = 0                        // 0 -> normal (always 1); 1 -> lower with recursion; 2 -> lower with influence over screen color
var maxRayAlphaOption = 3

var rayColorOption = 0                        // 0 -> standard; 1 -> in color of current light
var maxRayColorOption = 2

// draw variables
var showRay = true
var showShadowRay = true
var showNormals = false
var showObjHits = true                  // shows little circles on the obj surface displaying the color of that surface
var showEnergyPreservationTerm = false
var showObjHitOnCanvas = false          // when an object hits canvas border -> obj 0 -> black circle to show that it leaves the canvas

var displayExplainBoxes = false

var menuOpen = false
var keyInputLocked = false

var dontSaveLocalStorage = false

var measureTime_2d = false               // remove this, it is only to test and should not be accessible to the user
var totalMeasuredTime = 0               // remove this, it is only to test and should not be accessible to the user
var timesMeasured = 0                   // remove this, it is only to test and should not be accessible to the user