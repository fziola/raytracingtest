// const gpu = new GPU()
// const canvas3d = document.getElementById('canvas3d')

const distTo_3D_Screen = 30 // dist to the center of the 2d screen // todo not hardcode?
const epsilon = Math.pow(2, -20)  // normal epsilon is 2^-52 BUT this is too small, random js errors get bigger then that

let debug_3d = false

const info_3d = {
  hits: [],   // see intersect3D_ray_all returns
  pixelPos: [],
  pixelColor: []
}