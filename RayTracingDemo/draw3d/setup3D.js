function get3DscreenGPU(w, h) {
  const scrDist = distTo_3D_Screen
  const center = [Math.round(w/2), Math.round(h/2)]
  const camAngle = angle2d([1,0], normalize2d(cameraDir.slice(0,-1)))
  const objFaces = getAllObjectsFaces()

  let screen3D = gpu.createKernel(function(scrDist, center, camAngle, objFaces) {
    const relativeX =  (this.thread.x - center[0]) * this.constants.fovStep
    const relativeY  = (this.thread.y - center[1]) * this.constants.fovStep
    // we assume at first our camera is just looking in [1,0,0] direction and is positioned at [0,0,0]
    let dir = rotate3D_z_axis([scrDist, relativeX, relativeY], camAngle)
    let pos3D = addVector(this.constants.cameraPos, dir)

    let hitPoint_objNr = intersect3D_ray_all(pos3D, dir, objFaces)

    let color = this.constants.objColors[0]
    if(hitPoint_objNr) {
      color = this.constants.objColors[hitPoint_objNr[1]]
    }
    this.color(color)
  }).setConstants({
    fovStep: fovStep,
    objColors: objColors,
    cameraPos: cameraPos
  }).setOutput([w, h]).setGraphical(true)

  screen3D(scrDist, center, camAngle, objFaces)
  return new ImageData(screen3D.getPixels(), w, h)
}

function get3DscreenCPU(w, h) {
  const scrDist = distTo_3D_Screen
  const center = [Math.round(w/2), Math.round(h/2)]
  const camAngle = angle2d([1,0], normalize2d(cameraDir.slice(0,-1)))
  const pixels = []
  const objFaces = getAllObjectsFaces()
  const proportionalFovStep = multVectorNumber([1,1], fovStep)

  for(let y = 0; y < h; y++) {
    for(let x = 0; x < w; x++) {
      const relativeX = (x - center[0]) * proportionalFovStep[0]
      const relativeY = ((h-1-y) - center[1]) * proportionalFovStep[1]  // (h-1-y) weil screen bei 0,0 beginnt oben links -> in Koord. System aber oben = hohes y
      // we assume at first our camera is just looking in [1,0,0] direction and is positioned at [0,0,0]
      let dir = rotate3D_z_axis([scrDist, relativeX, relativeY], camAngle)
      let pos3D = addVector(cameraPos, dir)

      let color = castRay3d(pos3D, dir, 0, objFaces, 0)

      pixels.push(color[0])
      pixels.push(color[1])
      pixels.push(color[2])
      pixels.push(255)  // alpha is 0 to 255
    }
  }

  return new ImageData(new Uint8ClampedArray(pixels), w, h)
}

function getPixel3D(event) {
  debug_3d = true
  info_3d.hits = []
  info_3d.pixelColor = []

  const objFaces = getAllObjectsFaces()
  const proportionalFovStep = multVectorNumber([1,1], fovStep)
  const scrDist = distTo_3D_Screen
  let w = Math.round(size3d * canvas3d.clientWidth)
  let h = Math.round(size3d * canvas3d.clientHeight)
  const center = [Math.round(w/2), Math.round(h/2)]
  const camAngle = angle2d([1,0], normalize2d(cameraDir.slice(0,-1)))

  let x = event.offsetX * size3d
  let y = event.offsetY * size3d

  const relativeX = (x - center[0]) * proportionalFovStep[0]
  const relativeY = ((h-1-y) - center[1]) * proportionalFovStep[1]  // (h-1-y) weil screen bei 0,0 beginnt oben links -> in Koord. System aber oben = hohes y
  // we assume at first our camera is just looking in [1,0,0] direction and is positioned at [0,0,0]
  let dir = rotate3D_z_axis([scrDist, relativeX, relativeY], camAngle)
  let pos3D = addVector(cameraPos, dir)

  let color = castRay3d(pos3D, dir, 0, objFaces, 0)
  const pixel = [color[0],color[1],color[2],255]

  info_3d.pixelPos = pos3D
  info_3d.pixelColor = pixel

  setPopup3D(event.x, event.y)
  debug_3d = false
}
