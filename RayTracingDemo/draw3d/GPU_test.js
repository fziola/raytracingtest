// you can remove this file - it is only for test purposes

function generateMatrices() {
  const matrices = [[], []]
  for (let y = 0; y < 512; y++){
    matrices[0].push([])
    matrices[1].push([])
    for (let x = 0; x < 512; x++){
    matrices[0][y].push(Math.random())
    matrices[1][y].push(Math.random())
    }
  }
  return matrices
}

function multiplyMatrix(a, b) {
  let multiplyMatrixV = gpu.createKernel(function(a, b) {
    let sum = 0
    for (let i = 0; i < 512; i++) {
      sum += a[this.thread.y][i] * b[i][this.thread.x]
    }
    return sum
  }).setOutput([512, 512])
  //console.log(multiplyMatrixV(a, b))
  return multiplyMatrixV(a, b)
}

function genMatrix(w, h) {
  const matrix = []
  for (let x = 0; x < w; x++) {
    matrix.push([])
    for (let y = 0; y < h; y++) {
      matrix[x].push([0,0,0])
    }
  }
  return matrix
}

function randomMatrix(w, h) {
  // https://observablehq.com/@fil/the-gpu-js-loop
  v = Math.sin(Date.now() / 500) ** 2

  let matrix = gpu.createKernel(function(v) {
    function getV(v) {
      return v
    }

    this.color(this.thread.x / 500, this.thread.y / 500, getV(v))
  }).setOutput([w, h]).setGraphical(true)

  matrix(v)
  return new ImageData(matrix.getPixels(), w, h)
}

function testGPU(ve) {
  return ve[0]
}

function get3DscreenGPU_B(w, h) {
  const scrDist = distTo_3D_Screen
  const center = [Math.round(w/2), Math.round(h/2)]
  const camAngle = angle2d([1,0], normalize2d(cameraDir.slice(0,-1)))
  const objFaces = getAllObjectsFaces_GPU()
  const faces = objFaces.faces
  const faceBorders = objFaces.objBorders

  const screen3D = gpu.createKernel(function(faces, faceBorders) {
    const relativeX =  (this.thread.x - this.constants.center[0]) * this.constants.fovStep
    const relativeY  = (this.thread.y - this.constants.center[1]) * this.constants.fovStep
    // we assume at first our camera is just looking in [1,0,0] direction and is positioned at [0,0,0]
    let dir = rotate3D_z_axis([this.constants.scrDist, relativeX, relativeY], this.constants.camAngle)

    this.color(1,0,1,1)
  }).setConstants({
    fovStep: fovStep,
    objColors: objColors,
    scrDist: scrDist,
    cameraPos: cameraPos,
    center: center,
    camAngle: camAngle,
    faceBordersLength: faceBorders.length
  }).setOutput([w, h]).setGraphical(true)

  screen3D(faces, faceBorders)
  return new ImageData(screen3D.getPixels(), w, h)
}

    // https://github.com/gpujs/gpu.js

    // // this.constants.cameraPos gives "property not set" but like dont ask me why this works...
    // let camPos = [this.constants.cameraPos[0], this.constants.cameraPos[1], this.constants.cameraPos[2]]
    // let pos3D = addVector_GPU_3d(camPos, dir)

    // this.color(1,0,1,1)
    // .setGraphical(true)

    //https://github.com/jin/raytracer

    // https://github.com/gpujs/gpu.js/#pipelining
    // kernelOne -> getDir

    // while < maxRec:
    // kernelTwo -> gethitP_objNr(startpoint, dir) => [][][x,y,z, objNr]