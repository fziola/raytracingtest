function castRay3d(startPoint, direction, objNrFrom, objFaces, recCount, internal=false) {
  if(recCount >= maxRec) {
    return objColors[objNrFrom]
  }
  recCount += 1

  let hitPoint_objNr_face = intersect3D_ray_all(startPoint, direction, objFaces)
  if(debug_3d) {
    info_3d.hits.push(hitPoint_objNr_face)
  }

  if(!hitPoint_objNr_face) return objColors[objNrFrom]

  let lightingColor = objColors[0]

  if(!internal) {
    let planeNormal = getPlaneNormal(hitPoint_objNr_face[2])
    let phongStartPoint = addVector(hitPoint_objNr_face[0], multVectorNumber(planeNormal, 0.1))
    lightingColor = phongLighting3d(hitPoint_objNr_face[1], phongStartPoint, planeNormal, subVector(startPoint, hitPoint_objNr_face[0]), objFaces)
  }

  return getNewRay3d(hitPoint_objNr_face[0], direction, recCount, hitPoint_objNr_face[1], objFaces, hitPoint_objNr_face[2], lightingColor, internal)
}

function getNewRay3d(hitPoint, incDir, recCount, objNrHit, objFaces, hitFace, localColor, internal=false) {
  // make new rays at the point a ray hits an obj
  // recursion if reflective / refractive surface
  let reflectiveness = objMaterials[objNrHit][0]
  let refractiveness = objMaterials[objNrHit][1]
  let refrColor = null
  let reflColor = [0,0,0]

  let normal = getPlaneNormal(hitFace)
  // i mean the reflectiveness added if total reflection should NOT be added to the local color ONLY the refl ray color
  let addToReflectiveness = 0
  let refrC = [0,0,0]

  if (refractiveness > 0) {
    let from = 0
    let to = objNrHit
    if(internal) {
      from = objNrHit
      to = 0
    }
    let refrDir = refract3d(incDir, normal, from, to)
    if(!refrDir) {
      // total (internal) reflection
      addToReflectiveness = refractiveness
    } else {
      let startPoint = addVector(hitPoint, multVectorNumber(refrDir, 0.01))
      refrColor = castRay3d(startPoint, refrDir, objNrHit, objFaces, recCount, !internal)
      refrC = multVectorNumber(refrColor, refractiveness)
    }
  }
  if(reflectiveness + addToReflectiveness > 0) {
    let reflDir = reflect3d(incDir, normal)
    let startPoint = addVector(hitPoint, multVectorNumber(reflDir, 0.01))
    reflColor = castRay3d(startPoint, reflDir, objNrHit, objFaces, recCount, internal)
  }
  if(internal) {
    let objColorPart = divVectorNumber(objColors[objNrHit], 255)
    return addVector(multVectorsEach(multVectorNumber(reflColor, reflectiveness + addToReflectiveness), objColorPart), refrC)
  }
  return combineColors(reflColor, reflectiveness, refrColor, refractiveness, localColor, objColors[objNrHit])
}
