function intersect3D_ray_all(startPoint, direction, objFaces, maxDist=999999999999999999999999) {
  let dist = maxDist
  let hitPoint = []
  let objnr = -1
  let currentObj = 0
  let face
  objFaces.forEach(object => {
    object.forEach(surface => {
      let hit = lineIntersection_ray_face(surface, startPoint, direction)
      if(hit) {
        let d = vecLength(subVector(startPoint, hit))
        if(d < dist) {
          dist = d
          hitPoint = hit
          objnr = currentObj
          face = surface
        }
      }
    })
    currentObj += 1
  })
  if(dist >= maxDist) {
    // console.log('No 3D intersection found!')
    return false
  }
  return [hitPoint, objnr, face]
}

function getAllIntersectionObjects3D(startPoint, direction, objFaces, maxDist=999999999999999999999999) {
  let objNrs = []
  let currentObj = 0
  objFaces.forEach(object => {
    object.forEach(surface => {
      let hit = lineIntersection_ray_face(surface, startPoint, direction)
      if(hit) {
        let d = vecLength(subVector(startPoint, hit))
        if(d < maxDist && !(objNrs.includes(currentObj))) {
          objNrs.push(currentObj)
        }
      }
    })
    currentObj += 1
  })
  return objNrs
}

function lineIntersection_ray_face(plane, linePoint, lineDirection) {
  // plane is limited by x points (aka face)
  let planeNormal = normalize3d(getPlaneNormal(plane))
  lineDirection = normalize3d(lineDirection)
  let planePoint = plane[0]
  let denom = dotProduct(planeNormal, lineDirection)

  if (Math.abs(denom) < epsilon) {
    // parallel line to plane
    return false
  }

  let t = (dotProduct(planeNormal, planePoint) - dotProduct(planeNormal, linePoint)) / denom
  if(t <= 0) {
    // behind the ray start
    return false
  }
  let hit = addVector(linePoint, multVectorNumber(lineDirection, t))

  if(!pointOnFace(hit, plane)) {
    // point on plane but not face (in between the points)
    return false
  }

  return hit
}

// function shadowRayIntersectionTest3d(firstHit, faces) {
//   let hits = []
//   lights.forEach(l => {
//     // cast a ray to each light source and just see if its blocked
//     let direction = subVector(l, firstHit)
//     hits.push(intersect3D_ray_all(firstHit, direction, faces, vecLength(direction)))
//   })
//   return hits
// }
