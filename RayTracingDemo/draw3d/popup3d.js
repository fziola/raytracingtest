function setPopup3D(x,y) {
  let id = 'popup3dInner'
  document.getElementById(id).innerHTML = getPopup3dText()
  MathJax.Hub.Queue(["Typeset", MathJax.Hub, id]) // reload item with MathJax

  let popup = document.getElementById('popup3d')
  popup.style.display = 'block'
  popup.style.left = x
  popup.style.top = y
}

function getPopup3dText() {
  text = 'the camera at position $' + getMathJaxVector(cameraPos) + '$ $ \\\\ $ sends a ray through the pixel at $' + getMathJaxVector(info_3d.pixelPos) + '\\ $'
  text += `This Ray travels $ \\\\ $`
  for(hit of info_3d.hits) {
    text += 'to position $' + getMathJaxVector(hit[0]) + '$ (object ' + hit[1] + ' $ \\color{' + rgbArrayToString(objColors[hit[1]]) + '}{ ██ } $ ) $ \\\\ $ '
  }
  text += '. The result is the color (rgba) $' + getMathJaxVector(info_3d.pixelColor) + '$.'
  return text
}
