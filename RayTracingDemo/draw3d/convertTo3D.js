function getAllObjectsFaces_GPU() {
  // GPU.JS only accepts 3d input at max, my objFaces is 4d :(
  // so idea is: return all faces without sorting them by obj -> second array with the limits
  let faces = []
  let objBorders = []

  objects.forEach(object => {
    let obj3D = convertRegularObject(object)
    let objFaces = getSurfaces(obj3D)
    objFaces.forEach(face => {
      faces.push(face)
    })
    objBorders.push(objBorders[objBorders.length-1] + objFaces.length)
  })
  return {faces: faces, objBorders: objBorders}
}

function getAllObjectsFaces() {
  let objFaces = []
  let nr = 0
  objects.forEach(object => {
    let obj3D = convertRegularObject(object, objZLimits[nr])
    objFaces.push(getSurfaces(obj3D))
    nr += 1
  })
  return objFaces
}

function convertRegularObject(obj2D, zLimits) {
  let points = []
  // up
  obj2D.forEach(point => {
    points.push([point[0], point[1], zLimits[1]])
  })
  // down
  obj2D.forEach(point => {
    points.push([point[0], point[1], zLimits[0]])
  })
  return points
}

function getSurfaces(obj3D) {
  let surfaces = []
  // based on how 3D objects are created length is always even
  const half = Math.ceil(obj3D.length / 2)
  const firstHalf = obj3D.slice(0, half)
  const secondHalf = obj3D.slice(-half)

  // top
  let top = []
  firstHalf.forEach(point => {
    top.push(point)
  })
  surfaces.push(top)
  // bottom
  let bot = []
  secondHalf.forEach(point => {
    bot.push(point)
  })
  surfaces.push(bot)

  // sides
  for(let i = 0; i < firstHalf.length; i++) {
    surfaces.push([
      firstHalf[i],
      secondHalf[i],
      secondHalf[(i+1)%secondHalf.length],
      firstHalf[(i+1)%firstHalf.length]
    ])
  }

  return surfaces
}