function export_captured_SVG_state() {
    var svg = getNode("svg", {width: canvas2d.width, height: canvas2d.height})

    // console.log(svg_state)
    svg_state.forEach(obj => {
        let values = obj[1]
        switch(obj[0]) {
            case "circles_fill":
                add_circles_fill_ToSvg(svg, values)
                break
            case "circles_outline":
                add_circles_outline_ToSvg(svg, values)
                break
            case "paths":
                add_paths_ToSvg(svg, values)
                break
            case "paths_fill":
                add_paths_fill_ToSvg(svg, values)
                break
            case "arc":
                add_arc_ToSvg(svg, values)
                break
            default:
                console.log("not a valid object to SVG export")
        }
    })
    saveSvg(svg, exportName)
}

// https://stackoverflow.com/questions/23218174/how-do-i-save-export-an-svg-file-after-creating-an-svg-with-d3-js-ie-safari-an
function saveSvg(svgEl, name) {
    svgEl.setAttribute("xmlns", "http://www.w3.org/2000/svg")
    var svgData = svgEl.outerHTML
    var preface = '<?xml version="1.0" standalone="no"?>\r\n'
    var svgBlob = new Blob([preface, svgData], {type:"image/svg+xml;charset=utf-8"})
    var svgUrl = URL.createObjectURL(svgBlob)
    var downloadLink = document.createElement("a")
    downloadLink.href = svgUrl
    downloadLink.download = name
    document.body.appendChild(downloadLink)
    downloadLink.click()
    document.body.removeChild(downloadLink)
}

// https://stackoverflow.com/questions/20539196/creating-svg-elements-dynamically-with-javascript-inside-html
function getNode(n, v) {
    n = document.createElementNS("http://www.w3.org/2000/svg", n)
    for (var p in v)
        n.setAttributeNS(null, p.replace(/[A-Z]/g, function(m, p, o, s) { return "-" + m.toLowerCase(); }), v[p])
    return n
}

function add_circles_fill_ToSvg(svg, inpVal) {
    let center = inpVal[0]
    let color = inpVal[1]
    let rgb = "rgb(" + color[0] + ","+color[1] + "," + color[2] + ")"
    let radius = inpVal[2]
    let opacity = inpVal[3]

    let val = getNode('circle', { cx: center[0], cy: center[1], r: radius, fill: rgb, opacity: opacity })
    svg.appendChild(val)
}

function add_circles_outline_ToSvg(svg, inpVal) {
    let center = inpVal[0]
    let color = inpVal[1]
    let rgb = "rgb(" + color[0] + ","+color[1] + "," + color[2] + ")"
    let radius = inpVal[2]
    let lineWidth = inpVal[3]
    let opacity = inpVal[4]

    let val = getNode('circle', { cx: center[0], cy: center[1], r: radius, stroke: rgb, strokeWidth: lineWidth, fill: "none", opacity: opacity })
    svg.appendChild(val)
}

function pointsToPathString(points) {
    let path ="M " + points[0][0] + " " + points[0][1] + " "

    points.slice(1).forEach(p => {
        path += "L " + p[0] + " " + p[1] + " "
    })

    return path
}

function add_paths_ToSvg(svg, inpVal) {
    let points = inpVal[0]
    let color = inpVal[1]
    let rgb = "rgb(" + color[0] + "," + color[1] + "," + color[2] + ")"
    let lineWidth = inpVal[2]
    let SVG_path = pointsToPathString(points)
    let opacity = inpVal[3]

    let val = getNode('path', { d: SVG_path, stroke: rgb, strokeWidth: lineWidth, fill: "none", opacity: opacity })

    svg.appendChild(val)
}

function add_paths_fill_ToSvg(svg, inpVal) {
    let points = inpVal[0]
    let color = inpVal[1]
    let rgb = "rgb(" + color[0] + ","+color[1] + "," + color[2] + ")"
    let SVG_path = pointsToPathString(points)
    let opacity = inpVal[2]

    let val = getNode('path', { d: SVG_path, fill: rgb, opacity: opacity })

    svg.appendChild(val)
}

function add_arc_ToSvg(svg, inpVal) {
    let fromPoint = inpVal[0][0]
    let toPoint = inpVal[0][1]
    // let center = inpVal[1]
    let radiusArray = inpVal[2]
    let color = inpVal[3]
    let rgb = "rgb(" + color[0] + ","+color[1] + "," + color[2] + ")"
    let lineWidth = inpVal[4]
    let shortOrLong = inpVal[5]
    let clockwise = inpVal[6]
    let opacity = inpVal[7]

    let dStr = "M " + fromPoint[0] + " " + fromPoint[1] + " A " + radiusArray[0] + " " + radiusArray[1] + " 0 " +
        shortOrLong + " " + clockwise + " " + toPoint[0] + " " + toPoint[1]

    let val = getNode('path', { d: dStr, fill: "none", stroke: rgb, strokeWidth: lineWidth, opacity: opacity })

    svg.appendChild(val)
}