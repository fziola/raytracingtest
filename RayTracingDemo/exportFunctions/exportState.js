var exportName = "canvas_export"

// start is activated upon button press [.SVG]
// it will turn capture_SVG on at the start of draw and then false at the end
// -> it will capture 1 frame and save it here
var capture_SVG_start = false
var capture_SVG = false

var svg_state = [
    // "paths", "paths_fill" (ohne linewidth):
    // values = [[pA,pB,...], [r,g,b], lineWidth, alpha]
    // "circles", "circles_outline":
    // values = [centerX, centerY], [r,g,b], radius, (only outline: lineWidth), alpha]
    // "arc"
    // values = [[fromPoint, toPoint], [centerX, centerY], [radiusX, radiusY], [r,g,b], lineWidth, shortOrLong, clockwise, alpha]

    // ["obj", [values]]
]
