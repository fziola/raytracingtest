function hideMiniPopup(id) {
  let tempPopup = document.getElementById(id)
  tempPopup.style.display = "none"
}
  
function showMiniPopup(id) {
  let tempPopup = document.getElementById(id)
  tempPopup.style.display = "block"
}

function displayMiniPopup(id, pos, innerHTML, idSUB="") {
  const bonusoffset = 20
  let tempPopup = document.getElementById(id)
  tempPopup.style.display = "block"
  tempPopup.style.left = `${pos[0]}px`
  tempPopup.style.top = `${pos[1]}px`
  if (idSUB.length > 0) {
    let tempPopupText = document.getElementById(idSUB)
    tempPopupText.innerHTML = innerHTML
    MathJax.Hub.Queue(["Typeset", MathJax.Hub, tempPopupText]) // reload item with MathJax
  } else {
    tempPopup.innerHTML = innerHTML
    MathJax.Hub.Queue(["Typeset", MathJax.Hub, tempPopup]) // reload item with MathJax
  }
}

function displayMiniPopup_inCanvas(id, pos, innerHTML, idSUB="") {
  const bonusoffset = 20
  let tempPopup = document.getElementById(id)
  tempPopup.style.display = "block"
  tempPopup.style.left = `${(pos[0] / canvas2d.width)*canvas2d.clientWidth + canvas2d.offsetLeft + bonusoffset}px`
  tempPopup.style.top = `${(pos[1] / canvas2d.height)*canvas2d.clientHeight + canvas2d.offsetTop + bonusoffset}px`
  if (idSUB.length > 0) {
    let tempPopupText = document.getElementById(idSUB)
    tempPopupText.innerHTML = innerHTML
    MathJax.Hub.Queue(["Typeset", MathJax.Hub, tempPopupText]) // reload item with MathJax
  } else {
    tempPopup.innerHTML = innerHTML
    MathJax.Hub.Queue(["Typeset", MathJax.Hub, tempPopup]) // reload item with MathJax
  }
}
  
function closestPOI(x, y) {
  let minDist = $(window).height() + $(window).width()
  let vL = 1000000000
  let closest = -1
  for (let i = 0; i < lastPointsOfInterest.length; i++) {
    vL = vecLength([lastPointsOfInterest[i][0] - x, lastPointsOfInterest[i][1] - y])
    if (vL < minDist) {
      minDist = vL
      closest = i
    }
  }
  return closest
}
  
function editInputs(x, y) {
  if(editActive) {
    if(addRectActive) {
      startPos = [x,y]
      drawArray = [[x,y],[x,y],[x,y],[x,y]]
    } else if(drawActive) {
      if (startPos != [0.0,0.0]) {
        startPos = [x,y]
      }
      drawArray.push([x,y])
    } else if(selectionModeActive) {
      moveActive = true
      lastMousePos = [x,y]
      let hitObj = hitTest(objects, [x,y])
      let hitLight = hitTestCircle(lights, radiusLight, [x,y])

      let camBB = [cameraPos[1] + 100, cameraPos[0] + 100, cameraPos[1] - 100, cameraPos[0] - 100]
      let hitCam = pointInBoundingBox([x, y], camBB)

      if (highlighted > 0 && hitTestCircle([rotationGrabPoint], radiusOfGrabPoint, [x,y]) != -1) {
        draggingRotationGrabPoint = true
      } else {
        highlightedCam = false
        setHighlightObject(-1) // also opens the edit menu
        setHighlightLight(-1)
        if (hitObj > 0) {
          setHighlightObject(hitObj) // also opens the edit menu
        } else if(hitLight >= 0) {
          setHighlightLight(hitLight) // also opens the edit menu
        } else if(hitCam) {
          highlightedCam = true
          dragCamActive = true
          openCameraMenu()
        } else {
          closeAllEditMenus()
        }
      }
    }
  }
}

function openButtonPopup(x, y) {
  const bonusoffset = 20
  let popup = document.getElementById('buttonPopup')
  popup.style.display = 'block'
  popup.style.left = `${(x / canvas2d.width)*canvas2d.clientWidth + canvas2d.offsetLeft + bonusoffset}px`
  popup.style.top = `${(y / canvas2d.height)*canvas2d.clientHeight + canvas2d.offsetTop + bonusoffset}px`
}

function incHoldKeySpeed() {
  let max = 10
  if(holdKeySpeedup < max) {
    holdKeySpeedup *= 1.5
  } else {
    holdKeySpeedup = max
  }
}

function pasteObjInClipboard() {
  if (objInClipboard.points.length > 2) {
    let c = 0
    let posFound = false
    let objToBecopied = getDeepCopy(objInClipboard)
    while(c < 100 && !posFound) {
      c += 1
      for(let i = 0; i < objInClipboard.points.length; i++) {
        let originalPoint = getDeepCopy(objInClipboard.points[i])
        let point = objToBecopied.points[i]
        if(c%4 == 0) {
          point[0] = originalPoint[0] + (10 * c)
          point[1] = originalPoint[1] + (10 * c)
        } else if(c%4 == 1) {
          point[0] = originalPoint[0] + (10 * c)
          point[1] = originalPoint[1] - (10 * c)
        } else if(c%4 == 2) {
          point[0] = originalPoint[0] - (10 * c)
          point[1] = originalPoint[1] + (10 * c)
        } else {
          point[0] = originalPoint[0] - (10 * c)
          point[1] = originalPoint[1] - (10 * c)
        }
      }
      if(!isOverlap_obj_all(objToBecopied.points)) {
        posFound = true
      }
    }

    addObject_fromJSObj(objToBecopied)
  }
}

function tempLog() {
  console.log("all objArray lengths", objects.length, objMaterials.length, objColors.length)
  console.log("objects", objects)
  console.log("-------------------------")
  console.log("tempLog:", objFactors)
  console.log('objLen:', objColors.length)
  console.log("-------------------------")
}