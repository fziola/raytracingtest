function mouseDown2d(e) {
  const POIpopupID = 'POIexplanationPopUp'
  const POIpopupID_text = 'POIexplanation'
  e.preventDefault()
  pos = getCanvasPos(e)
  x = pos[0]
  y = pos[1]
  // 0 -> left, 1 -> middle, 2 -> right
  mouseBut = e.button
  if(showPOI && mouseBut === 0) {
    const clPOI = closestPOI(x, y)
    if (clPOI != -1) {
      const distVec = [lastPointsOfInterest[clPOI][0] - x, lastPointsOfInterest[clPOI][1] - y]
      if(vecLength(distVec) <= 15) {
        // for current draw radius see function draw2d()
        chosenPOI = clPOI
        displayMiniPopup_inCanvas(POIpopupID, lastPointsOfInterest[clPOI], get_POI_popup_text(clPOI), POIpopupID_text)
      } else {
        chosenPOI = -1
        hideMiniPopup(POIpopupID)
        editInputs(x, y)
      }
    } else {
      chosenPOI = -1
      hideMiniPopup(POIpopupID)
      editInputs(x, y)
    }
  } else if(mouseBut === 0) {
    editInputs(x, y)
  } else if(mouseBut === 1) {
    zoomCanvas(0)
  }
}

function mouseUp2d(e) {
  draggingRotationGrabPoint = false
  pos = getCanvasPos(e)
  x = pos[0]
  y = pos[1]
  // 0 -> left, 1 -> middle, 2 -> right
  mouseBut = e.button
  if (!editCamActive && mouseBut === 2) {
    // editcam has to be off because you control it with rightclick
    openButtonPopup(x, y)
  } else if(editActive) {
    if(addRectActive) {
      sX = startPos[0]
      sY = startPos[1]
      addRectRandomColorMat(x,y,sX,sY)
      startPos = [0.0,0.0]
      deselectAddRect(document.getElementById('rectBut'))
    } else if(selectionModeActive) {
      moveActive = false
      dragCamActive = false
    } else if(addLightActive) {
      addLight(x,y)
    } else if(editCamActive) {
      let oldVal = numberOfRays
      if(mouseBut == 0 && numberOfRays < maxRays2d) {
        numberOfRays += 1
      } else if(mouseBut == 2 && numberOfRays > 1) {
        numberOfRays -= 1
      }
      storeChange('numberOfRays', oldVal, numberOfRays)
    }
    updateAllPopups()
  }
}

function mouseMove2d(e) {
  pos = getCanvasPos(e)
  x = pos[0]
  y = pos[1]

  if(editActive) {
    if(addRectActive && drawArray.length > 3) {
      drawArray[1][1] = y
      drawArray[2] = [x,y]
      drawArray[3][0] = x
    } else if(draggingRotationGrabPoint) {
      let oldObjectsFull = getObjectsFull()
      let oldValue = getDeepCopy(objects)
      let newAngle = angle2d([0, -1], normalize2d(subVector([x, y], centerOfHighlighted)))
      objects[highlighted] = rotateObject2d(objects[highlighted], newAngle - objRotations[highlighted])
      if(isOverlap_objNr_all(highlighted)) {
        copyToConstArray(objects, oldValue)
      } else {
        objRotations[highlighted] = newAngle
        storeChange('fullObjects', oldObjectsFull, getObjectsFull())
      }
    } else if(highlighted != -1 && moveActive) {
      if (changeObjPos(objects[highlighted], [x-lastMousePos[0], y-lastMousePos[1]])) {
        lastMousePos = [x,y]
      }
    } else if(highlightedLight != -1 && moveActive) {
      changeLightPos(lights[highlightedLight], [x-lastMousePos[0], y-lastMousePos[1]])
      lastMousePos = [x,y]
    } else if(dragCamActive) {
      changeCamPos(x-lastMousePos[0], y-lastMousePos[1])
      lastMousePos = [x,y]
    } else if(editCamActive) {
      let oldVal = getCameraFull()
      cameraDir[0] = x-cameraPos[0]
      cameraDir[1] = y-cameraPos[1]
      storeChange('fullCamera', oldVal, getCameraFull())
    }
    if (highlighted > 0 && (draggingRotationGrabPoint || moveActive)) {
      setGrabPoint()
    }
    updateAllPopups()
  }
}

function keyDown(e) {
  if (keyInputLocked) return
  if(!menuOpen) {
    // https://keycode.info/
    if( e.code != "F12") {
      e.preventDefault()  // so space/arrowKeys doesnt scroll the page (and more)
    }

    switch(e.code) {
      case "Delete":
      case "Backspace":
        if(selectionModeActive && highlighted != -1) {
          removeHighlightedObject()
        } else if(selectionModeActive && highlightedLight != -1) {
          removeHighlightedLight()
        }
        break
      case "Space":
        if(selectionModeActive && highlighted != -1) {
          unsetHighlightObj()
        } else if(selectionModeActive && highlightedLight != -1) {
          unsetHighlightLight()
        } else if(selectionModeActive && highlightedCam) {
          highlightedCam = false
        } else {
          // toggle camActive"
          button2d("editCamBut")
        }
        break
      case "ArrowLeft":
        if(selectionModeActive && highlighted != -1) {
          let oldObjectsFull = getObjectsFull()
          let oldValue = getDeepCopy(objects)
          objects[highlighted] = rotateObject2d(objects[highlighted], -0.01 * holdKeySpeedup)
          if(isOverlap_objNr_all(highlighted)) {
            copyToConstArray(objects, oldValue)
          } else {
            objRotations[highlighted] -= 0.01 * holdKeySpeedup
            storeChange('fullObjects', oldObjectsFull, getObjectsFull())
          }
        } else if(selectionModeActive && highlightedCam) {
          rotateCam(-0.01 * holdKeySpeedup)
        } else if(editCamActive) {
          changeCameraPosition(0, -1)
        } else if(!selectionModeActive) {
          offsetCanvas(0, -1*holdKeySpeedup)
        }
        incHoldKeySpeed()
        break
      case "ArrowRight":
        if(selectionModeActive && highlighted != -1) {
          let oldObjectsFull = getObjectsFull()
          let oldValue = getDeepCopy(objects)
          objects[highlighted] = rotateObject2d(objects[highlighted], 0.01 * holdKeySpeedup)
          if(isOverlap_objNr_all(highlighted)) {
            copyToConstArray(objects, oldValue)
          } else {
            objRotations[highlighted] += 0.01 * holdKeySpeedup
            storeChange('fullObjects', oldObjectsFull, getObjectsFull())
          }
        } else if(selectionModeActive && highlightedCam) {
          rotateCam(0.01 * holdKeySpeedup)
        } else if(editCamActive) {
          changeCameraPosition(0, 1)
        } else if(!selectionModeActive) {
          offsetCanvas(0, 1*holdKeySpeedup)
        }
        incHoldKeySpeed()
        break
      case "ArrowUp":
        if(editCamActive) {
          changeCameraPosition(1, -1)
        } else if(!selectionModeActive) {
          offsetCanvas(1, -1*holdKeySpeedup)
        }
        incHoldKeySpeed()
        break
      case "ArrowDown":
        if(editCamActive) {
          changeCameraPosition(1, 1)
        } else if(!selectionModeActive) {
          offsetCanvas(1, 1*holdKeySpeedup)
        }
        incHoldKeySpeed()
        break
      case "KeyC":
        if(drawActive) {
          addDrawnObjRandomColorMat(drawArray)
          deselectDraw(document.getElementById('drawBut'))
        } else if(highlighted > 0 && highlighted < objects.length) {
          // copy object
          objInClipboard = {
            points: getDeepCopy(objects[highlighted]),
            color: getDeepCopy(objColors[highlighted]),
            materials: getDeepCopy(objMaterials[highlighted]),
            factors: getDeepCopy(objFactors[highlighted]),
            rotation: getDeepCopy(objRotations[highlighted])
          }
        }
        break
      case 'KeyV':
        pasteObjInClipboard()
        break
      case "KeyE":
        btnId = "editObjBut"
        button2d(btnId)
        break
      case "KeyR":
        btnId = "rectBut"
        button2d(btnId)
        break
      case "KeyD":
        btnId = "drawBut"
        button2d(btnId)
        break
      case "KeyL":
        tempLog()
        break
      default:
        console.log("what keyDown is ",e.code)
        break
    }
    if (highlighted > 0) {
      setGrabPoint()
    }
    updateAllPopups()
  } else {
    switch(e.code) {
      case "KeyL":
        tempLog()
        break
      case "Escape":
        closeMenus()
        break
    }
  }
}

function keyUp(e) {
  if (keyInputLocked) return
  if(!menuOpen) {
    // https://keycode.info/
    if( e.code != "F12") {
      e.preventDefault()  // so space/arrowKeys doesnt scroll the page (and more)
    }

    switch(e.code) {
      case "ArrowLeft":
      case "ArrowRight":
      case "ArrowUp":
      case "ArrowDown":
        holdKeySpeedup = 1
      default:
        console.log("what keyUp is ", e.code)
        break
    }

    if (highlighted > 0) {
      setGrabPoint()
    }
    updateAllPopups()
  }
}

function onMouseWheel(event) {
  let rect = canvas2d.getBoundingClientRect()
  let boundingBox = [rect.bottom, rect.right, rect.top, rect.left]
  let x = event.x
  let y = event.y
  if (pointInBoundingBox([x, y], boundingBox)) {
    event.preventDefault()
    zoomCanvas(event.deltaY/1000)
    updateAllPopups()
  }
}