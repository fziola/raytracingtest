function refractRay(ctx, objNr, wallVecOld, incDir, point, incObjNr) {
    let wallN = multVectorNumber(normalize2d(rotatepoint2d(wallVecOld, (2*Math.PI) - 1.57079632679)), -1)   // rotate 90 deg to right
    let angle = angle2d(normalize2d(incDir), wallN)
    let outAngle = -1*Math.asin((Math.sin(angle)*objMaterials[incObjNr][2])/objMaterials[objNr][2])
    let outVec = rotatepoint2d(wallN, outAngle)
    if (!outAngle || outAngle > 1) {
        // total reflection should be handled in getNewRay
        return undefined
    } else {
        let firstHit_info = getIntersectionInfo2d_vector(point, outVec)
        let closestObj = firstHit_info[1]
        if(closestObj == 0) {
            return [0,0,0]
        }
        let newVec = [0,0]
        if (closestObj === objNr) {
            // ray leaves the obj -> back to incObjNr
            return getNewRay([0,0,0], incObjNr, firstHit_info[2], firstHit_info[0], outVec, ctx, incObjNr=objNr)
        } else {
            // to test if the first hit obj is completely IN the obj we are coming from,
            // i trace another ray and see what objNr that ray hits
            let firstHit_info_b = getIntersectionInfo2d_vector(firstHit_info[0], outVec)
            let secondClosestObj = firstHit_info_b[1]
            if (closestObj == secondClosestObj) {
                // obj completely inside other obj
            } else {
                // ray to the end of obj -> new objNr
                let distToWall = distToObjWall_nr(point, outVec, objNr)
                newVec = multVectorNumber(outVec, distToWall)
            }
        }

        drawBasicPath(ctx, [point, addVector(point, newVec)], invertArray(objColors[objNr], 255))
    }
}
// stack of objNrs????????
// recCount locally