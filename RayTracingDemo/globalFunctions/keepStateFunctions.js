function setGrabPoint() {
    if(highlighted > 0 && highlighted < objects.length) {
        centerOfHighlighted = [0,0]
        let c = 0

        for (let point of objects[highlighted]) {
            centerOfHighlighted[0] += point[0]
            centerOfHighlighted[1] += point[1]

            c+=1
        }
        centerOfHighlighted[0] /= c
        centerOfHighlighted[1] /= c

        let dist = distToObjWall_nr(centerOfHighlighted, rotatepoint2d([0, -2000000000], objRotations[highlighted]), highlighted)
        const length = dist + 25
        rotationGrabPoint = addVector(rotatepoint2d([0, -length], objRotations[highlighted]), centerOfHighlighted)
    }
}

function isOverlap_obj_all(obj) {
    for (const object of objects.filter((value, index) => obj !== value && 0 !== index)) {
        if(isOverlap_obj_obj(obj, object) || isInside_obj_obj(obj, object)) {
            return true
        }
    }
    return false
}

function isOverlap_objNr_all(objNr) {
    return isOverlap_obj_all(objects[objNr])
}

function isOverlap_obj_obj(objA, objB) {
    for(let a = 0; a < objA.length; a++) {
        for(let b = 0; b < objB.length; b++) {
            if(line_intersect2d(objA[a], objA[(a+1)%objA.length], objB[b], objB[(b+1)%objB.length])) {
                return true
            }
        }
    }
    return false
}

function isInside_obj_obj(objA, objB) {
    if (isInside_point_obj(objA[0], objB)) {
        return true
    }
    if (isInside_point_obj(objB[0], objA)) {
        return true
    }
    return false
}

function isInside_point_obj(point, obj) {
    // cast vec from point into both directions
    if(distToObjWall(point, [1,0], obj) == -1 || distToObjWall(point, [-1,0], obj) == -1) {
        return false
    }
    return true
}