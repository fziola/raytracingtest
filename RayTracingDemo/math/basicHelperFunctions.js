function isFloat(n){
    return Number(n) === n && n % 1 !== 0;
}

function isInt(n){
    return Number(n) === n && n % 1 === 0;
}

function rgbStringToArray(rgbString) {
    // input: "rgb(r,g,b)"
    rgb = rgbString.replace(/[^\d,]/g, '').split(',')
    return [parseFloat(rgb[0]),parseFloat(rgb[1]),parseFloat(rgb[2])]
}

function rgbArrayToString(rgbArray) {
    return `rgb(${rgbArray[0]},${rgbArray[1]},${rgbArray[2]})`
}

function round2(num) {
    return Math.round((num + Number.EPSILON) * 100) / 100
}

function copyToConstArray(to, from) {
    to.length = 0
    from.forEach(el => {
        to.push(el)
    })
}

function copyToConstObj(to, from) {
    const keys = Object.keys(from)
    keys.forEach(key => {
        to[key] = from[key]
    })
}

function getDeepCopy(obj) {
    return JSON.parse(JSON.stringify(obj))
}