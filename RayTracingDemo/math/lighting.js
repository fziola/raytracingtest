function phongLighting2d(ctx, objNr, firstHit, normalInp, vecHitToCamInp, currentRayInfluence) {
    let normal = normalize2d(normalInp)
    let vecHitToCam = normalize2d(vecHitToCamInp)
    let n = objFactors[objNr][2]
    let diffuse = [0, 0, 0]
    let specular = [0, 0, 0]

    const lightsForObjHitPOI = []
    // no shadow ray on canvas border -> pretend ray leaves screen
    if (objNr != 0) {
        let hits = shadowRayIntersectionTest2d(firstHit[0], firstHit[1])
        let c = 0
        hits.forEach(hit => {
            if (Array.isArray(hit)) {
                let vecToLight = normalize2d(subVector([lights[c][0], lights[c][1]], firstHit))
                let vecReflectedLightRay = normalize2d(subVector(multVectorNumber(normal, (2 * vector2dDotProduct(normal, vecToLight))), vecToLight))
                firstHit_objNr_wallVector = getIntersectionInfo2d(firstHit, [lights[c][0], lights[c][1]])
                refractiveness = objMaterials[firstHit_objNr_wallVector[1]][1]
                if(refractiveness > 0) {
                    if(currentRayInfluence > minInfluence) drawBasicPath(ctx, [firstHit_objNr_wallVector[0], firstHit], rayColors.shadowRayColor, 2, showShadowRay)
                    // shadow ray blocked by transparent obj
                    let lightIntensity = castShadowRay(ctx, addVector(firstHit_objNr_wallVector[0], vecToLight), [lights[c][0], lights[c][1]], currentRayInfluence, lightColors[c])
                    lightsForObjHitPOI.push(lightIntensity)
                    pointsOfInterest.push([hit[0], hit[1], 3, true, refractiveness, lightIntensity])
                    lightsForPOI.push([c])
                    for (let i = 0; i < 3; i += 1) {
                        let intensity = lightIntensity[i] / 255
                        diffuse[i] += objFactors[objNr][0] * intensity * Math.max(0, vector2dDotProduct(normal, vecToLight))
                        // the dot product is the cos of the angle ^^
                        specular[i] += objFactors[objNr][1] * intensity * Math.pow(Math.max(0, vector2dDotProduct(vecReflectedLightRay, vecHitToCam)), n)
                    }
                } else if(currentRayInfluence > minInfluence) {
                    // fully blocked shadow ray (only visual code)
                    let tempPath = [firstHit, hit]
                    drawBasicPath(ctx, tempPath, rayColors.shadowRayColor, 2, showShadowRay)
                    pointsOfInterest.push([hit[0], hit[1], 1, true, objNr, c])
                    lightsForPOI.push([c])
                    tempPath = [hit, [lights[c][0], lights[c][1]]]
                    drawBasicPath(ctx, tempPath, rayColors.shadowRayBlockedColor, 2, showShadowRay)
                }
            } else {
                // open shadow ray
                if(currentRayInfluence > minInfluence) {
                    let tempPath = [firstHit, [lights[c][0], lights[c][1]]]
                    const vecToPOI = [lights[c][0] - firstHit[0], lights[c][1] - firstHit[1]]
                    drawBasicPath(ctx, tempPath, rayColors.shadowRayColor, 2, showShadowRay)
                    pointsOfInterest.push([firstHit[0] + (vecToPOI[0]/2), firstHit[1] + (vecToPOI[1]/2), 0, true, objNr, c])
                    lightsForPOI.push([c])
                    lightsForObjHitPOI.push(lightColors[c])
                }

                let vecHitToLight = multVectorNumber(normalize2d(subVector(firstHit, [lights[c][0], lights[c][1]])), -1)
                let vecReflectedLightRay = normalize2d(subVector(multVectorNumber(normal, (2 * vector2dDotProduct(normal, vecHitToLight))), vecHitToLight))

                // drawBasicPath(ctx, [firstHit, [firstHit[0]+vecReflectedLightRay[0]*1000, firstHit[1]+vecReflectedLightRay[1]*1000]], [0,255,0], 2, showShadowRay)
                for (let i = 0; i < 3; i += 1) {
                    let intensity = lightColors[c][i] / 255
                    // for the hitpoint POI in next getNewRay
                    phi = Math.max(0, vector2dDotProduct(normal, vecHitToLight))
                    theta = Math.max(0, vector2dDotProduct(vecReflectedLightRay, vecHitToCam))

                    diffuse[i] += objFactors[objNr][0] * intensity * Math.max(0, vector2dDotProduct(normal, vecHitToLight))
                    // the dot product is the cos of the angle ^^
                    specular[i] += objFactors[objNr][1] * intensity * Math.pow(Math.max(0, vector2dDotProduct(vecReflectedLightRay, vecHitToCam)), n)
                }
            }
            c += 1
        })
        //fillCircle(ctx, 500,500,[255,0,255],10)
        // light ray refl + long n
        // drawBasicPath(ctx, [[firstHit[0], firstHit[1]], [firstHit[0] + lightRefl[0] * 1000, firstHit[1] + lightRefl[1] * 1000]], rayColors.shadowRayColor, lineWidth = 2, showBool = true)
        // drawBasicPath(ctx, [[firstHit[0], firstHit[1]], [firstHit[0] + normal[0] * 200, firstHit[1] + normal[1] * 200]], invertArray(objColors[0], 255), lineWidth = 2, showBool = true)
        // add ambient????
        //console.log("phong:diff:",diffuse)
        //console.log("phong:spec:",specular)
    }

    if(objFactors[objNr][4]) {
        specular = multVectorsEach(specular, divVectorNumber(objColors[objNr], 255))
    }

    if (showEnergyPreservationTerm) {
        for (let spec = 0; spec < specular.length; spec+=1) {
            specular[spec] *= (n + 2) / (2 * Math.PI)
        }
    }
    // this matches with the POI in draw2d.js, created at the pos where the camera ray hits the object
    lightsForPOI.push(lightsForObjHitPOI)

    // reduce light from 0-255 to 0-1 so the mult wont overkill and make everything white
    // divVectorNumber(ambientLight, 255) = multVectorNumber(ambientLight, 0.0039)
    let ambient = multVectorsEach(objColors[objNr], multVectorNumber(multVectorNumber(ambientLight, 0.0039), objFactors[objNr][3]))
    return addVector(addVector(multVectorsEach(diffuse, objColors[objNr]), multVectorNumber(specular, 255)), ambient)
}

function castShadowRay(ctx, p, lightPos, currentRayInfluence, lightColor) {
    let firstHit_objNr_wallVector = getIntersectionInfo2d(p, lightPos)
    let vec = normalize2d(subVector(lightPos, p))

    if(!firstHit_objNr_wallVector) {
        // open shadow ray
        if(currentRayInfluence > minInfluence) {
            let tempPath = [p, lightPos]
            drawBasicPath(ctx, tempPath, rayColors.shadowRayColor, 2, showShadowRay)
        }
        return lightColor
    } else {
        if(currentRayInfluence > minInfluence) {
            let tempPath = [p, firstHit_objNr_wallVector[0]]
            drawBasicPath(ctx, tempPath, rayColors.shadowRayColor, 2, showShadowRay)
        }
        let refractiveness = objMaterials[firstHit_objNr_wallVector[1]][1]
        if(refractiveness > 0) {
            // shadow ray blocked by transparent obj
            let colorInc = castShadowRay(ctx, addVector(firstHit_objNr_wallVector[0], vec), lightPos, currentRayInfluence, lightColor)
            let objColorPart = divVectorNumber(objColors[firstHit_objNr_wallVector[1]], 255)
            let re = multVectorNumber(addVector(multVectorNumber(colorInc, refractiveness), multVectorNumber(multVectorsEach(objColorPart, colorInc), 1-refractiveness)), refractiveness)
            return re
        } else {
            // shadow ray blocked by non transparent obj
            if(currentRayInfluence > minInfluence) {
                let tempPath = [firstHit_objNr_wallVector[0], lightPos]
                drawBasicPath(ctx, tempPath, rayColors.shadowRayBlockedColor, 2, showShadowRay)
            }
            return [0,0,0]
        }
    }
}

function phongLighting3d(objNr, firstHit, normalInp, vecHitToCamInp, faces) {
    let normal = normalize3d(normalInp)
    let vecHitToCam = normalize3d(vecHitToCamInp)
    let n = objFactors[objNr][2]
    let diffuse = [0, 0, 0]
    let specular = [0, 0, 0]

    // no shadow ray on canvas border -> pretend ray leaves screen
    if (objNr != 0) {
        let incomingLights = colorFromLights3D(firstHit, faces)
        let c = 0
        incomingLights.forEach(light => {
            if(vecLength(light) > 0) {
                let vecHitToLight = multVectorNumber(normalize3d(subVector(firstHit, lights[c])), -1)
                let vecReflectedLightRay = normalize3d(subVector(multVectorNumber(normal, (2 * dotProduct(normal, vecHitToLight))), vecHitToLight))

                for (let i = 0; i < 3; i += 1) {
                    let intensity = light[i] / 255
                    diffuse[i] += objFactors[objNr][0] * intensity * Math.max(0, dotProduct(normal, vecHitToLight))
                    // the dot product is the cos of the angle ^^
                    specular[i] += objFactors[objNr][1] * intensity * Math.pow(Math.max(0, dotProduct(vecReflectedLightRay, vecHitToCam)), n)
                }
            }
            c += 1
        })
    }
    if(objFactors[objNr][4]) {
        specular = multVectorsEach(specular, divVectorNumber(objColors[objNr], 255))
    }

    if (showEnergyPreservationTerm) {
        for (let spec = 0; spec < specular.length; spec+=1) {
            specular[spec] *= (n + 2) / (2 * Math.PI)
        }
    }

    // reduce light from 0-255 to 0-1 so the mult wont overkill and make everything white
    let ambient = multVectorsEach(objColors[objNr], multVectorNumber(divVectorNumber(ambientLight, 255), objFactors[objNr][3]))
    return addVector(addVector(multVectorsEach(diffuse, objColors[objNr]), multVectorNumber(specular, 255)), ambient)
}

function colorFromLights3D(firstHit, faces) {
    let incomingLights = []
    let lc = 0
    lights.forEach(l => {
        let lightColor = lightColors[lc]
        // cast a ray to each light source and just see if its blocked
        let direction = subVector(l, firstHit)
        let hitObjs = getAllIntersectionObjects3D(firstHit, direction, faces, vecLength(direction))
        hitObjs.forEach(obj => {
            let refractiveness = objMaterials[obj][1]
            let objColorPart = divVectorNumber(objColors[obj], 255)
            lightColor = multVectorNumber(addVector(multVectorNumber(lightColor, refractiveness), multVectorNumber(multVectorsEach(objColorPart, lightColor), 1-refractiveness)), refractiveness)
        })
        incomingLights.push(lightColor)
        lc += 1
    })
    return incomingLights
}
