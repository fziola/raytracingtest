function hitTestCircle(circles, radius, mPos) {
    let c = 0
    let re = -1
    circles.forEach(circle => {
        // test if mPos is in circle radius
        if (vecLength(subVector(mPos, [circle[0], circle[1]])) <= radius) {
            re = c
        }
        c += 1
    })
    return re
}

function boundingBox(obj) {
    // returns the (upright rectangular) bounding box of any object
    var highest = -1 * $(window).height() - $(window).width()
    var rightmost = -1 * $(window).height() - $(window).width()
    var lowest = $(window).height()
    var leftmost = $(window).width()
    
    obj.forEach(point => {
        x = point[0]
        y = point[1]
        
        if(x < leftmost) {
            leftmost = x
        }
        if(x > rightmost) {
            rightmost = x
        }
        if(y < lowest) {
            lowest = y
        }
        if(y > highest) {
            highest = y
        }

    })
    return [highest,rightmost,lowest,leftmost]
}

function pointInBoundingBox(point, bb) {
    // bb -> highest, rightmost, lowest, leftmost
    return (bb[3] < point[0] && point[0] < bb[1] && bb[2] < point[1] && point[1] < bb[0])
}

function pointInPolygon(point, corners) {
    // https://stackoverflow.com/questions/217578/how-can-i-determine-whether-a-2d-point-is-within-a-polygon
    // consider using https://de.serlo.org/mathe/30258/winkel-zwischen-vektoren-berechnen-1-2 instead (its shorter but equivalent)
    let angle = 0
    for(let i = 0; i < corners.length; i++) {
        let a_s = corners[i]
        let b_s = corners[(i + 1)%corners.length]

        // calculate distance of vector
        let A = vecLength(subVector(a_s, b_s))
        let B = vecLength(subVector(point, a_s))
        let C = vecLength(subVector(point, b_s))

        // calculate direction of vector
        ta_x = a_s[0] - point[0]
        ta_y = a_s[1] - point[1]
        tb_x = b_s[0] - point[0]
        tb_y = b_s[1] - point[1]

        cross = tb_y * ta_x - tb_x * ta_y
        clockwise = cross < 0

        let currentAngle = Math.acos((B * B + C * C - A * A) / (2.0 * B * C))

        // calculate sum of angles
        if(clockwise) {
            angle += currentAngle
        } else {
            angle -= currentAngle
        }
    }

    // 360 deg -> 6,28319
    return Math.abs(angle) > 6.283
}

function hitTest(objects, mousePos) {
    // tests if a click has hit an objects BB
    var c = 1
    var re = -1
    // slice cuz objecs[0] is the boundary and should NOT be moved
    objects.slice(1).forEach(obj => {
        let angle = -1 * objRotations[c]
        let bb = boundingBox(rotateObject2d_around_origin(obj, angle))
        if (pointInBoundingBox(rotatepoint2d(mousePos, angle), bb)) {
            re = c
        }
        c += 1
    })
    return re
}

function findCenter(obj) {
    let avg = [0,0]
    let c = 0
    obj.forEach(point => {
        avg[0] += point[0]
        avg[1] += point[1]
        c += 1
    })
    avg[0] /= c
    avg[1] /= c
    return avg
}

function setCenter(obj, oldCenter) {
    let newCenter = findCenter(obj)
    let centerDiff = subVector(oldCenter, newCenter)
    for(let i = 0; i < obj.length; i++) {
        obj[i] = addVector(obj[i], centerDiff)
    }
    return obj
}

function rotatepoint2d(point, angle) {
    // angle in radians pls :)
    x = point[0] * Math.cos(angle) - point[1] * Math.sin(angle)
    y = point[0] * Math.sin(angle) + point[1] * Math.cos(angle)
    return[x,y]
}

function rotateObject2d_around_origin(obj, angle) {
    // radians
    tempObj = []
    obj.forEach(point => {
        rp = rotatepoint2d(point, angle)
        tempObj.push(rp)
    })
    return tempObj
}

function rotateObject2d(obj, angle) {
    // angle is radians
    center = findCenter(obj)
    tempObj = []
    obj.forEach(point => {
        rp = rotatepoint2d([point[0]-center[0], point[1]-center[1]], angle)
        tempObj.push([rp[0]+center[0], rp[1]+center[1]])
    })
    return tempObj
}

function normalize2d(vec) {
    let vec2 = [0,0]
    let len = vecLength(vec)
    vec2[0] = vec[0]/len
    vec2[1] = vec[1]/len
    return vec2
}

function vector2dDotProduct(v1, v2) {
  return v1[0] * v2[0] + v1[1] * v2[1]
}

function angle2d(vecA, vecB) {
    // returns radian value between 0 and 2PI
    // PI = 180 degrees
    r = Math.atan2(vecB[1], vecB[0]) - Math.atan2(vecA[1], vecA[0])
    if(r <= 0) {
        r += 2 * Math.PI
    }
    return r
}

function getScreenLineCorners(camPos, camDir, distToScreen) {
    // returns left and right point of the line representing the screen in 2d-view

    let tmi = multVectorNumber(camDir, distToScreen)
    let left = addVector(camPos, rotatepoint2d(tmi, -0.8))
    let right = addVector(camPos, rotatepoint2d(tmi, 0.8))

    return [left,right]
}

function interpolate2d(pointA, pointB, x) {
    // 0 <= x <= 1
    let tempPoint = [0.0,0.0]

    tempPoint[0] = interpolateValue(pointA[0], pointB[0], x)
    tempPoint[1] = interpolateValue(pointA[1], pointB[1], x)

    return tempPoint
}

function getScreen_Ray_Start(left_right, numberOfRays, ray) {
    // returns points on the screen-line (get left_right from getScreenLineCorners)
    // these will be the starting points for ray casting
    // continue the Ray in direction [rayStart-camPos]


    // where on the line should the point be (0 to 1)
    relPos = (ray+1) / (numberOfRays+1)
    return interpolate2d(left_right[0], left_right[1], relPos)
}

function getScreenPart(left_right, numberOfParts, part) {
    // seperates the screen into "1d pixels"
    relPosA = (part) / (numberOfParts)
    relPosB = (part+1) / (numberOfParts)
    return [interpolate2d(left_right[0], left_right[1], relPosA), interpolate2d(left_right[0], left_right[1], relPosB)]
}

function vecToPointObject2d(vec) {
    return {x: vec[0], y: vec[1]}
}
