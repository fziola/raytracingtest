function degreesToRadians(deg) {
    return deg * Math.PI / 180
}

function radiansToDegrees(rad) {
    return ( rad * 180 ) / Math.PI
}

function addVector(a, b) {
    return a.map((e,i) => e + b[i])
}

function subVector(a, b) {
    return a.map((e,i) => e - b[i])
}

function addVectorNumber(vec, number) {
    return vec.map(e => e + number)
}

function subVectorNumber(vec, number) {
    return vec.map(e => e - number)
}

function multVectorNumber(vec, num) {
    return vec.map(x => x * num)
}

function divVectorNumber(vec, num) {
    return vec.map(x => x / num)
}

function multVectorsEach(a, b) {
    return a.map((e,i) => e * b[i])
}

function divVectorsEach(a, b) {
    return a.map((e,i) => e / b[i])
}

function dotProduct(vecA, vecB) {
    let sum = 0
    for (let i = 0; i < vecA.length; i+=1) {
        sum += vecA[i] * vecB[i]
    }
    return sum
}

function invertArray(vec, from=1) {
    return vec.map(x => from - x)
}

function vecLength(vec) {
    var sum = 0
    for(let i = 0; i < vec.length; i+=1) {
        sum += Math.pow(vec[i], 2)
    }
    return Math.sqrt(sum)
}

function normalize(vec) {
    let vecNew = getDeepCopy(vec)
    let len = vecLength(vec)
    for(let i = 0; i < vec.length; i++) {
        vecNew[i] = vec[i]/len
    }
    return vecNew
}

function arraysEqualNumber(a, b, epsilon=0, normalized=false) {
    a = Array.isArray(a) ? a : []
    b = Array.isArray(b) ? b : []
    if(normalized) {
        a = normalize(a)
        b = normalize(b)
    }
    return a.length === b.length && a.every((el, ix) => Math.abs(el - b[ix]) < epsilon)
}

function arraysEqual(a, b) {
    a = Array.isArray(a) ? a : []
    b = Array.isArray(b) ? b : []
    return a.length === b.length && a.every((el, ix) => el === b[ix])
}

function interpolateValue(a, b, x) {
    x = Math.max(Math.min(1, x), 0)
    return a + x * (b - a)
}
