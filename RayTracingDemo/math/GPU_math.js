function addVectorNumber_GPU_3d(vec, number) {
  // GPU.js does not accept arrow function :(
  return [vec[0]+number, vec[1]+number, vec[2]+number]
}

function addVector_GPU_3d(vecA, vecB) {
  // GPU.js does not accept arrow function :(
  return [vecA[0]+vecB[0], vecA[1]+vecB[1], vecA[2]+vecB[2]]
}