function normalize3d(vec) {
    let vec3 = [0,0,0]
    let len = vecLength(vec)
    vec3[0] = vec[0]/len
    vec3[1] = vec[1]/len
    vec3[2] = vec[2]/len
    return vec3
}

function crossProduct3d(vecA, vecB) {
    return [
        vecA[1] * vecB[2] - vecA[2] * vecB[1],
        vecA[2] * vecB[0] - vecA[0] * vecB[2],
        vecA[0] * vecB[1] - vecA[1] * vecB[0]
    ]
}

function getPlaneNormal(plane) {
    let vecA = subVector(plane[0], plane[1])
    let vecB = subVector(plane[2], plane[1])
    return normalize3d(crossProduct3d(vecA, vecB))
}

function getPointRelativeToPlane(point, face) {
    // they are just named xyz cuz creativity
    // it has nothing to do with the 3 dimensions [0], [1] and [2] (well its the new ones, not the old)
    let vecX = normalize3d(subVector(face[1], face[0]))
    let vecY = normalize3d(subVector(face[2], face[0]))
    let vecZ = crossProduct3d(vecX, vecY)

    // transform the point onto the plane
    pointB = subVector(point, face[0])
    pointA = [0,0,0]

    pointA[0] = pointB[0] * vecX[0] + pointB[1] * vecX[1] + pointB[2] * vecX[2]
    pointA[1] = pointB[0] * vecY[0] + pointB[1] * vecY[1] + pointB[2] * vecY[2]
    pointA[2] = pointB[0] * vecZ[0] + pointB[1] * vecZ[1] + pointB[2] * vecZ[2]

    return pointA
}

function pointOnFace(point, face) {
    point2d = getPointRelativeToPlane(point, face)
    polygon = []
    for(facePoint of face) {
        polygon.push(getPointRelativeToPlane(facePoint, face).splice(0,2))
    }

    if(Math.abs(point2d[2]) > epsilon) {
        // console.log('out of plane', point2d[2])
        // since this mostly called by lineIntersection, it should always be on the plane
        // this can still fail, mostly due to js errors
        return false  // point2d is out of the polygons plane.
    }

    return pointInPolygon(point2d.splice(0,2), polygon)
}

function reflect3d(vec, normal) {
    // R = 2*(V dot N)*N - V
    vec = normalize3d(multVectorNumber(vec, -1))
    normal = normalize3d(normal)
    return subVector(multVectorNumber(normal, dotProduct(vec, normal)*2), vec)
}

function refract3d(vec, normal, objNrFrom, objNrTo) {
    // https://stackoverflow.com/questions/29758545/how-to-find-refraction-vector-from-incoming-vector-and-surface-normal
    // https://www.scratchapixel.com/lessons/3d-basic-rendering/introduction-to-shading/reflection-refraction-fresnel
    vec = normalize3d(vec)
    normal = normalize3d(normal)

    const n = objMaterials[objNrFrom][2] / objMaterials[objNrTo][2]

    // Theta0 is the angle of the incoming ray to the normal

    // cos(Theta0) = n*vec
    let cos = dotProduct(normal, vec)
    if (cos < 0) {
        cos = -cos
    } else {
        normal = multVectorNumber(normal, -1)
    }

    // sin^2(x) + cos^2(x) = 1
    const sin_squared = 1.0 - cos * cos

    // sin(T1)/sin(T0) = n0/n1
    // sin(T1) = n * sin(T0)
    if(sin_squared * n * n > 1.0) return false // TIR

    // n * (vec + normal * cos(Theta0))
    let a = multVectorNumber(addVector(multVectorNumber(normal, cos), vec), n)
    // normal * sqrt(1 - n^2 sin^2(Theta(0)))
    let b = multVectorNumber(normal, Math.sqrt( 1 - n * n * sin_squared ))
    // n * (vec + normal * cos(Theta0)) - normal * sqrt(1 - n^2 sin^2(Theta(0)))
    refrVec =  subVector(a, b)

    return refrVec
}

function angle3d(vecA, vecB) {
    // returns radian value between 0 and 2PI
    // PI = 180 degrees
    const div = vecLength(vecA) * vecLength(vecB)
    const dotP = dotProduct(vecA, vecB)
    const angle = dotP / div
    
    return angle
}

function rotate3D_x_axis(vec, angle) {
    // rotates around only x axis
    // angle in radians pls :)
    x = vec[0]
    y = Math.cos(angle) * vec[1] - Math.sin(angle) * vec[2]
    z = Math.sin(angle) * vec[1] + Math.cos(angle) * vec[2]
    return[x,y,z]
}

function rotate3D_y_axis(vec, angle) {
    // rotates around only x axis
    // angle in radians pls :)
    x = Math.cos(angle) * vec[0] + Math.sin(angle) * vec[2]
    y = 
    z = -1 * Math.sin(angle) * vec[0] + Math.cos(angle) * vec[2]
    return[x,y,z]
}

function rotate3D_z_axis(vec, angle) {
    // rotates around only x axis
    // angle in radians pls :)
    const x = Math.cos(angle) * vec[0] - Math.sin(angle) * vec[1]
    const y = Math.sin(angle) * vec[0] + Math.cos(angle) * vec[1]
    const z = vec[2]
    return[x,y,z]
}