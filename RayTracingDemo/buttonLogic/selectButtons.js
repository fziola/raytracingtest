function closeThisPopup(id) {
  document.getElementById(id).style.display = 'none'
}

function collapsible(rowId, collapseButtonId) {
  let collapseButton = document.getElementById(collapseButtonId)
  let content = document.getElementById(rowId)

  if (content.style.display === "block") {
    content.style.display = "none"
    collapseButton.style.backgroundColor =  "#eee"
  } else {
    content.style.display = "block"
    collapseButton.style.backgroundColor = "lightgreen"
  }
}

function toggleShowRay_input() {
  toggleShowRay()
  storeChange('showRay', !showRay, showRay)
}

function toggleShowRay() {
  showRay = !showRay
  document.getElementById("showRayBtn").innerHTML = " show rays: " + showRay
}

function toggleShowShadowRay_input() {
  toggleShowShadowRay()
  storeChange('showShadowRay', !showShadowRay, showShadowRay)
}

function toggleShowShadowRay() {
  showShadowRay = !showShadowRay
  document.getElementById("showShadowRayBtn").innerHTML = " show shadow rays: " + showShadowRay
}

function toggleShowFormulas_input() {
  toggleShowFormulas()
  storeChange('displayExplainBoxes', !displayExplainBoxes, displayExplainBoxes)
}

function toggleShowFormulas() {
  displayExplainBoxes = !displayExplainBoxes
  document.getElementById("showFormulasBtn").innerHTML = "  show formulas:  " + displayExplainBoxes
  document.getElementById("miniPopup").style.display = "none"
}

function toggleShowNormals_input() {
  toggleShowNormals()
  storeChange('showNormals', !showNormals, showNormals)
}

function toggleShowNormals() {
  showNormals = !showNormals
  document.getElementById("showNormalsBtn").innerHTML = " show object normals: " + showNormals
}

function toggleShowObjHits_input() {
  toggleShowObjHits()
  storeChange('showObjHits', !showObjHits, showObjHits)
}

function toggleShowObjHits() {
  showObjHits = !showObjHits
  document.getElementById("showObjHitsBtn").innerHTML = " show object hit color: " + showObjHits
}

function toggleShowPOI_input() {
  if (showPOI === showObjHits) {
    toggleShowObjHits_input()
  }
  toggleShowPOI()
  storeChange('showPOI', !showPOI, showPOI)
}

function toggleShowPOI() {
  showPOI = !showPOI
  const POIpopupID = 'POIexplanationPopUp'
  if(!showPOI) {
    hideMiniPopup(POIpopupID)
  } else if (chosenPOI != -1) {
    showMiniPopup(POIpopupID)
  }
  document.getElementById("showPOI").innerHTML = " show points of interest: " + showPOI
}

function button2d(selfId) {
    let but = document.getElementById(selfId)

      switch (selfId) {
        case 'editObjButPopup':
        case 'editObjBut':
          if(selectionModeActive) {
            deselectEdit(but)
          } else {
            selectEdit(but)
          }
          break
        case 'rectButPopup':
        case 'rectBut':
          if(addRectActive) {
            deselectAddRect(but)
          } else {
            selectAddRect(but)
          }
          break
        case 'circleBut':
          if(addCircleActive) {
            deselectAddCircle(but)
          } else {
            selectAddCircle(but)
          }
          break
        case 'drawBut':
          if(drawActive) {
            deselectDraw(but)
          } else {
            selectDraw(but)
          }
          break
        case 'addLightButPopup':
        case 'addLightBut':
          if(addLightActive) {
              deselectAddLight(but)
          } else {
              selectAddLight(but)
          }
          break
        case 'editCamButPopup':
        case 'editCamBut':
          if(editCamActive) {
              deselectEditCam(but)
          } else {
              selectEditCam(but)
          }
      break
        default:
          console.log("what button is",selfId)
          break
      }
  }

function setBut(but) {
  // deselect all others
  var x = document.getElementsByClassName("btnAct")
  for (let i = 0; i < x.length; i++) {
    resetBut(x[i])
  }
  var xPopup = document.getElementsByClassName("popupBtn")
  for (let i = 0; i < xPopup.length; i++) {
    resetBut(xPopup[i])
  }

  selectionModeActive = false
  addRectActive = false
  addCircleActive = false
  drawActive = false
  addLightActive = false
  editCamActive = false

  editActive = true

  // there some buttons that do the same thing in different menus, to set all of those when
  // something gets activated they ave the same name
  const sameFunctionButtons = document.getElementsByName(but.getAttribute("name"))

  for (sfBut of sameFunctionButtons) {
    if(sfBut.className === 'popupBtn') {
      // buttons in the right-click popup look different
      sfBut.style.backgroundColor = 'green'
    } else {
      sfBut.className = "btnAct"
    }
  }
}

function selectEdit(but) {
  setBut(but)
  selectionModeActive = true
  instructionText.innerHTML = editText
}

function selectAddRect(but) {
  setBut(but)
  addRectActive = true
  instructionText.innerHTML = addRectText
}

function selectAddCircle(but) {
  setBut(but)
  addCircleActive = true
  instructionText.innerHTML = addCircleText
}

function selectDraw(but) {
  setBut(but)
  drawActive = true
  instructionText.innerHTML = drawText
}

function selectAddLight(but) {
  setBut(but)
  addLightActive = true
  instructionText.innerHTML = "add Text for addLight"
}

function selectEditCam(but) {
  setBut(but)
  editCamActive = true
  instructionText.innerHTML = editCamText
}