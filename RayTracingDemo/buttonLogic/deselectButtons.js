function resetBut(but) {
    editActive = false

    // there some buttons that do the same thing in different menus, to set all of those when
    // something gets activated they ave the same name
    const sameFunctionButtons = document.getElementsByName(but.getAttribute("name"))

    for (sfBut of sameFunctionButtons) {
        if(sfBut.className === 'popupBtn') {
        // buttons in the right-click popup look different
            sfBut.style.backgroundColor = 'white'
        } else {
            sfBut.className = "btn"
        }
    }
    instructionText.innerHTML = standardText
    drawArray = []
    unsetHighlightObj()
    unsetHighlightLight()
    highlightedCam = false
    let tc = document.getElementById("editCameraPopup")
    tc.style.display = "none"
}

function deselectEdit(but) {
    selectionModeActive = false
    resetBut(but)
}

function deselectAddRect(but) {
    addRectActive = false
    resetBut(but)
}

function deselectAddCircle(but) {
    addCircleActive = false
    resetBut(but)
}

function deselectDraw(but) {
    drawActive = false
    resetBut(but)
}

function deselectAddLight(but) {
    addLightActive = false
    resetBut(but)
}

function deselectEditCam(but) {
    editCamActive = false
    resetBut(but)
}
