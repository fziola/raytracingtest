function onhover(selfId) {
  if (showBtnHoverTexts) {
    let but = document.getElementById(selfId)
    let hoverBox = document.getElementById("buttonHover")
    let hoverText = document.getElementById("buttonHoverText")
    let x = but.offsetLeft
    let y = but.offsetTop

    hoverBox.style.display = "block"
    hoverBox.style.left = `${x}px`
    hoverBox.style.top = `${y-75}px`

    switch (selfId) {
      // buttons below
      case 'editObjBut':
        hoverText.innerHTML = editObjBtnHoverText
        break
      case 'rectBut':
        hoverText.innerHTML = rectBtnHoverText
        break
      case 'circleBut':
        hoverText.innerHTML = circleBtnHoverText
        break
      case 'drawBut':
        hoverText.innerHTML = drawBtnHoverText
        break
      case 'addLightBut':
        hoverText.innerHTML = addLightBtnHoverText
        break
      case 'editCamBut':
        hoverText.innerHTML = editCamBtnHoverText
        break
      default:
        console.log("what button is", selfId)
        break
    } 
  }
}

function onleave() {
  let hoverBox = document.getElementById("buttonHover")
  hoverBox.style.display = "none"
}

function sliderHover(event, endStr='', startStr='') {
  let t = event.target
  displayMiniPopup('sliderHover', getOffset(t, -50, 30), startStr + t.value + endStr)
}

function sliderHoverLeave() {
  hideMiniPopup('sliderHover')
}

function updateHover(id, newStr) {
  document.getElementById(id).innerHTML = newStr
}

function getOffset(el, moveY=0, moveX=0) {
  const rect = el.getBoundingClientRect()
  return [
    rect.left + window.scrollX + moveX,
    rect.top + window.scrollY + moveY
  ]
}
