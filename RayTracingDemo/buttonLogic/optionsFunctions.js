function changeCanvasBackground(event) {
    let oldVal = window.getComputedStyle(canvas2d).backgroundColor
    var root = document.querySelector(':root')
    root.style.setProperty('--canvas2dBackround', event.target.value)
    objColors[0] = rgbStringToArray(window.getComputedStyle(canvas2d).backgroundColor)
    saveCanvasBackgroundToLocalStorage(window.getComputedStyle(canvas2d).backgroundColor)
    storeChange('background2d', oldVal, window.getComputedStyle(canvas2d).backgroundColor)
}

function setCanvasBackgroundColor(hexText) {
    // give like ffffff
    let oldVal = window.getComputedStyle(canvas2d).backgroundColor
    var root = document.querySelector(':root')
    root.style.setProperty('--canvas2dBackround', " #"+hexText)
    objColors[0] = rgbStringToArray(window.getComputedStyle(canvas2d).backgroundColor)
    saveCanvasBackgroundToLocalStorage(window.getComputedStyle(canvas2d).backgroundColor)
    storeChange('background2d', oldVal, window.getComputedStyle(canvas2d).backgroundColor)
}

function setBackgroundRefrIndex(event) {
    let oldValue = getDeepCopy(objMaterials)
    let field = event.target
    field.style.color = "black"
    field.style.backgroundColor = "white"
    if(event.key === 'Enter') {
        keyInputLocked = false
        let inpNr = parseFloat(field.value)
        if ((isFloat(inpNr) || isInt(inpNr)) && inpNr > 0) {
            field.style.backgroundColor = "green"
            objMaterials[0][2] = inpNr
        } else {
            field.style.backgroundColor = "red"
            console.log("not a valid float or invalid higlight")
        }
    }
    storeChange('objMaterials', oldValue, objMaterials)
}

function changeLampStyle(id) {
    let butT = document.getElementById(id)
    lampStyle = (lampStyle+1) % numberOfLampStyles
    butT.innerHTML = "Style: " + lampStyleNames[lampStyle]
}

function toggleShowBtnHover(id) {
    showBtnHoverTexts = !showBtnHoverTexts
    document.getElementById(id).innerHTML = 'Show hover-texts: ' + showBtnHoverTexts
}

function toggleShowObjHitOnCanvas(id) {
    showObjHitOnCanvas = !showObjHitOnCanvas
    document.getElementById(id).innerHTML = 'Show obj. hit: ' + showObjHitOnCanvas
}

function controlRayAlpha(id) {
    rayAlphaOption = (rayAlphaOption+1) % maxRayAlphaOption
    let text = 'always 1'
    switch(rayAlphaOption) {
        case 1:
            text = 'lower with recursion'
            break
        case 2:
            text = 'according to influence'
            break
    }
    document.getElementById(id).innerHTML = 'Currently: ' + text
}

function controlRayColor(id) {
    rayColorOption = (rayColorOption+1) % maxRayColorOption
    let text = 'default'
    switch(rayColorOption) {
        case 1:
            text = 'color of light'
            break
    }
    document.getElementById(id).innerHTML = 'Currently: ' + text
}

function setMaxRec(event) {
    let el = event.target
    el.style.color = "black"
    el.style.backgroundColor = "white"
    if(event.key === 'Enter') {
        keyInputLocked = false
        let inpNr = parseFloat(el.value)
        if (isInt(inpNr) && inpNr > 0) {
            el.style.backgroundColor = "green"
            maxRec = inpNr
        } else {
            el.style.backgroundColor = "red"
            console.log("not a valid float " + inpNr + " or negative float")
        }
    }
}

function setExportNameKey(event, id) {
    if(event.key === 'Enter') {
        setExportName(id)
    }
}

function setExportName(id) {
    let field = document.getElementById(id)
    exportName = field.value
}

function exportPng() {
    let canv2dImg = canvas2d.toDataURL("image/png")
    let link2d = document.createElement('a')
    link2d.download = exportName + '_2d.png'
    link2d.href = canv2dImg
    link2d.click()
    let canv3dImg = canvas3d.toDataURL("image/png")
    let link3d = document.createElement('a')
    link3d.download = exportName + '_3d.png'
    link3d.href = canv3dImg
    link3d.click()
}

function exportSvg() {
    capture_SVG_start = true
}