function lockKeyboard() {
    keyInputLocked = true
}

function unlockKeyboard() {
    keyInputLocked = false
}

function blurAll(id) {
    // used to deselect the input
    var tmp = document.createElement("input")
    let tObj = document.getElementById(id).parentElement
    tObj.appendChild(tmp)
    tmp.focus()
    tObj.removeChild(tmp)
    keyInputLocked = false
}

function hexToRgbArray(hex) {
    var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex)
    return [
      parseInt(result[1], 16),
      parseInt(result[2], 16),
      parseInt(result[3], 16)
    ]
}

function rgbStringToHex(rgbString) {
    // https://stackoverflow.com/questions/13070054/convert-rgb-strings-to-hex-in-javascript
    var a = rgbString.split("(")[1].split(")")[0]
    a = a.split(",")
    var b = a.map(function(x) {
        x = parseInt(x).toString(16)
        return (x.length==1) ? "0"+x : x
    })
    b = "#"+b.join("")
    return b
}

function componentToHex(c) {
    var hex = c.toString(16)
    return hex.length == 1 ? "0" + hex : hex
}

function rgbArrayToHex(colorArray) {
    return "#" + componentToHex(Math.round(colorArray[0])) + componentToHex(Math.round(colorArray[1])) + componentToHex(Math.round(colorArray[2]))
}

function setHighlightObjectEvent(event, id) {
    let field = document.getElementById(id)
    field.style.color = "black"
    field.style.backgroundColor = "white"

    if(event.key === 'Enter') {
        blurAll(id)
        let nr = parseInt(field.value)
        if(nr > 0 && nr < objColors.length) {
            setHighlightObject(nr)
            field.style.backgroundColor = "green"
        } else {
            unsetHighlightObj()
            field.style.backgroundColor = "red"
        }
    }
}

function setObjColor(event) {
    let oldValue = getDeepCopy(objColors)
    setObjColor_helper(event)
    storeChange('objColors', oldValue, objColors)
}

function setObjColor_helper(event) {
    objColors[highlighted] = hexToRgbArray(event.target.value)
}

function refl_refr(event, refl_refr) {
    // refl_refr = 0 or 1
    let oldValue = getDeepCopy(objMaterials)
    let val = parseInt(event.target.value) / 100
    objMaterials[highlighted][refl_refr] = val
    if (objMaterials[highlighted][0] + objMaterials[highlighted][1] > 1) {
        objMaterials[highlighted][1-refl_refr] = 1 - objMaterials[highlighted][refl_refr]
        document.getElementById('inputObj_refl').value = objMaterials[highlighted][0] * 100
        document.getElementById('inputObj_refr').value = objMaterials[highlighted][1] * 100
    }
    updateHover('sliderHover', Math.round(val*100)+'%')
    storeChange('objMaterials', oldValue, objMaterials)
}

function setRefrIndex(event) {
    let oldValue = getDeepCopy(objMaterials)
    let field = event.target
    field.style.color = "black"
    field.style.backgroundColor = "white"
    if(event.key === 'Enter') {
        blurAll(event.target.id)
        let inpNr = parseFloat(field.value)
        if ((isFloat(inpNr) || isInt(inpNr)) && highlighted > 0 && highlighted < objColors.length && inpNr > 0) {
            field.style.backgroundColor = "green"
            objMaterials[highlighted][2] = inpNr
        } else {
            field.style.backgroundColor = "red"
            console.log("not a valid float or invalid higlight")
        }
    }
    storeChange('objMaterials', oldValue, objMaterials)
}

function setHighlightLightsourceEvent(event, id) {
    let field = document.getElementById(id)
    field.style.color = "black"
    field.style.backgroundColor = "white"

    if(event.key === 'Enter') {
        blurAll(id)
        let nr = parseInt(field.value)
        if(nr >= 0 && nr < lightColors.length) {
            setHighlightLight(nr)
            field.style.backgroundColor = "green"
        } else {
            unsetHighlightLight()
            field.style.backgroundColor = "red"
        }
    }
}

function setLightColor(event) {
    let oldValue = getDeepCopy(lightColors)
    setLightColor_helper(event)
    storeChange('lightColors', oldValue, lightColors)
}

function setLightColor_helper(event) {
    lightColors[highlightedLight] = hexToRgbArray(event.target.value)
}

function changeLightPos(light, diff) {
    // called in input listener
    let oldValue = getDeepCopy(lights)
    light[0] += diff[0]
    light[1] += diff[1]
    storeChange('lights', oldValue, lights)
}

function setLightPos(event, id, direction) {
    if(event.key === 'Enter') {
        let oldValue = getDeepCopy(lights)
        setLightPos_helper(event, id, direction)
        storeChange('lights', oldValue, lights)
    }
}

function setLightPos_helper(event, id, direction) {
    // direction 0 is x / 1 is y
    let field = document.getElementById(id)
    field.style.color = "black"
    field.style.backgroundColor = "white"
    if(event.key === 'Enter') {
        blurAll(id)
        let inpNr = parseFloat(field.value)
        if ((isFloat(inpNr) || isInt(inpNr)) && inpNr > 0 && highlightedLight >= 0 && highlightedLight < objColors.length) {
            field.style.backgroundColor = "green"
            lights[highlightedLight][direction] = inpNr
        } else {
            field.style.backgroundColor = "red"
            console.log("not a valid float " + inpNr + " or negative float or invalid higlightLight " + highlightedLight)
        }
    }
}

function setNumberOfRays(event) {
    let oldVal = numberOfRays
    let id = event.target.id
    let field = document.getElementById(id)
    field.style.color = "black"
    field.style.backgroundColor = "white"
    if(event.key === 'Enter') {
        blurAll(id)
        let inpNr = parseInt(field.value)
        if (isInt(inpNr) && inpNr > 0 && inpNr < maxRays2d) {
            field.style.backgroundColor = "green"
            numberOfRays = inpNr
            storeChange('numberOfRays', oldVal, numberOfRays)
        } else {
            field.style.backgroundColor = "red"
            console.log("not a valid int " + inpNr + " or negative or > " + maxRays2d)
        }
    }
}

function setCameraAngle(event) {
    let oldVal = getDeepCopy(cameraDir)
    let id = event.target.id
    let field = document.getElementById(id)
    field.style.color = "black"
    field.style.backgroundColor = "white"
    if(event.key === 'Enter') {
        blurAll(id)
        let inpNr = parseFloat(field.value)
        if ((isFloat(inpNr) || isInt(inpNr))) {
            field.style.backgroundColor = "green"
            const currentAngle = angle2d([0,-1],cameraDir.slice(0, -1))
            let newAngle = degreesToRadians(inpNr) - currentAngle
            let newDir = rotatepoint2d(cameraDir.slice(0, -1), newAngle)
            cameraDir[0] = newDir[0]
            cameraDir[1] = newDir[1]

            storeChange('cameraDir', oldVal, cameraDir)
        } else {
            field.style.backgroundColor = "red"
            console.log("not a valid angle " + inpNr)
        }
    }
}

function setCameraPos(event, dimension) {
    let oldVal = getDeepCopy(cameraPos)
    let id = event.target.id
    let field = document.getElementById(id)
    field.style.color = "black"
    field.style.backgroundColor = "white"
    if(event.key === 'Enter') {
        blurAll(id)
        let inpNr = parseFloat(field.value)
        if ((isFloat(inpNr) || isInt(inpNr))) {
            field.style.backgroundColor = "green"
            cameraPos[dimension] = inpNr

            storeChange('cameraPos', oldVal, cameraPos)
        } else {
            field.style.backgroundColor = "red"
            console.log("not a valid angle " + inpNr)
        }
    }
}

function changeCamPos(xDiff, yDiff) {
    let oldVal = getDeepCopy(cameraPos)

    cameraPos[0] += xDiff
    cameraPos[1] += yDiff

    storeChange('cameraPos', oldVal, cameraPos)
}

function rotateCam(angle) {
    let oldVal = getDeepCopy(cameraDir)

    let temp = rotatepoint2d([cameraDir[0], cameraDir[1]], angle)
    cameraDir[0] = temp[0]
    cameraDir[1] = temp[1]

    storeChange('cameraDir', oldVal, cameraDir)
}

function setObjFactor(event, id) {
    if(event.key === 'Enter') {
        let oldValue = getDeepCopy(objFactors)
        setObjFactor_helper(event, id)
        storeChange('objFactors', oldValue, objFactors)
    }
}

function setObjFactor_helper(event, id) {
    let field = document.getElementById(id)
    field.style.color = "black"
    field.style.backgroundColor = "white"
    if(event.key === 'Enter') {
        blurAll(id)
        let inpNr = parseFloat(field.value)
        if ((isFloat(inpNr) || isInt(inpNr)) && highlighted > 0 && highlighted < objColors.length) {
            field.style.backgroundColor = "green"
            switch (id) {
                case "inputObj_kd":
                    objFactors[highlighted][0] = inpNr
                    break
                case "inputObj_ks":
                    objFactors[highlighted][1] = inpNr
                    break
                case "inputObj_n":
                    objFactors[highlighted][2] = inpNr
                    break
                case "inputObj_ka":
                    objFactors[highlighted][3] = inpNr
                    break
                default:
                    console.log("invalid id in setObjFactor()")
            }
        } else {
            field.style.backgroundColor = "red"
            console.log("not a valid float or invalid higlight")
        }
    }
}

function setSize(event, id, direction) {
    if(event.key === 'Enter') {
        let oldValue = getDeepCopy(objects)
        setSize_helper(event, id, direction)
        if(isOverlap_objNr_all(highlighted)) {
            copyToConstArray(objects, oldValue)
            document.getElementById(id).style.backgroundColor = "red"
        } else {
           storeChange('objects', oldValue, objects) 
        }
    }
}

function setSize_helper(event, id, direction) {
    // direction 0 is x / 1 is y
    let field = document.getElementById(id)
    field.style.color = "black"
    field.style.backgroundColor = "white"
    if(event.key === 'Enter') {
        blurAll(id)
        let inpNr = parseFloat(field.value)
        if ((isFloat(inpNr) || isInt(inpNr)) && inpNr > 0 && highlighted > 0 && highlighted < objColors.length) {
            field.style.backgroundColor = "green"
            let center = findCenter(objects[highlighted])
            objects[highlighted] = rotateObject2d(objects[highlighted], -1 * objRotations[highlighted])
            const bb = boundingBox(objects[highlighted])
            // bb -> highest,rightmost,lowest,leftmost
            const currentSize = bb[(1-direction)] - bb[(1-direction) + 2]
            const diff = inpNr / currentSize
            for (let i = 0; i < objects[highlighted].length; i++) {
                objects[highlighted][i][direction] -= bb[(1-direction) + 2]
                objects[highlighted][i][direction] *= diff
                objects[highlighted][i][direction] += bb[(1-direction) + 2]
            }
            objects[highlighted] = rotateObject2d(objects[highlighted], objRotations[highlighted])

            objects[highlighted] = setCenter(objects[highlighted], center)
            setGrabPoint()
        } else {
            field.style.backgroundColor = "red"
            console.log("not a valid float or negative float or invalid higlight")
        }
    }
}

function scaleSize(event, id, direction) {
    if(event.key === 'Enter') {
        let oldValue = getDeepCopy(objects)
        scaleSize_helper(event, id, direction)
        if(isOverlap_objNr_all(highlighted)) {
            copyToConstArray(objects, oldValue)
            document.getElementById(id).style.backgroundColor = "red"
        } else {
           storeChange('objects', oldValue, objects) 
        }
    }
}

function scaleSize_helper(event, id, direction) {
    // direction 0 is x / 1 is y
    let field = document.getElementById(id)
    field.style.color = "black"
    field.style.backgroundColor = "white"
    if(event.key === 'Enter') {
        blurAll(id)
        let inpNr = parseFloat(field.value)
        if ((isFloat(inpNr) || isInt(inpNr)) && inpNr > 0 && highlighted > 0 && highlighted < objColors.length) {
            field.style.backgroundColor = "green"
            const bb = boundingBox(objects[highlighted])
            let center = findCenter(objects[highlighted])

            objects[highlighted] = rotateObject2d(objects[highlighted], -1 * objRotations[highlighted])
            // bb -> highest,rightmost,lowest,leftmost
            for (let i = 0; i < objects[highlighted].length; i++) {
                objects[highlighted][i][direction] -= bb[(1-direction) + 2]
                objects[highlighted][i][direction] *= inpNr
                objects[highlighted][i][direction] += bb[(1-direction) + 2]
            }
            objects[highlighted] = rotateObject2d(objects[highlighted], objRotations[highlighted])

            objects[highlighted] = setCenter(objects[highlighted], center)
            setGrabPoint()
        } else {
            field.style.backgroundColor = "red"
            console.log("not a valid float or negative float or invalid higlight")
        }
    }
}

function changeObjPos(obj, diff) {
    // called in input listener
    let oldValue = getDeepCopy(objects)
    obj.forEach(point => {
        point[0] += diff[0]
        point[1] += diff[1]
    })
    if(isOverlap_objNr_all(highlighted)) {
        copyToConstArray(objects, oldValue)
        return false
    } else {
       storeChange('objects', oldValue, objects)
       return true
    }
}

function setObjPos(event, id, direction) {
    if(event.key === 'Enter') {
        let oldValue = getDeepCopy(objects)
        setObjPos_helper(event, id, direction)
        if(isOverlap_objNr_all(highlighted)) {
            copyToConstArray(objects, oldValue)
            document.getElementById(id).style.backgroundColor = "red"
        } else {
            storeChange('objects', oldValue, objects) 
        }
    }
}

function setObjPos_helper(event, id, direction) {
    // direction 0 is x / 1 is y
    let field = document.getElementById(id)
    field.style.color = "black"
    field.style.backgroundColor = "white"
    if(event.key === 'Enter') {
        blurAll(id)
        let inpNr = parseFloat(field.value)
        if ((isFloat(inpNr) || isInt(inpNr)) && inpNr > 0 && highlighted > 0 && highlighted < objColors.length) {
            field.style.backgroundColor = "green"
            const bb = boundingBox(objects[highlighted])
            // bb -> highest,rightmost,lowest,leftmost
            let bb_pos = 3
            if(direction == 1) bb_pos = 0
            const diffToNew = inpNr - bb[bb_pos]
            objects[highlighted].forEach(point => {
                point[direction] += diffToNew
            })
            setGrabPoint()
        } else {
            field.style.backgroundColor = "red"
            console.log("not a valid float or negative float or invalid higlight")
        }
    }
}

function setObjZ(event, botOrTop) {
    if(event.key === 'Enter') {
        blurAll(event.target.id)
        let oldValue = getDeepCopy(objZLimits)
        let inpNr = parseFloat(event.target.value)
        if(isInt(inpNr) && highlighted > 0 && (botOrTop == 0 && inpNr >= objZLimits[highlighted][1]) && (botOrTop == 1 && inpNr <= objZLimits[highlighted][0])) {
            document.getElementById(event.target.id).style.backgroundColor = "red"
            console.log("lower bound higher then upper or invalid higlight or non int input", highlighted, objZLimits[highlighted])
        } else {
            event.target.style.backgroundColor = "green"
            objZLimits[highlighted][botOrTop] = event.target.value
            storeChange('objZLimits', oldValue, objZLimits)
        }
    }
}

function rotate(event, id) {
    if(event.key === 'Enter') {
        let oldObjectsFull = getObjectsFull()
        let oldValueObjects = getDeepCopy(objects)
        let oldValueRotations = getDeepCopy(objRotations)
        rotate_helper(event, id)
        if(isOverlap_objNr_all(highlighted)) {
            copyToConstArray(objects, oldValueObjects)
            copyToConstArray(objRotations, oldValueRotations)
            document.getElementById(id).style.backgroundColor = "red"
        } else {
            storeChange('fullObjects', oldObjectsFull, getObjectsFull())
        }
    }
}

function rotate_helper(event, id) {
    let field = document.getElementById(id)
    field.style.color = "black"
    field.style.backgroundColor = "white"
    if(event.key === 'Enter') {
        blurAll(id)
        let angle = parseFloat(field.value)
        if ((isFloat(angle) || isInt(angle)) && highlighted > 0 && highlighted < objColors.length) {
            field.style.backgroundColor = "green"
            objects[highlighted] = rotateObject2d(objects[highlighted], degreesToRadians(angle) - objRotations[highlighted])
            objRotations[highlighted] = degreesToRadians(angle)
            setGrabPoint()
        } else {
            field.style.backgroundColor = "red"
            console.log("not a valid float or invalid higlight")
        }
    }
}

function toggleShowPreserveEnergy() {
    toggleShowPreserveEnergy_helper()
    storeChange('showEnergyPreservationTerm', !showEnergyPreservationTerm, showEnergyPreservationTerm)
}

function toggleShowPreserveEnergy_helper() {
    showEnergyPreservationTerm = !showEnergyPreservationTerm
    document.getElementById("showPreserveEnergy").innerHTML = "preserve energy: " + showEnergyPreservationTerm
}

function toggleShowHighlightObjColor(event) {
    if(highlighted > 0 && highlighted < objColors.length) {
        let oldValue = getDeepCopy(objFactors)
        objFactors[highlighted][4] = !objFactors[highlighted][4]
        storeChange('objFactors', oldValue, objFactors)
        document.getElementById(event.target.id).innerHTML = "Highlight dyed with object color:  " + objFactors[highlighted][4]
    }
}

function changeCameraPosition(dimension, multValue) {
    // called by input listeners
    let oldVal = getCameraFull()
    cameraPos[dimension] += multValue * holdKeySpeedup
    storeChange('fullCamera', oldVal, getCameraFull())
}