function toggleGPU(id) {
    useCPU = !useCPU
    document.getElementById(id).innerHTML = 'using GPU: '+ !useCPU
}

function start3d() {
    if(!continuous3D) window.requestAnimationFrame(draw3d)
}

function setContinuous(id) {
    let hasConfirmed = true
    if (!continuous3D) {
        hasConfirmed = confirm(`This can slow down your browser drastically on higher resolutions\n
You can turn the resolution down with the "size" slider below the canvas!\n
Are you sure you want to activate continuous 3D canvas?`)
    }

    if (hasConfirmed) {
        continuous3D = !continuous3D
        document.getElementById(id).innerHTML = 'continuous3d: '+ continuous3D
        if(continuous3D) {
            window.requestAnimationFrame(draw3d)
        }
    }
}

function setSize_3d(event) {
    // set size relative to canvas size proportions

    size3d = parseInt(event.target.value) / 100
    updateHover('sliderHover', 'size: ' + Math.round(size3d*100) + '%')
}

function setFov_3d(event) {
    // set fov relative to canvas proportions

    fovStep = parseInt(event.target.value) / 100
    updateHover('sliderHover', 'fov: ' + Math.round(fovStep*100))
}