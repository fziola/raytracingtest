function openMenu(id) {
    let menu = document.getElementById(id)
    let blockScreen = document.getElementById("blockScreenId")
    if (menu.style.display == "none") {
        // reset all menus so no 2 menus are open
        document.getElementById("options").style.display = "none"
        document.getElementById("about").style.display = "none"
        menu.style.display = "block"
        blockScreen.style.display = "block"
        menuOpen = true
    } else {
        menu.style.display = "none"
        blockScreen.style.display = "none"
        menuOpen = false
    }
}

function closeMenus() {
    const menus = document.getElementsByClassName('menu')
    for (var menu = 0; menu < menus.length; menu++) {
      menus[menu].style.display = "none"
    }

    document.getElementById("blockScreenId").style.display = "none"
    menuOpen = false
}