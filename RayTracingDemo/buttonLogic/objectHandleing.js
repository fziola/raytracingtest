function addRectRandomColorMat(xA,yA,xB,yB) {
    let r = Math.round(Math.random()*255)
    let g = Math.round(Math.random()*255)
    let b = Math.round(Math.random()*255)
    // refl + refr <= 1
    let refl = 1 //Math.random()
    let refr = 0
    let refr_index = 3
    drawArray = []

    let smallX = xA
    let bigX = xB
    let smallY = yA
    let bigY = yB
    if(xA > xB) {
        smallX = xB
        bigX = xA
    }
    if(yA > yB) {
        smallY = yB
        bigY = yA
    }
    // arange counter-clockwise
    let obj = [[smallX,smallY],[smallX,bigY],[bigX,bigY],[bigX,smallY]]
    addObjectSetColorMat(obj, [r,g,b], [refl,refr,refr_index])
}

function addDrawnObjRandomColorMat(obj) {
    let r = Math.round(Math.random()*255)
    let g = Math.round(Math.random()*255)
    let b = Math.round(Math.random()*255)
    // refl + refr <= 1
    let refl = 1 //Math.random()
    let refr = 0
    let refr_index = 3
    drawArray = []

    addObjectSetColorMat(obj, [r,g,b], [refl,refr,refr_index])
}

function addObjectSetColorMat(obj, color, material) {
    addObject(obj, color, material, standardObjFactors(), 0)
}

function addObject_fromJSObj(js_obj) {
    // see objInClipboard
    addObject(getDeepCopy(js_obj.points), getDeepCopy(js_obj.color), getDeepCopy(js_obj.materials), getDeepCopy(js_obj.factors), getDeepCopy(js_obj.rotation))
}

function addObject(obj, color, material, factors, rotation) {
    // sortieren gg Uhrzeigersinn, sonst werden die normals des obj falsch berechnet
    if(arePointsClockwise(obj)) {
        let tempObj = obj.slice()
        for(var i = 0; i < obj.length; i++) {
            obj[i] = tempObj[obj.length-i-1]
        }
    }

    if(!isOverlap_obj_all(obj)) {
        const oldObjectsFull = getObjectsFull()
        objects.push(obj)
        objZLimits.push([250,750])
        objColors.push(color)
        objMaterials.push(material)
        objFactors.push(factors)
        objRotations.push(rotation)
        storeChange('fullObjects', oldObjectsFull, getObjectsFull())
    }
}

function arePointsClockwise(obj) {
    let sum = 0
    for(var i = 0; i < obj.length; i++) {
        sum += (obj[(i+1)%obj.length][0] - obj[i][0]) * (obj[(i+1)%obj.length][1] + obj[i][1])
    }
    return (sum < 0)
}

function addLight(x,y) {
    const oldLightsFull = getLightsFull()

    lights.push([x,y,500])
    lightColors.push([Math.round(Math.random()*255),Math.round(Math.random()*255),Math.round(Math.random()*255)])
    deselectAddLight(document.getElementById('addLightBut'))

    const newLightsFull = getLightsFull()
    storeChange('fullLights', oldLightsFull, newLightsFull)
}

function setEditInputFields() {
    resetInputField("inputHighlightNr")
    resetInputField("inputObj_kd")
    resetInputField("inputObj_ks")
    resetInputField("inputObj_n")
    resetInputField("inputObj_ka")
}

function resetInputField(id) {
    let fieldTemp = document.getElementById(id)
    fieldTemp.value = ''
    fieldTemp.style.color = "black"
    fieldTemp.style.backgroundColor = "white"
}

function setHighlightObject(highlight) {
    highlighted = highlight
    setEditInputFields()
    document.getElementById("inputHighlightNr").value = String(highlighted)
    if (highlighted > 0) {
        openObjectEditMenu()
        setGrabPoint()
    } else {
        document.getElementById("inputHighlightNr").style.backgroundColor = "red"
    }
}

function setHighlightLight(highlightLight) {
    highlightedLight = highlightLight
    resetInputField("inputHighlightLightNr")
    if (highlightedLight >= 0) {
        openLightEditMenu()
    } else {
        document.getElementById("inputHighlightLightNr").style.backgroundColor = "red"
    }
    document.getElementById("inputHighlightLightNr").value = String(highlightedLight)
}

function unsetHighlightObj() {
    let t = document.getElementById("editObjectPopup")
    t.style.display = "none"
    highlighted = -1
}

function unsetHighlightLight() {
    let t = document.getElementById("editLightPopup")
    t.style.display = "none"
    highlightedLight = -1
}

function updateCurrentEditMenu() {
    if(highlighted > 0 && highlighted < objects.length) {
        // document.getElementById('inputHighlightNr').value = highlighted // done seperately in setHighlightObject
        document.getElementById('inputObjColor').value = rgbArrayToHex(objColors[highlighted])
        document.getElementById('inputObj_refl').value = objMaterials[highlighted][0] * 100
        document.getElementById('inputObj_refr').value = objMaterials[highlighted][1] * 100
        document.getElementById('inputObj_refrIndex').value = objMaterials[highlighted][2]

        document.getElementById('inputObj_kd').value = objFactors[highlighted][0]
        document.getElementById('inputObj_ks').value = objFactors[highlighted][1]
        document.getElementById('inputObj_n').value = objFactors[highlighted][2]
        document.getElementById('inputObj_ka').value = objFactors[highlighted][3]
        const bb = boundingBox(rotateObject2d(objects[highlighted], -1 * objRotations[highlighted]))
        document.getElementById('inputObj_width').value = Math.round(bb[1] - bb[3])
        document.getElementById('inputObj_height').value = Math.round(bb[0] - bb[2])

        document.getElementById('inputObj_rotation').value = radiansToDegrees(objRotations[highlighted])
        document.getElementById('inputObj_posX').value = Math.round(bb[3])
        document.getElementById('inputObj_posY').value = Math.round(bb[0])
        document.getElementById('inputObj_Z0').value = objZLimits[highlighted][0]
        document.getElementById('inputObj_Z1').value = objZLimits[highlighted][1]

        // the 'showPreserveEnergy' is done seperately cuz global
        document.getElementById('showHighlightObjColor').innerHTML = "Highlight dyed with object color:  " + objFactors[highlighted][4]
    } else if(highlightedLight >= 0 && highlightedLight < lights.length) {
        document.getElementById('inputLightIntensity').value = rgbArrayToHex(lightColors[highlightedLight])
        document.getElementById('inputLight_posX').value = Math.round(lights[highlightedLight][0])
        document.getElementById('inputLight_posY').value = Math.round(lights[highlightedLight][1])
    } else if(highlightedCam) {
        document.getElementById('inputNumberRays').value = numberOfRays
        document.getElementById('inputCameraAngle').value = radiansToDegrees(angle2d([0,-1],cameraDir.slice(0, -1)))
        document.getElementById('inputCamera_posX').value = Math.round(cameraPos[0])
        document.getElementById('inputCamera_posY').value = Math.round(cameraPos[1])
    }
}

function openObjectEditMenu() {
    if(highlighted > 0 && highlighted < objects.length) {
        // document.getElementById('inputHighlightNr').value = highlighted // done seperately in setHighlightObject
        document.getElementById('inputObjColor').value = rgbArrayToHex(objColors[highlighted])
        document.getElementById('inputObj_refl').value = objMaterials[highlighted][0] * 100
        document.getElementById('inputObj_refr').value = objMaterials[highlighted][1] * 100
        document.getElementById('inputObj_refrIndex').value = objMaterials[highlighted][2]

        document.getElementById('inputObj_kd').value = objFactors[highlighted][0]
        document.getElementById('inputObj_ks').value = objFactors[highlighted][1]
        document.getElementById('inputObj_n').value = objFactors[highlighted][2]
        document.getElementById('inputObj_ka').value = objFactors[highlighted][3]
        const bb = boundingBox(rotateObject2d(objects[highlighted], -1 * objRotations[highlighted]))
        document.getElementById('inputObj_width').value = Math.round(bb[1] - bb[3])
        document.getElementById('inputObj_height').value = Math.round(bb[0] - bb[2])

        document.getElementById('inputObj_rotation').value = radiansToDegrees(objRotations[highlighted])
        document.getElementById('inputObj_posX').value = Math.round(bb[3])
        document.getElementById('inputObj_posY').value = Math.round(bb[0])
        document.getElementById('inputObj_Z0').value = objZLimits[highlighted][0]
        document.getElementById('inputObj_Z1').value = objZLimits[highlighted][1]
        // the 'showPreserveEnergy' is done seperately cuz global
        document.getElementById('showHighlightObjColor').innerHTML = "Highlight dyed with object color:  " + objFactors[highlighted][4]
    } else {
        console.log('opened menu without setting highlighted')
    }

    closeAllEditMenus()
    let t = document.getElementById("editObjectPopup")
    t.style.display = "block"
}

function openLightEditMenu() {
    if(highlightedLight >= 0 && highlightedLight < lights.length) {
        // document.getElementById('highlightedLight').value = highlightedLight // done seperately in setHighlightLight
        document.getElementById('inputLightIntensity').value = rgbArrayToHex(lightColors[highlightedLight])
        document.getElementById('inputLight_posX').value = Math.round(lights[highlightedLight][0])
        document.getElementById('inputLight_posY').value = Math.round(lights[highlightedLight][1])
    } else {
        console.log('opened menu without setting highlightedLight')
    }
    closeAllEditMenus()
    let t = document.getElementById("editLightPopup")
    t.style.display = "block"
}

function openCameraMenu() {
    closeAllEditMenus()
    document.getElementById('inputNumberRays').value = numberOfRays
    document.getElementById('inputCameraAngle').value = radiansToDegrees(angle2d([0,-1],cameraDir.slice(0, -1)))
    document.getElementById('inputCamera_posX').value = Math.round(cameraPos[0])
    document.getElementById('inputCamera_posY').value = Math.round(cameraPos[1])
    let t = document.getElementById("editCameraPopup")
    t.style.display = "block"
}

function closeAllEditMenus() {
    let to = document.getElementById("editObjectPopup")
    to.style.display = "none"
    let tl = document.getElementById("editLightPopup")
    tl.style.display = "none"
    let tc = document.getElementById("editCameraPopup")
    tc.style.display = "none"
}

function removeHighlightedObject() {
    const oldObjectsFull = getObjectsFull()

    objects.splice(highlighted, 1)
    objColors.splice(highlighted, 1)
    objMaterials.splice(highlighted, 1)
    objFactors.splice(highlighted, 1)
    objRotations.splice(highlighted, 1)
    unsetHighlightObj()

    const newObjectsFull = getObjectsFull()
    storeChange('fullObjects', oldObjectsFull, newObjectsFull)
}

function removeHighlightedLight() {
    const oldLightsFull = getLightsFull()

    lights.splice(highlightedLight, 1)
    lightColors.splice(highlightedLight, 1)
    unsetHighlightLight()

    const newLightsFull = getLightsFull()
    storeChange('fullLights', oldLightsFull, newLightsFull)
}