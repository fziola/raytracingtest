This project aims to provide an interactive way of learning about the "RayTracing" algorithm.
To achieve this it shows a top-down view on a scene, in which the rays are visible. It also provides popups with the current formulas, to help understand difficult calculations.
In addition it provides the result of this raytracing in a 3d-view.

To run tests type runAllTests(); in the browser console (F12), they can be edited in tests/test.js.