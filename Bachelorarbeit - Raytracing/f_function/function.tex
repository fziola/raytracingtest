% !TEX root = Main.tex
\newpage

\section{Grundlagen des Raytracings}
\label{formeln}

\subsection{Strahlverfolgung}

Von der Kameraposition wird durch den Bildschirm für jedes Pixel ein Strahl gesendet. Zur Veranschaulichung ist der Bildschirm hier 1x2 Pixel, siehe dazu \cite{wikiRayTrace}, \cite{nvidia} und \cite{rtrIntersect}.

\begin{figure}[h]
	\centering
	\includegraphics[width=\linewidth]{Figures/reflOne.pdf}
	\caption{Strahlverfolgung im Prototyp}
	\label{b-demo-strahlverfolgung}
\end{figure}
\noindent
Dieser Strahl (orange in Abb. \ref{b-demo-strahlverfolgung}) wird in die Szene ausgesandt und je nach Material des Objektes, auf das er trifft, gebrochen und reflektiert. Dann wird die Farbe an jeder getroffenen Stelle berechnet (siehe Formel \ref{formel_phong}) und trägt anteilig zur Farbe des Pixels bei. So entsteht das Bild der Szene auf dem Bildschirm (siehe Abb. \ref{b-3d-screen}).\\
\noindent
Die Farbe an jeder getroffenen Stelle richtet sich dabei nach der Farbe des Objektes und des einfallenden Lichtes, sowie dessen Winkel.

\begin{figure}[h]
	\centering
	\includegraphics[width=0.5\linewidth]{Figures/nvidia_3d_raytracing_image.jpg}
	\caption{Kamera mit Bildschirm im dreidimensionalen Raum \cite{nvidia}}
	\label{b-3d-screen}
\end{figure}

\newpage

\subsubsection{Reflektion}
Wie in Abb. \ref{b-anwendung-3d-refl} zu sehen, wird bei spiegelnden Objekten an der Stelle, an der das Objekt vom Aug-Strahl ($\vec{v}$) getroffen wird, ein Strahl in die gespiegelte Richtung ($\vec{d}$) fortgesetzt, siehe Formel \ref{formel_refl} und Abb. \ref{b-expl_refl}. Die Farbe, welche an der Schnittstelle dieses Strahls berechnet wird, wird mit der lokal berechneten Farbe verrechnet. Dadurch erscheinen farblich angepasste Reflektionen.

{\small
	\begin{equation}
		\vec{d} = \vec{v} - (\vec{n} * 2 * (\vec{v} \circ \vec{n}))
		\label{formel_refl}
	\end{equation}
}
\begin{figure}[h]
	\centering
	\includegraphics[width=0.4\linewidth]{Figures/explainImg/expl_refl.PNG}
	\caption{Reflektion des Vektors $\vec{v}$}
	\label{b-expl_refl}
\end{figure}
\begin{figure}[h]
	\centering
	\includegraphics[width=0.7\linewidth]{Figures/rbp_2_3dB.PNG}
	\caption{Darstellung farbiger Spiegel im 3D-Teil der Anwendung}
	\label{b-anwendung-3d-refl}
\end{figure}

\newpage

\subsubsection{Refraktion}
In Abb. \ref{refr_2d} ist ein Strahl zu sehen, welcher durch ein semi-transparentes Objekt gesendet und dabei gebrochen wird (die Kreise stellen jeweils die Phong-Beleuchtung entlang des einflussreichsten Strahles dar) . Wie auch bei der Reflektion wird die Farbe aus der Richtung des gebrochenen Strahls anteilig verrechnet. Anders als bei der Reflektion gibt es bei der Brechung jedoch einige weitere Berechnungen. So müssen innerhalb des Objektes auch Reflektionen durchgeführt werden, sog. \enquote{interne Reflektionen}. Sollte der Brechungswinkel größer als $90^\circ$ werden und dadurch nicht mehr die Fläche durchdringen, findet statt dessen eine \enquote{Totalreflektion} statt. Der Brechungswinkel hängt dabei von den Brechungsindexen ($\eta$) der einzelnen Materialien ab, siehe \cite{scratch-refr}.

{\small
\begin{equation}
	\vec{d} = \dfrac{\eta_{0}}{\eta_{1}} (\vec{v} + cos(\theta_0) * \vec{n}) \sqrt{1- (\dfrac{\eta_{0}}{\eta_{1}})^2 sin^2(\theta_0)}
\end{equation}
Wobei Kosinus als
\begin{equation}
	cos(\theta_0) = \vec{n} \circ \vec{v}
\end{equation}
und Sinus als
\begin{equation}
	sin^2(\theta_0) = 1-cos^2(\theta_0)
\end{equation}
berechnet werden können.
}
\begin{figure}[h]
	\centering
	\includegraphics[width=0.6\linewidth]{Figures/explainImg/expl_refr.PNG}
	\caption{Brechung des Strahls}
	\label{b-expl_refr}
\end{figure}
\begin{figure}[h]
	\centering
	\includegraphics[width=0.4\linewidth]{Figures/refr_example_2d.png}
	\caption{Brechung in der Draufsicht}
	\label{refr_2d}
\end{figure}

\newpage

\subsection{Schnittpunktberechnung}
Die Berechnung von Schnittpunkten der Strahlen mit den Objekten (siehe \cite{rtrIntersect}) ist ein essentieller Teil von Raytracing. Der Strahl, welcher von der Kamera aus in die Szene ausgesandt wird, muss dabei mit jeder Fläche der Objekte einen Schnittpunkttest machen (siehe Abb. \ref{b-all_hits}). Falls Schnittpunkte gefunden werden, wird der Schnittpunkt mit der kürzesten Distanz zum Startpunkt gewählt, da dieser zum Objekt im Vordergrund gehört und alle anderen getroffenen Objekte verdeckt.
\begin{figure}[H]
	\centering
	\includegraphics[width=0.43\linewidth]{Figures/explainImg/all_hits.png}
	\caption{Alle Schnittpunkte des Strahls und der Objekte}
	\label{b-all_hits}
\end{figure}

\subsubsection{2D-Schnittpunkt}
Im Zweidimensionalen bestehen Objekte nicht aus Flächen sondern Liniensegmenten. Für den Test seien die Punkte $p_a$ (Pixel des Bildschirms), $p_b$ (Punkt in Blickrichtung) und die Eckpunkte $p_c$, $p_d$ des Objektes gegeben (siehe Abb. \ref{b-expl_2d_intersect}).
\begin{equation}
	\vec{ab} = p_b - p_a
\end{equation}
\begin{equation}
	\vec{cd} = p_d - p_c
\end{equation}
\begin{equation}
	denom = (\vec{cd}[1])*(\vec{ab}[0]) - (\vec{cd}[0])*(\vec{ab}[1])
\end{equation}
\begin{equation}
	u_a = \dfrac{(p_d[0] - p_c[0])*(p_a[1] - p_c[1]) - (p_d[1] - p_c[1])*(p_a[0] - p_c[0])}{denom}
\end{equation}
\begin{equation}
   	u_b = \dfrac{(p_b[0] - p_a[0])*(p_a[1] - p_c[1]) - (p_b[1] - p_a[1])*(p_a[0] - p_c[0])}{denom}
\end{equation}
\begin{equation}
	P = p_a + u_a * (p_b - p_a)
\end{equation}
\begin{figure}[H]
	\centering
	\includegraphics[width=0.25\linewidth]{Figures/explainImg/expl_2d_intersect.png}
	\caption{Schnittpunkt von Strahl und Objektkante}
	\label{b-expl_2d_intersect}
\end{figure}

\newpage \noindent
Dabei müssen $denom$ ungleich 0 und $0 \leq u_a, u_b \leq 1$ sein (sonst liegt der Schnittpunkt zwar auf der Geraden, aber nicht auf dem Liniensegment). $P$ ist der Schnittpunkt mit dem Liniensegment. Der Ansatz dieses Verfahrens besteht daraus, die Formeln der Geraden
\begin{equation}
	L_1 = p_a + u_a * (p_b - p_a)
\end{equation}
und
\begin{equation}
	L_2 = p_c + u_b * (p_d - p_c)
\end{equation}
für den Fall $L_1 = L_2$ nach $u_a$ umzustellen. Eine ausführliche Herleitung ist in \cite{bourkeIntersect} zu finden.


\subsubsection{3D-Schnittpunkt}
Im Dreidimensionalen sei gegeben: die Fläche $F$ bestehend aus einem Array von Eckpunkten, die Normale der Fläche ($\vec{n}$) und der Strahl $S+t*\vec{d}$ (siehe Abb. \ref{b-expl_hit}). Zunächst wird auf Parallelität getestet. Im folgenden werden Gleichheitstests mit Toleranz $\epsilon$ durchgeführt. Dies ist ein extrem kleiner Betrag (z.B. $2^{-20}$) und wirkt der Rechenungenauigkeit des Computers entgegen. $dotProduct(a,b)$ bezeichnet das Vektorprodukt von a und b.
\begin{equation}
	denom = dotProduct(\vec{n}, \vec{d})
\end{equation}
$|denom|$ muss ungleich 0 sein, um Parallele zu vermeiden.
\begin{equation}
	t = \dfrac{dotProduct(\vec{n}, F[0]) - dotProduct(\vec{n}, S)}{denom}
\end{equation}
Mit $t$ wird die Entfernung zur Ebene, welche die Fläche aufspannt, berechnet (siehe \cite{scratch-hit}). Dabei sollte $t>0$ sein, da sonst Objekte hinter der Kamera mit betrachtet werden. Jetzt muss getestet werden, ob der berechnete Punkt $P$ innerhalb der Fläche liegt.\\
\begin{figure}[H]
	\centering
	\includegraphics[width=0.5\linewidth]{Figures/explainImg/expl_hit.png}
	\caption{Zwei Strahlen, welche die Ebene schneiden, aber nur S schneidet das Polygon}
	\label{b-expl_hit}
\end{figure}
\noindent
\textbf{2D-Transformation:}\\\noindent
Um zu testen, ob der Punkt in der Fläche $F$ liegt, wird ein Polygontest im zweidimensionalen durchgeführt. Dazu werden alle Punkte der Ebene sowie der berechnete Punkt $P$ auf diese Ebene projiziert (siehe Abb. \ref{b-expl_transform}), welche nun als neue Basis für das Koordinatensystem mit Ursprung $F[0]$ und Achsen $\vec{x}$ und $\vec{y}$ gilt. Siehe dazu \cite{2d-transform}.
\begin{equation}
	P = P - F[0]
\end{equation}
\begin{equation}
	\vec{x} = \dfrac{F[1] - F[0]}{|F[1] - F[0]|}
\end{equation}
\begin{equation}
	\vec{z} = \dfrac{\vec{n}}{|\vec{n}|}
\end{equation}
\begin{equation}
	\vec{y} = \vec{z} \times  \vec{x}
\end{equation}

\begin{equation}
	P' =
	\begin{pmatrix}
		\vec{x}[0] & \vec{x}[1] & \vec{x}[2]\\
		\vec{y}[0] & \vec{y}[1] & \vec{y}[2]\\
		\vec{z}[0] & \vec{z}[1] & \vec{z}[2]\\
	\end{pmatrix} \times \begin{pmatrix}
	P[0]\\
	P[1]\\
	P[2]\\
\end{pmatrix}
\end{equation}
Dabei sollte, da $P$ bereits auf der Ebene liegt, $P'[2] = 0$ sein. Die Punkte werden daher im Polygontest als zweidimensional betrachtet.
\begin{figure}[H]
	\centering
	\includegraphics[width=0.7\linewidth]{Figures/explainImg/expl_transform2.png}
	\caption{Transformation ins Zweidimensionale}
	\label{b-expl_transform}
\end{figure}
\newpage \noindent
\textbf{Polygontest:}\\\noindent
Für jede Ecke der Fläche $F'[i]$ und die nächste Ecke $F'[(i+1)\%l_F]$ wird der Winkel der Vektoren zum Punkt $P'$ berechnet (siehe Abb. \ref{b-expl_one_angle} und Abb. \ref{b-expl_angle}). $l_F$ ist die Anzahl der Eckpunkte. $acos(x)$ ist der Arkuskosinus von x.

\begin{figure}[H]
	\centering
	\includegraphics[width=0.3\linewidth]{Figures/explainImg/le_angle2.png}
	\caption{Winkel zwischen Vektoren zu benachbarten Ecken des Objektes}
	\label{b-expl_one_angle}
\end{figure}
\begin{equation}
	\vec{t_a} = F'[i] - P'
\end{equation}
\begin{equation}
	\vec{t_b} = F'[(i+1)\%l_F] - P'
\end{equation}
\begin{equation}
	angle = acos(\dfrac{\vec{t_a} \circ \vec{t_b}}{|\vec{t_a}| * |\vec{t_b}|})
\end{equation}

Das 2D-Kreuzprodukt bestimmt, ob sich die Punkte im Uhrzeigersinn folgen.
\begin{equation}
	clockwise = (\vec{t_b}[1] * \vec{t_a}[0] - \vec{t_b}[0] * \vec{t_a}[1]) < 0
\end{equation}
Die Winkel werden aufsummiert, wobei der Uhrzeigersinn das Vorzeichen bestimmt.
\begin{equation}
	total = \sum_{0}^{l_f-1}(angle ( * -1 \textrm{ falls nicht }clockwise))
\end{equation}
Wenn $P'$ im, durch die Punkte begrenzten, Polygon liegt ist $|total|=2\pi$ $(360^\circ)$, also rund $6,28319$. Andernfalls ist $|total| < 2\pi$. Wenn der Punkt genau auf dem Rand des Objektes liegt ergibt die Summe der Winkel $180^\circ$. Falls er außerhalb liegt gleichen sich die Winkel zu $0^\circ$ aus. Siehe zum Polygontest \cite{windingNumber}.

\begin{figure}[H]
	\centering
	\includegraphics[width=0.7\linewidth]{Figures/explainImg/expl_angle2.png}
	\caption{Winkel (gerundet) innerhalb eines Polygons}
	\label{b-expl_angle}
\end{figure}

\newpage

\subsection{Phong-Beleuchtungsmodell}
Die diffusen und spekularen Anteile werden jeweils für jede Lichtquelle $m \in L$ bestimmt und dann aufsummiert. Beide gehen nur in die Formel ein, falls die Lichtquelle nicht von einem Objekt verdeckt ist (siehe Abb. \ref{b-demo-phong-shadow-ray}).
Siehe dazu \cite{scratch} und \cite{wikiPhongBel}.
$I_{in}$ ist dabei die Intensität der Lichtquelle $m$. Sie wird meist in ihre RGB Komponenten (siehe Abb. \ref{b-demo-phong-3D}) aufgeteilt.
Zusätzlich ist $C_o$ die RGB Farbe der getroffenen Oberfläche.

\begin{figure}[h]
	\centering
	% trim={<left> <lower> <right> <upper>}
	\subcaptionbox{weißes Licht}{
		\centering
		\includegraphics[trim={80px 0 0 0}, clip, width=0.22\linewidth, height=150px]{Figures/3dlamp_phong/white_lamp_3d.png}
	}
	\subcaptionbox{blaues Licht}{
		\centering
		\includegraphics[trim={80px 0 0 0}, clip, width=0.22\linewidth, height=150px]{Figures/3dlamp_phong/blue_lamp_3d.png}
	}
	\subcaptionbox{rotes Licht}{
		\centering
		\includegraphics[trim={80px 0 0 0}, clip, width=0.22\linewidth, height=150px]{Figures/3dlamp_phong/red_lamp_3d.png}
	}
	\subcaptionbox{grünes Licht}{
		\centering
		\includegraphics[trim={80px 0 0 0}, clip, width=0.22\linewidth, height=150px]{Figures/3dlamp_phong/green_lamp_3d.png}
	}
	\caption{Einfluss der Lichtquellenfarbe auf das Objekt (Plastik)}
	\label{b-demo-phong-3D}
\end{figure}

\begin{figure}[h]
	\centering
	\includegraphics[width=0.49\linewidth]{Figures/3dlamp_phong/blocked _shadow_ray_2d.png}
	\includegraphics[width=0.49\linewidth]{Figures/3dlamp_phong/blocked _shadow_ray_3d.png}
	\caption{\\links: Blockierter Schattenfühler zeigt, dass das rote Objekt zwischen der Lichtquelle und dem Punkt auf dem Objekt liegt.\\rechts: auf dem blauen 3D-Objekt ist deutlich der vom roten 3D-Objekt geworfene Schatten zu sehen.}
	\label{b-demo-phong-shadow-ray}
\end{figure}

\newpage

\subsubsection{vollständige Formel}
Hier sind die gesammelten Formeln für die Phong-Beleuchtung, genauere Erklärungen sind auf der nächsten Seite zu finden. In Formel \ref{formel_phong} ist die vollständige Formel für ein Plastik-Objekt (siehe Kapitel \ref{spec}) zu finden. $I_{out}$ ist die letztendliche Farbe am Betrachteten Punkt des Objektes.
{\small
\begin{equation}
	AMB = I_a * K_a
	\label{formel_amb}
\end{equation}
\begin{equation}
	DIFF =  \sum_{m \in L}[( I_{in} * K_d * cos(\phi))] * daempfung
	\label{formel_diff}
\end{equation}
\begin{equation}
	SPEC = \sum_{m \in L}[( I_{in} * K_s * cos^\alpha(\theta) )] * daempfung
	\label{formel_spec}
\end{equation}
\begin{equation}
	I_{out} = AMB * C_o + DIFF * C_o + SPEC
\end{equation}
\begin{equation}
	I_{out} = I_a * K_a * C_o + daempfung * \sum_{m \in L}[( I_{in} * K_d * cos(\phi)  * C_o ) + ( I_{in} * K_s * cos^\alpha(\theta) )]
	\label{formel_phong}
\end{equation}
\centerline{
	dabei sind $K_a \leq 1$ und $K_d + K_s \leq 1$
}
}
\\\\\\\\\\

\begin{figure}[h]
	\centering
	\includegraphics[width=0.6\linewidth]{Figures/angles2.jpg}
	\caption{Winkel für die Berechnung nach dem Phong-Beleuchtungsmodell}
	\label{b-phong-angles}
\end{figure}

\newpage

\subsubsection{ambienter Anteil}
Der ambiente Anteil (Formel \ref{formel_amb}) ist eine globale Beleuchtung der Szene, um die Wellen-eigenschaft des Lichtes  einzuberechnen und damit Stellen zu beleuchten, welche keinen direkten Strahl zur Lichtquelle haben.
$I_a$ ist die Intensität des ambienten Lichtes. Das kann ein fester Wert z.B. FFFFFF oder ein Mittelwert über alle Lichtquellen der Szene sein.
$K_a$ ist ein Faktor (0 bis 1) des Materials. Er ist meistens über alle Objekte der Szene gleich und bestimmt, mit welchem Anteil die ambiente Lichtintensität das Objekt bestrahlt.

\subsubsection{diffuser Anteil}
$K_d$ ist der diffuse Faktor des Materials (0 bis 1). Er bestimmt den Einfluss des diffusen Anteils in Formel \ref{formel_diff}.
Durch das $cos(\phi)$ (siehe Abb. \ref{b-phong-angles}) wird die Intensität des eintreffenden Lichtes abgeschwächt, je größer der Winkel des Lichtes ist.

\subsubsection{spekularer Anteil}
\label{spec}
$K_s$ ist der spekulare Faktor des Materials (0 bis 1)
Durch das $cos^\alpha(\theta)$ (siehe Abb. \ref{b-phong-angles}) wird der Einfluss des spekularen Highlights in Formel \ref{formel_spec} berechnet.
$\alpha$ ist dabei ein Faktor des Materials und beeinflusst die Breite des Highlights. Je nach Material wird der spekulare Anteil mit der Farbe des Objektes eingefärbt: $SPEC' = SPEC * C_0$. Plastik reflektiert die Farbe der Lichtquelle ohne Einfärbung des spekularen Highlights, bei Metallen wird das Highlight in Objektfarbe gefärbt.
Der spekulare Anteil kann zusätzlich noch mit einem Korrekturterm (z.B. $\frac{\alpha + 2}{2 \pi}$) multipliziert werden. Dieser dient zur Energieerhaltung. Er sorgt dafür, dass auch bei großen Spiegelexponenten ($\alpha$) die Helligkeit nicht abnimmt.
\begin{equation}
	SPEC = \sum_{m \in L}[( I_{in} * K_s * cos^\alpha(\theta) )] * \frac{\alpha + 2}{2 \pi}
\end{equation}

\subsubsection{Distanz zur Lichtquelle}
Neben dem Korrekturterm gibt es noch weitere Möglichkeiten das Phong-Beleuchtungs-modell anzupassen. Die Distanz zur Lichtquelle ist eine Variable, welche noch nicht genauer in Formel \ref{formel_phong} beschrieben wird. Sie kann jeweils auf die Lichtquellen abhängigen Terme angewendet werden. Dabei wird die Distanz meist linear oder quadratisch einbezogen. Hier ist $dist$ die Distanz vom betrachteten Punkt zur Lichtquelle und $F$ jeweils ein fester Faktor. Formel \ref{formel_phong_daempf} aus \cite{phongDist}.

\begin{equation}
	daempfung = 1 / (dist * dist * F_{quad} + dist * F_{lin} + F_{const})
	\label{formel_phong_daempf}
\end{equation}
\begin{equation}
	SPEC' = SPEC_{alt} * daempfung
\end{equation}
\begin{equation}
	DIFF' = DIFF_{alt} * daempfung
\end{equation}

\newpage

\subsubsection{Anteile visuell}
\begin{figure}[h]
	\centering
	% trim={<left> <lower> <right> <upper>}
	\subcaptionbox{ambienter Anteil\label{b-phong-2D-a}}{
		\centering
		\includegraphics[trim={100px 50px 500px 20px}, clip, width=0.22\linewidth, height=150px]{Figures/amb.png}
	}
	\subcaptionbox{diffuser Anteil\label{b-phong-2D-b}}{
		\centering
		\includegraphics[trim={100px 50px 500px 20px}, clip, width=0.22\linewidth, height=150px]{Figures/dif.png}
	}
	\subcaptionbox{spekularer Anteil\label{b-phong-2D-c}}{
		\centering
		\includegraphics[trim={100px 50px 500px 20px}, clip, width=0.22\linewidth, height=150px]{Figures/spec.png}
	}
	\subcaptionbox{Summe\label{b-phong-2D-d}}{
		\centering
		\includegraphics[trim={100px 50px 500px 20px}, clip, width=0.22\linewidth, height=150px]{Figures/total.png}
	}
	\caption{Anteile der Phong-Beleuchtung entlang eines 2D-Objektes}
	\label{b-phong-2D}
\end{figure}
\noindent
Wie in Abb. \ref{b-phong-2D-a} zu sehen ist, sorgt der ambiente Anteil für eine gleichmäßige Grundfarbe des Objektes, unabhängig von der tatsächlichen Beleuchtung. Daneben in Abb. \ref{b-phong-2D-b} sind es die diffusen Anteile welche dafür sorgen, dass das Objekt in Richtung der Lichtquelle stärker beleuchtet ist. Rechts  davon (Abb. \ref{b-phong-2D-c}) sind die spekularen Highlights zu sehen. Sie sind eine direkte Reflektion des Lichtes und damit zwischen Auge und Lichtquelle. Durch die Summe dieser Anteile (Abb. \ref{b-phong-2D-d}) erhält man die letztendliche Farbe am betrachteten Schnittpunkt von Augstrahl und Objekt. Diese Anteile sind in Abb. \ref{b-phong-3D} jeweils für ein 3D-Objekt zu sehen.
\begin{figure}[h]
	\centering
	\includegraphics[width=\linewidth]{Figures/phong.png}
	\caption{Phong-Beleuchtung im dreidimensionalen Raum \cite{wikiPhongImg}}
	\label{b-phong-3D}
\end{figure}
\newpage