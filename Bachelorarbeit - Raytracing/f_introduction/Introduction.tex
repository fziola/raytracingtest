% !TEX root = Main.tex
\newpage
\section{Einleitung}

\subsection{Aufgabenstellung}

Interaktive Visualisierung des Raytracing-Prinzips:\\
In der Vorlesung \enquote{Grundlagen der Computergrafik} werden zum Thema globale Beleuchtung unter anderem Grundlagen zum Raytracing Algorithmus vermittelt. Dazu enthält das Lehrmaterial bereits statische 2D-Grafiken zur Veranschaulichung. Für das bessere Verständnis sollen Web-Applets mit vielen Eingabe-Parametern erstellt werden, in denen Lehrende die Prinzipien veranschaulichen und Studierende sie als Selbstversuch testen können.

\subsubsection{Inspiration durch Ergebnisse eines Hauptseminars}
Als Beispiel für sinnvollen Funktionsumfang und Interaktionsmöglichkeiten dieser Arbeit dient das Hauptseminar von Adrian Pitterling, welches im Sommersemester 2021 am Fachgebiet \enquote{Graphische Datenverarbeitung} erstellt wurde. In der Anwendung, welche zu diesem Hauptseminar entwickelt wurde, können bereits einfache Szenen erstellt werden, siehe Abb. \ref{b-adrian}.\\
\noindent
Auf den Ideen dieses Hauptseminars soll aufgebaut werden, wobei vor allem die Funktionalität zur Lehre, sowie die Brechung, anpassbare Lichtquellen und die 3D-Ansicht hinzukommen. Der Code der Bachelorarbeit baut nicht auf diesem Projekt auf, sondern ist von Grund auf neu geschrieben, da bestimmte Funktionen angepasst oder als Bibliothek genutzt werden sollen.

\begin{figure}[h]
	\centering
	\includegraphics[width=0.7\linewidth]{Figures/adrian.jpg}
	\caption{Anwendung im Hauptseminar}
	\label{b-adrian}
\end{figure}

\subsection{Motivation}

Raytracing ist ein Algorithmus, um Farben einer Szene auf einer Fläche aus Pixeln darzustellen. Im Gegensatz zu einfacheren Verfahren werden durch die \enquote{Strahlenverfolgung} auch Brechung und Reflektion von Objekten sowie die Position und Stärke der Lichtquellen mit einberechnet.
Um einen graphischen Algorithmus zu verstehen ist es oft einfacher, diesen über interaktive Anwendungen selbst auszuprobieren, um verschiedene Situationen zu gestalten, welche in statischen Folien nicht enthalten sind.
Dazu wird als Haupt-Komponente eine zweidimensionale Darstellung als \enquote{Draufsicht} des Algorithmus genutzt, da dies leichter für Nutzer zu editieren ist. Zum anderen ist die Funktionsweise der Strahlen so deutlich besser zu verstehen, da in dieser
Ansicht der komplette Strahl zu sehen ist, was im Dreidimensionalen nicht zu garantieren wäre, da der Strahl oft hinter der Kamera oder Objekten verläuft.

\subsection{Struktur der Arbeit}
Diese Arbeit ist in Theorie, Umsetzung und Auswertung gegliedert. Dabei werden sowohl die Lehr- und Lernaspekte als auch der Algorithmus \enquote{Raytracing} behandelt. Zunächst werden bisherige Arbeiten zur allgemeinen interaktiven Lehre und zum Raytracing dargestellt und mögliche Kombinationen oder Verbesserungen diskutiert. Danach werden die benötigten Formeln für Raytracing und die Phong-Beleuchtung erklärt.\\
\noindent Als Grundlage für die Umsetzung werden die erwarteten Anwendungsszenarien und Nutzer dargestellt und welche Anforderungen diese mit sich bringen. Darauf folgt die Dokumentation der technischen Entwicklung, in welcher beschrieben wird, wie diese Anforderungen praktisch umgesetzt wurden. Die Ergebnisse der Entwicklung werden in der Auswertung beurteilt, wobei durch gesammelte Daten und Vergleiche die Nutzbarkeit und der Grad der erreichten Anforderungen bewertet werden.\\
\noindent Im Anhang befinden sich des weiteren besonders wichtige und teilweise kommentierte Auszüge aus dem Code, mehr Bildquellen und die genauen Daten der Messung für die Auswertung.

\newpage

\section{Bisherige Arbeiten}

\subsection{Raytracing}
Der erste Algorithmus, welcher die Strahlverfolgung umsetzt (Abb. \ref{oldRT}), heute als Raytracing bekannt, wurde in den 1960er Jahren von Arthur Appel, Robert Goldstein und Roger Nagel entwickelt \cite{firstRaytracer}. Das erste so berechnete Bild wurde 1963 an der University of Maryland auf einem oszilloskopartigen Bildschirm ausgegeben \cite{wikiRayTrace}. Die Berechnung der ersten Bilder war auf damaligen Computern aufgrund der pro-pixel Berechnung sehr langsam. Es gab im Laufe der Zeit jedoch viele Neuerungen in der Hardware, was auch Erweiterungen des Raytracings, wie z.B. mehrere reflektierte Strahlen für diffuse oder \enquote{glossy} Oberflächen, sowie die Kombination mit Beleuchtungsmodellen möglich machte.
\begin{figure}[h]
	\centering
	% trim={<left> <lower> <right> <upper>}
	\includegraphics[clip, width=0.4\linewidth, height=250px]{Figures/goldsteinRT_1.png}
	\includegraphics[clip, width=0.4\linewidth, height=250px]{Figures/goldsteinRT_2.png}
	\caption{Ergebnisse der Arbeit von Goldstein und Nagel \cite{firstRaytracer}}
	\label{oldRT}
\end{figure}

\subsection{Phong-Beleuchtung}
Das später nach seinem Entwickler benannte \enquote{Phong-Beleuchtungsmodell} wurde bereits 1975 von Bui Tuong Phong entwickelt \cite{origPhong}. Es dominiert bis heute die Computergraphik und wird u.A. von Crytek's CryEngine series, Valve's Source Engine, Epic's Unreal Engine 3 und 4 so wie Square Enix's Luminous Studio verwendet \cite{phongSource}.

\newpage

\subsection{Computergraphik-Folien}
Die Folien der TU Ilmenau \cite{tuiFolien} bieten statische Graphiken (z.B. Abb. \ref{b-Graphik-Folie}) zur Veranschaulichung des Algorithmus. Diese bilden jedoch nur sehr bestimmte Situationen ab und beinhalten keine eigentliche Anwendung von Raytracing, sondern zeigen lediglich den Aug-Strahl sowie die Schattenfühler. Daher ist das Ziel dieser Arbeit die eigenständige Erkundung verschiedener Situationen, wie z.B. mehrere Lichtquellen, sowie die eigentliche Berechnung der Farben am Objekt und am Pixel interaktiv und anschaulich darzustellen.
\begin{figure}[h]
	\centering
	\includegraphics[width=0.3\linewidth]{Figures/exampleCGFolien.jpg}
	\caption{Graphik aus einer Computergraphik Vorlesung zu Raytracing}
	\label{b-Graphik-Folie}
\end{figure}

\subsection{2D-Raytracing mit Top-View}
Auf der Webseite von Taylor Petrick \cite{taylorPetrick} ist eine Umsetzung des Algorithmus bereitgestellt, welche er zum Testen verschiedener Techniken nutzte (siehe Abb. \ref{b-top-view-taylor}).
Dieses Programm setzt 2D-Raytracing auch als Draufsicht um, verwendet aber keine einzelnen Strahlen und ist sehr restriktiv in den umsetzbaren Situationen. Sie ist deshalb nicht zur Lehre geeignet.
\begin{figure}[h]
	\centering
	\includegraphics[width=0.3\linewidth]{Figures/taylor.png}
	\caption{Raytracing in der Draufsicht \cite{taylorPetrick}}
	\label{b-top-view-taylor}
\end{figure}

\newpage

\subsection{3D-Raytracing Demo}
Rayground \cite{rayground} hat einen online Editor für ihre API mit Raytracing-Code (siehe Abb. \ref{b-rayground-demo} rechts), welcher direkt auf der Website geändert und ausgeführt werden kann. Dabei enthält der Beispielcode jedoch nur diffuse Berechnungen, und kann daher ohne eigene Ergänzungen nicht sinnvoll eingesetzt werden, was sowohl einige Vorkenntnisse zur Phong-Beleuchtung als auch Programmiererfahrung voraussetzt.

\begin{figure}[h]
	\centering
	\includegraphics[width=\linewidth]{Figures/rayground.jpg}
	\caption{Screenshot des Rayground-Editors}
	\label{b-rayground-demo}
\end{figure}

\newpage

\section{Raytracing in der Lehre}
Neben den Folien, welche Universitäten den Studierenden zur Verfügung stellen, gibt es einige weitere Möglichkeiten, sich über Raytracing zu informieren. Dazu gehören wissenschaftliche Arbeiten, Informatik fokussierte Webseiten und Blogs sowie Videos, welche die Grundlagen des Raytracing vermitteln möchten.

\begin{figure}[h]
	\centering
	\includegraphics[width=0.7\linewidth]{Figures/bsp_lehre/nvidia.png}
	\caption{Beispiel Video von NVIDIA \cite{example-edu-1}}
	\label{b-example-edu-1}
\end{figure}
\noindent
Was besonders auffällt ist, dass es hauptsächlich zwei verschiedene Arten von Artikeln zum Thema gibt. Zum einen sind dies Videospiel-, und vor allem Spieler-orientierte Blogs wie in Abb. \ref{b-example-edu-2}. Diese erklären Raytracing nur sehr oberflächlich und zeigen fertig gerenderte Bilder.

\begin{figure}[h]
	\centering
	\includegraphics[width=0.5\linewidth]{Figures/bsp_lehre/ionos.png}
	\caption{Ausschnitt des Artikels zum Thema Raytracing von Ionos \cite{example-edu-2}}
	\label{b-example-edu-2}
\end{figure}

\newpage
\noindent
Zum anderen gibt es code- oder formelorientierte Quellen, welche auch mit Vorkenntnissen schwer zu verstehen sind, wie in Abb. \ref{b-example-edu-3}.

\begin{figure}[h]
	\centering
	\includegraphics[width=0.3\linewidth]{Figures/bsp_lehre/ez.png}
	\caption{Beispiel einer wissenschaftlichen Arbeit zu Raytracing \cite{example-edu-3}}
	\label{b-example-edu-3}
\end{figure}

\subsection{Interaktive Lehre}
\label{interactive-learn}
Die Arbeit \cite{cgInteractive} zeigt die Relevanz von interaktiver Lehre in der Computergraphik und gibt einige Design-Prinzipien vor. Es wird dabei besonders auf \enquote{web-based learning} eingegangen, also Lehre im Browser und ohne direkten Lehrenden. Folgende Prinzipien werden in dieser Arbeit berücksichtigt:
\begin{itemize}
	\item \textbf{Graphical user interface:} Die GUI sollte möglichst interaktiv sein, und Texte durch Graphiken unterstützen.
	\item \textbf{User Characteristics:} Die Anwendung sollte Nutzergruppen analysieren und auf diese zugeschnitten sein, siehe dazu Kapitel \ref{anwendungsmoeglichkeiten} - Anwendungsmöglichkeiten.
	\item \textbf{User Friendly:} Um neuen Nutzern den Einstig zu ermöglichen sollte die Anwendung möglichst so entworfen werden, dass die Grundlagen ohne lange Erklärungstexte zu nutzen sind.
	\item \textbf{Interactive:} Es sollten lange Passagen von statischem Inhalt vermieden werden.
\end{itemize}

\noindent
Die Verfasser von \cite{cgInteractive} haben auch eine kleine Anwendung zu Raytracing entwickelt, jedoch nur in 3D und mit limitierter Interaktivität.

\newpage \noindent
Für Raytracing gibt es bisher keine interaktiven Anwendungen, welche in der Lehre eingesetzt werden können. Dabei kann Interaktivität beim Lernen helfen.
In \cite{interactiveLearn} wurden Computerspiele und Anwendungen, bei denen ein Lernender Parameter anpassen konnte, mit traditionellen Lehrmethoden verglichen. Dabei kamen sie zu dem Ergebnis, dass interaktive Lehrmedien im direkten Vergleich besser abschneiden.
\\\textit{\enquote{Not surprisingly, the overall results yielded significantly higher cognitive gains
and better attitudes toward learning for subjects utilizing interactive games or
simulations compared to those using traditional teaching methods for instruction}}\\
 - \enquote{Computer Gaming and Interactive Simulations for Learning} (2006) \cite{interactiveLearn}\\
\newline
\noindent Bei der Erklärung, insbesondere bei komplexen Algorithmen wie Raytracing hat die Interaktivität viele Vorteile.\\ \newline
\textbf{Nutzer nehmen die Informationen unterschiedlich schnell auf:}\\
Videos oder Vorlesungen sind in ihrer Geschwindigkeit an bestimmte Personengruppen angepasst. Ein großer Faktor ist hier das Vorwissen. Wird die Vektorrechnung noch einmal wiederholt, kurz erwähnt oder ganz übersprungen? So kommt es häufig vor, dass Zuhörer bei einem Video nicht hinterherkommen oder, dass ihnen das Video zu langsam ist. Die eigenständige Erkundung ermöglicht Nutzern in eigener Geschwindigkeit die Erklärungen nachzuvollziehen.\\
% https://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.956.1310&rep=rep1&type=pdf

\noindent\textbf{Vorteile eigener Szenen:}\\
Die Szenen selbst erstellen zu können hat mehrere Vorteile. Zum einen können Lernende genau die Szenen bauen, welche sie benötigen, ohne suchen zu müssen, ob und wo es eine solche Graphik gibt. Für den Anfang ist es möglich eine einfache Szene mit einem Objekt zu bauen, um die Phong-Beleuchtung mit verschiedenen Farben oder Winkeln zu testen. Fortgeschrittene Nutzer können komplexere Szenen bauen und zum Beispiel die Auswirkung von Brechungsindices auf die Brechung des Strahls beobachten.
Zum anderen ist es einfacher die berechnete Farbe eines Strahles nachzuvollziehen, wenn der Nutzer die Szene selbst erstellt hat, und dabei Einfluss auf alle Faktoren, wie zum Beispiel Farbe und Winkel hat.\\

\noindent\textbf{Eigene Erkenntnisse gewinnen:}\\
\textit{\enquote{Verstehensprozesse resultieren aus etwas anderem als dem klassischen Lernen oder Lerntricks, wie man sie z.B. in der Bildung gerne propagiert.}}
\\- Henning Beck, Neurowissenschaftler (im Interview siehe \cite{interview-1})
\newline \noindent
Eigene Erkenntnisse zu erlangen sorgt für Verstehen des Themas, im Gegensatz zum Auswendiglernen von Lehrbuchtexten. Verstandenes Wissen ist länger verfügbar als auswendig gelerntes. Außerdem is es einfacher in weiterführenden Erklärungen auf dieses Wissen aufzubauen.\\

\newpage

\noindent\textbf{Bewegte Bilder:}\\
Graphische Anwendungen und Videos haben den Vorteil der Bewegung gegenüber statischen Graphiken. Viele Konzepte und Auswirkungen von Parametern im Raytracing lassen sich am besten in der Bewegung nachverfolgen. So kann z.B. die Lichtquelle oder Kamera bewegt werden, um dabei die Auswirkung der Winkel auf die berechnete Farbe zu beobachten.\\

\noindent\textbf{Zusammenhang von Theorie und Praxis:}\\
Oft sind in den Erklärungen die theoretischen Formeln und 2D-Graphiken getrennt (siehe Abb. \ref{b-example-edu-1}). Das kann zu Abstraktionsproblemen der Theorie, oder zu einem schwereren Verständnis des Überganges von Formel zu Graphik führen. Dies ist ein Problem, welches auch in statischen Folien gelöst werden könnte, indem der Lehrende zusammenpassende Graphiken erstellt. Dies ist jedoch von Hand sehr schwierig und ungenau. Des weiteren ist der Zusammenhang in einer interaktiven Anwendung einfacher zu erkennen.\\

\noindent\textbf{Motivation und Aufmerksamkeit:}\\
Die Motivation zum Lernen und Aufmerksamkeit auf den Stoff werden generell durch Interaktivität erhöht. Die Aufmerksamkeit wird vor allem dadurch gesteigert, dass der Lernende aktiv beteiligt ist, und eigene Ideen ausprobieren kann. Das führt zu mehr Freiheiten beim Lernen, und dadurch zu mehr Motivation. So wurden in \cite{gamesMotivation} verschiedene Studien zu dem Thema analysiert. Die Studie kam zu dem Ergebnis, dass interaktives Lernen die Motivation erhöhen kann (siehe Abb. \ref{b-result-games-motivation}).\\
\small{\textit{\enquote{Q1: What is the impact of mathematical computer games on realising educational goals at all levels of education in learning mathematics?\\
Q2: How does the usage of mathematical computer games influence pupils’ motivation
and attitude to mathematics as the most difficult subject?}}}

\begin{figure}[h]
	\centering
	\includegraphics[width=0.37\linewidth]{Figures/bsp_lehre/motivation.png}
	\caption{Einfluss interaktiver Lernmöglichkeiten in der Mathematik \cite{gamesMotivation}}
	\label{b-result-games-motivation}
\end{figure}

\newpage