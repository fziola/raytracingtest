\boolfalse {citerequest}\boolfalse {citetracker}\boolfalse {pagetracker}\boolfalse {backtracker}\relax 
\babel@toc {ngerman}{}\relax 
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {1}Einleitung}{1}{section.1}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {1.1}Aufgabenstellung}{1}{subsection.1.1}%
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {1.1.1}Inspiration durch Ergebnisse eines Hauptseminars}{1}{subsubsection.1.1.1}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {1.2}Motivation}{2}{subsection.1.2}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {1.3}Struktur der Arbeit}{2}{subsection.1.3}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2}Bisherige Arbeiten}{3}{section.2}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.1}Raytracing}{3}{subsection.2.1}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.2}Phong-Beleuchtung}{3}{subsection.2.2}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.3}Computergraphik-Folien}{4}{subsection.2.3}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.4}2D-Raytracing mit Top-View}{4}{subsection.2.4}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.5}3D-Raytracing Demo}{5}{subsection.2.5}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {3}Raytracing in der Lehre}{6}{section.3}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.1}Interaktive Lehre}{7}{subsection.3.1}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {4}Grundlagen des Raytracings}{10}{section.4}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.1}Strahlverfolgung}{10}{subsection.4.1}%
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {4.1.1}Reflektion}{11}{subsubsection.4.1.1}%
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {4.1.2}Refraktion}{12}{subsubsection.4.1.2}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.2}Schnittpunktberechnung}{13}{subsection.4.2}%
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {4.2.1}2D-Schnittpunkt}{13}{subsubsection.4.2.1}%
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {4.2.2}3D-Schnittpunkt}{14}{subsubsection.4.2.2}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.3}Phong-Beleuchtungsmodell}{17}{subsection.4.3}%
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {4.3.1}vollständige Formel}{18}{subsubsection.4.3.1}%
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {4.3.2}ambienter Anteil}{19}{subsubsection.4.3.2}%
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {4.3.3}diffuser Anteil}{19}{subsubsection.4.3.3}%
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {4.3.4}spekularer Anteil}{19}{subsubsection.4.3.4}%
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {4.3.5}Distanz zur Lichtquelle}{19}{subsubsection.4.3.5}%
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {4.3.6}Anteile visuell}{20}{subsubsection.4.3.6}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {5}Anwendungsmöglichkeiten der entwickelten App}{21}{section.5}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {5.1}Folienerstellung}{21}{subsection.5.1}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {5.2}Live-Demonstrationen}{21}{subsection.5.2}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {5.3}Eigenständiges Lernen}{22}{subsection.5.3}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {5.4}Aufgaben bearbeiten}{22}{subsection.5.4}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {6}Umsetzung}{23}{section.6}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {6.1}Genutzte Technologien}{23}{subsection.6.1}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {6.2}Zustand der Anwendung (State)}{25}{subsection.6.2}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {6.3}2D-Graphik Berechnung}{25}{subsection.6.3}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {6.4}3D-Graphik Berechnung}{27}{subsection.6.4}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {6.5}Export / Import}{27}{subsection.6.5}%
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {6.5.1}Graphik-Export}{28}{subsubsection.6.5.1}%
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {6.5.2}State-Ex-/Import}{28}{subsubsection.6.5.2}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {6.6}Points Of Interest}{30}{subsection.6.6}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {6.7}3D-Inspektion}{32}{subsection.6.7}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {6.8}Bearbeitung im Canvas}{33}{subsection.6.8}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {6.9}Personalisierungen}{34}{subsection.6.9}%
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {6.9.1}Optionen}{34}{subsubsection.6.9.1}%
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {6.9.2}Einstellungen}{35}{subsubsection.6.9.2}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {6.10}Tests}{37}{subsection.6.10}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {6.11}Undo/Redo}{38}{subsection.6.11}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {7}Auswertung}{39}{section.7}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {7.1}Erstellung und Einbindung von Graphiken}{39}{subsection.7.1}%
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {7.1.1}Einbindung in PDF}{39}{subsubsection.7.1.1}%
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {7.1.2}Einbindung in PowerPoint}{40}{subsubsection.7.1.2}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {7.2}Einbindung in HTML}{41}{subsection.7.2}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {7.3}Export und Import des Zustandes}{42}{subsection.7.3}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {7.4}Echtzeitmodifikationen}{43}{subsection.7.4}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {7.5}Qualität der erstellten Graphiken}{45}{subsection.7.5}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {7.6}Kompatibilität der Anwendung}{46}{subsection.7.6}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {8}Zusammenfassung}{48}{section.8}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {9}Ausblick}{49}{section.9}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{Literatur}{51}{section*.71}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {A}FPS - gemessene Werte und Messszenarien}{55}{appendix.A}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {B}Dateigröße JSON - gemessene Werte}{57}{appendix.B}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {C}Beispiel für einen App-State}{58}{appendix.C}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {D}2D-Grafikerstellung}{60}{appendix.D}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {E}Qualitätsvergleiche}{62}{appendix.E}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {F}Options}{64}{appendix.F}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {G}Ausgewählte Code-Sequenzen}{65}{appendix.G}%
